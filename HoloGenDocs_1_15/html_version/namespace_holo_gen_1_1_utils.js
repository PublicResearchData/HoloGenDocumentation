var namespace_holo_gen_1_1_utils =
[
    [ "BitmapUtils", "class_holo_gen_1_1_utils_1_1_bitmap_utils.html", null ],
    [ "ColorInterpolator", "class_holo_gen_1_1_utils_1_1_color_interpolator.html", null ],
    [ "ColorScheme", "class_holo_gen_1_1_utils_1_1_color_scheme.html", "class_holo_gen_1_1_utils_1_1_color_scheme" ],
    [ "ColorSchemes", "class_holo_gen_1_1_utils_1_1_color_schemes.html", "class_holo_gen_1_1_utils_1_1_color_schemes" ],
    [ "Dimension", "class_holo_gen_1_1_utils_1_1_dimension.html", "class_holo_gen_1_1_utils_1_1_dimension" ],
    [ "FileUtils", "class_holo_gen_1_1_utils_1_1_file_utils.html", null ],
    [ "GenericCache", "class_holo_gen_1_1_utils_1_1_generic_cache.html", null ],
    [ "InfoUtils", "class_holo_gen_1_1_utils_1_1_info_utils.html", null ],
    [ "LogFileHandle", "class_holo_gen_1_1_utils_1_1_log_file_handle.html", "class_holo_gen_1_1_utils_1_1_log_file_handle" ],
    [ "ObservableObject", "class_holo_gen_1_1_utils_1_1_observable_object.html", "class_holo_gen_1_1_utils_1_1_observable_object" ],
    [ "Scale", "class_holo_gen_1_1_utils_1_1_scale.html", "class_holo_gen_1_1_utils_1_1_scale" ],
    [ "StatusMessage", "class_holo_gen_1_1_utils_1_1_status_message.html", "class_holo_gen_1_1_utils_1_1_status_message" ],
    [ "Time", "class_holo_gen_1_1_utils_1_1_time.html", null ]
];