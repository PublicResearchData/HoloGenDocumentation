var namespace_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_variants =
[
    [ "Common", "namespace_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_variants_1_1_common.html", "namespace_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_variants_1_1_common" ],
    [ "DS", "namespace_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_variants_1_1_d_s.html", "namespace_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_variants_1_1_d_s" ],
    [ "GS", "namespace_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_variants_1_1_g_s.html", "namespace_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_variants_1_1_g_s" ],
    [ "OSPR", "namespace_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_variants_1_1_o_s_p_r.html", "namespace_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_variants_1_1_o_s_p_r" ],
    [ "SA", "namespace_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_variants_1_1_s_a.html", "namespace_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_variants_1_1_s_a" ],
    [ "VariantOption", "interface_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_variants_1_1_variant_option.html", "interface_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_variants_1_1_variant_option" ],
    [ "VariantPossibility", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_variants_1_1_variant_possibility.html", null ]
];