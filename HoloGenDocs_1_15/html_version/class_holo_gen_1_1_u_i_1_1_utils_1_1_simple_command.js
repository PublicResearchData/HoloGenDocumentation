var class_holo_gen_1_1_u_i_1_1_utils_1_1_simple_command =
[
    [ "CanExecute", "class_holo_gen_1_1_u_i_1_1_utils_1_1_simple_command.html#a752ff56947b0629edb18fece4464ff3f", null ],
    [ "Execute", "class_holo_gen_1_1_u_i_1_1_utils_1_1_simple_command.html#a08c87701c14988b5c8ef470bbedb0f85", null ],
    [ "CanExecuteChanged", "class_holo_gen_1_1_u_i_1_1_utils_1_1_simple_command.html#a63eff5886d58146366ce615120bfa654", null ],
    [ "CanExecuteDelegate", "class_holo_gen_1_1_u_i_1_1_utils_1_1_simple_command.html#a8f9d6b737ec789f11793262911bdb87d", null ],
    [ "ExecuteDelegate", "class_holo_gen_1_1_u_i_1_1_utils_1_1_simple_command.html#a52d438de1c3346fcc293894336cad1c9", null ]
];