var class_holo_gen_1_1_settings_1_1_general_1_1_file_1_1_save_scale_bar_on_image_export =
[
    [ "Default", "class_holo_gen_1_1_settings_1_1_general_1_1_file_1_1_save_scale_bar_on_image_export.html#a2b66332fabb3f045359c0fe171b68e57", null ],
    [ "Name", "class_holo_gen_1_1_settings_1_1_general_1_1_file_1_1_save_scale_bar_on_image_export.html#adbc53a1909479a393b19d78f40284a50", null ],
    [ "OffText", "class_holo_gen_1_1_settings_1_1_general_1_1_file_1_1_save_scale_bar_on_image_export.html#a0f087e386cf28f6d75421af78166546d", null ],
    [ "OnText", "class_holo_gen_1_1_settings_1_1_general_1_1_file_1_1_save_scale_bar_on_image_export.html#a80e9d17c263024d9bc343dd3cb06b8ea", null ],
    [ "ToolTip", "class_holo_gen_1_1_settings_1_1_general_1_1_file_1_1_save_scale_bar_on_image_export.html#ad5476440c31ce94ffb9a29523f057cce", null ],
    [ "Watermark", "class_holo_gen_1_1_settings_1_1_general_1_1_file_1_1_save_scale_bar_on_image_export.html#ad9b4d4676679aad6e7109b47bf104845", null ]
];