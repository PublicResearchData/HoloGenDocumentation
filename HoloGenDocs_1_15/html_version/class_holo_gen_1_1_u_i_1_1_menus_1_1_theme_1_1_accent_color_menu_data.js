var class_holo_gen_1_1_u_i_1_1_menus_1_1_theme_1_1_accent_color_menu_data =
[
    [ "DoChangeTheme", "class_holo_gen_1_1_u_i_1_1_menus_1_1_theme_1_1_accent_color_menu_data.html#a6da368c69c57d7cf3674156b6f89ea7c", null ],
    [ "BorderColorBrush", "class_holo_gen_1_1_u_i_1_1_menus_1_1_theme_1_1_accent_color_menu_data.html#a47521754d2a665106899e0e4d56303ff", null ],
    [ "ChangeAccentCommand", "class_holo_gen_1_1_u_i_1_1_menus_1_1_theme_1_1_accent_color_menu_data.html#ab800001afaaed51c9d0eeb2a6302a95f", null ],
    [ "ColorBrush", "class_holo_gen_1_1_u_i_1_1_menus_1_1_theme_1_1_accent_color_menu_data.html#a69da3972c2ae9d2108dbdf674fdec20d", null ],
    [ "IsSelected", "class_holo_gen_1_1_u_i_1_1_menus_1_1_theme_1_1_accent_color_menu_data.html#af08252f04af568c3112f85022dd2acfa", null ],
    [ "Name", "class_holo_gen_1_1_u_i_1_1_menus_1_1_theme_1_1_accent_color_menu_data.html#ad7940e2f93061d0c96068d45d5a9851b", null ]
];