var namespace_holo_gen_1_1_u_i_1_1_menus =
[
    [ "Theme", "namespace_holo_gen_1_1_u_i_1_1_menus_1_1_theme.html", "namespace_holo_gen_1_1_u_i_1_1_menus_1_1_theme" ],
    [ "ViewModels", "namespace_holo_gen_1_1_u_i_1_1_menus_1_1_view_models.html", "namespace_holo_gen_1_1_u_i_1_1_menus_1_1_view_models" ],
    [ "Views", "namespace_holo_gen_1_1_u_i_1_1_menus_1_1_views.html", "namespace_holo_gen_1_1_u_i_1_1_menus_1_1_views" ],
    [ "IMenuWindow", "interface_holo_gen_1_1_u_i_1_1_menus_1_1_i_menu_window.html", "interface_holo_gen_1_1_u_i_1_1_menus_1_1_i_menu_window" ]
];