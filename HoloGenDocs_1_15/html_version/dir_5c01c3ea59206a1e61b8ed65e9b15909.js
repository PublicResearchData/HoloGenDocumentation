var dir_5c01c3ea59206a1e61b8ed65e9b15909 =
[
    [ "ColorSchemeOption.cs", "_color_scheme_option_8cs.html", [
      [ "ColorSchemeOption", "class_holo_gen_1_1_image_1_1_options_1_1_color_scheme_option.html", "class_holo_gen_1_1_image_1_1_options_1_1_color_scheme_option" ]
    ] ],
    [ "ColorSchemePossibilities.cs", "_color_scheme_possibilities_8cs.html", [
      [ "ColorSchemePossibilities", "class_holo_gen_1_1_image_1_1_options_1_1_color_scheme_possibilities.html", "class_holo_gen_1_1_image_1_1_options_1_1_color_scheme_possibilities" ]
    ] ],
    [ "ColorSchemePossibility.cs", "_color_scheme_possibility_8cs.html", [
      [ "ColorSchemePossibility", "class_holo_gen_1_1_image_1_1_options_1_1_color_scheme_possibility.html", "class_holo_gen_1_1_image_1_1_options_1_1_color_scheme_possibility" ]
    ] ],
    [ "Dimension2D.cs", "_dimension2_d_8cs.html", [
      [ "Dimension2D", "class_holo_gen_1_1_image_1_1_options_1_1_dimension2_d.html", "class_holo_gen_1_1_image_1_1_options_1_1_dimension2_d" ]
    ] ],
    [ "Dimension3D.cs", "_dimension3_d_8cs.html", [
      [ "Dimension3D", "class_holo_gen_1_1_image_1_1_options_1_1_dimension3_d.html", "class_holo_gen_1_1_image_1_1_options_1_1_dimension3_d" ]
    ] ],
    [ "DimensionOption.cs", "_dimension_option_8cs.html", [
      [ "DimensionOption", "class_holo_gen_1_1_image_1_1_options_1_1_dimension_option.html", "class_holo_gen_1_1_image_1_1_options_1_1_dimension_option" ]
    ] ],
    [ "DimensionPossibilities.cs", "_dimension_possibilities_8cs.html", [
      [ "DimensionPossibilities", "class_holo_gen_1_1_image_1_1_options_1_1_dimension_possibilities.html", "class_holo_gen_1_1_image_1_1_options_1_1_dimension_possibilities" ]
    ] ],
    [ "DimensionPossibility.cs", "_dimension_possibility_8cs.html", [
      [ "DimensionPossibility", "class_holo_gen_1_1_image_1_1_options_1_1_dimension_possibility.html", null ]
    ] ],
    [ "ImageOptions.cs", "_type_2_image_options_8cs.html", [
      [ "ImageOptions", "class_holo_gen_1_1_image_1_1_options_1_1_type_1_1_image_options.html", "class_holo_gen_1_1_image_1_1_options_1_1_type_1_1_image_options" ]
    ] ],
    [ "ImageViewImaginary.cs", "_image_view_imaginary_8cs.html", [
      [ "ImageViewImaginary", "class_holo_gen_1_1_image_1_1_options_1_1_image_view_imaginary.html", "class_holo_gen_1_1_image_1_1_options_1_1_image_view_imaginary" ]
    ] ],
    [ "ImageViewMagnitude.cs", "_image_view_magnitude_8cs.html", [
      [ "ImageViewMagnitude", "class_holo_gen_1_1_image_1_1_options_1_1_image_view_magnitude.html", "class_holo_gen_1_1_image_1_1_options_1_1_image_view_magnitude" ]
    ] ],
    [ "ImageViewOption.cs", "_image_view_option_8cs.html", [
      [ "ImageViewOption", "class_holo_gen_1_1_image_1_1_options_1_1_image_view_option.html", "class_holo_gen_1_1_image_1_1_options_1_1_image_view_option" ]
    ] ],
    [ "ImageViewPhase.cs", "_image_view_phase_8cs.html", [
      [ "ImageViewPhase", "class_holo_gen_1_1_image_1_1_options_1_1_image_view_phase.html", "class_holo_gen_1_1_image_1_1_options_1_1_image_view_phase" ]
    ] ],
    [ "ImageViewPossibilities.cs", "_image_view_possibilities_8cs.html", [
      [ "ImageViewPossibilities", "class_holo_gen_1_1_image_1_1_options_1_1_image_view_possibilities.html", "class_holo_gen_1_1_image_1_1_options_1_1_image_view_possibilities" ]
    ] ],
    [ "ImageViewPossibility.cs", "_image_view_possibility_8cs.html", [
      [ "ImageViewPossibility", "class_holo_gen_1_1_image_1_1_options_1_1_image_view_possibility.html", "class_holo_gen_1_1_image_1_1_options_1_1_image_view_possibility" ]
    ] ],
    [ "ImageViewReal.cs", "_image_view_real_8cs.html", [
      [ "ImageViewReal", "class_holo_gen_1_1_image_1_1_options_1_1_image_view_real.html", "class_holo_gen_1_1_image_1_1_options_1_1_image_view_real" ]
    ] ],
    [ "ScaleLocationBottom.cs", "_scale_location_bottom_8cs.html", [
      [ "ScaleLocationBottom", "class_holo_gen_1_1_image_1_1_options_1_1_scale_location_bottom.html", "class_holo_gen_1_1_image_1_1_options_1_1_scale_location_bottom" ]
    ] ],
    [ "ScaleLocationLeft.cs", "_scale_location_left_8cs.html", [
      [ "ScaleLocationLeft", "class_holo_gen_1_1_image_1_1_options_1_1_scale_location_left.html", "class_holo_gen_1_1_image_1_1_options_1_1_scale_location_left" ]
    ] ],
    [ "ScaleLocationNone.cs", "_scale_location_none_8cs.html", [
      [ "ScaleLocationNone", "class_holo_gen_1_1_image_1_1_options_1_1_scale_location_none.html", "class_holo_gen_1_1_image_1_1_options_1_1_scale_location_none" ]
    ] ],
    [ "ScaleLocationOption.cs", "_scale_location_option_8cs.html", [
      [ "ScaleLocationOption", "class_holo_gen_1_1_image_1_1_options_1_1_scale_location_option.html", "class_holo_gen_1_1_image_1_1_options_1_1_scale_location_option" ]
    ] ],
    [ "ScaleLocationPossibilities.cs", "_scale_location_possibilities_8cs.html", [
      [ "ScaleLocationPossibilities", "class_holo_gen_1_1_image_1_1_options_1_1_scale_location_possibilities.html", "class_holo_gen_1_1_image_1_1_options_1_1_scale_location_possibilities" ]
    ] ],
    [ "ScaleLocationPossibility.cs", "_scale_location_possibility_8cs.html", [
      [ "ScaleLocationPossibility", "class_holo_gen_1_1_image_1_1_options_1_1_scale_location_possibility.html", "class_holo_gen_1_1_image_1_1_options_1_1_scale_location_possibility" ]
    ] ],
    [ "ScaleLocationRight.cs", "_scale_location_right_8cs.html", [
      [ "ScaleLocationRight", "class_holo_gen_1_1_image_1_1_options_1_1_scale_location_right.html", "class_holo_gen_1_1_image_1_1_options_1_1_scale_location_right" ]
    ] ],
    [ "ScaleLocationTop.cs", "_scale_location_top_8cs.html", [
      [ "ScaleLocationTop", "class_holo_gen_1_1_image_1_1_options_1_1_scale_location_top.html", "class_holo_gen_1_1_image_1_1_options_1_1_scale_location_top" ]
    ] ],
    [ "TransformTypeFFT.cs", "_transform_type_f_f_t_8cs.html", [
      [ "TransformTypeFFT", "class_holo_gen_1_1_image_1_1_options_1_1_transform_type_f_f_t.html", "class_holo_gen_1_1_image_1_1_options_1_1_transform_type_f_f_t" ]
    ] ],
    [ "TransformTypeIFFT.cs", "_transform_type_i_f_f_t_8cs.html", [
      [ "TransformTypeIFFT", "class_holo_gen_1_1_image_1_1_options_1_1_transform_type_i_f_f_t.html", "class_holo_gen_1_1_image_1_1_options_1_1_transform_type_i_f_f_t" ]
    ] ],
    [ "TransformTypeNone.cs", "_transform_type_none_8cs.html", [
      [ "TransformTypeNone", "class_holo_gen_1_1_image_1_1_options_1_1_transform_type_none.html", "class_holo_gen_1_1_image_1_1_options_1_1_transform_type_none" ]
    ] ],
    [ "TransformTypeOption.cs", "_transform_type_option_8cs.html", [
      [ "TransformTypeOption", "class_holo_gen_1_1_image_1_1_options_1_1_transform_type_option.html", "class_holo_gen_1_1_image_1_1_options_1_1_transform_type_option" ]
    ] ],
    [ "TransformTypePossibilities.cs", "_transform_type_possibilities_8cs.html", [
      [ "TransformTypePossibilities", "class_holo_gen_1_1_image_1_1_options_1_1_transform_type_possibilities.html", "class_holo_gen_1_1_image_1_1_options_1_1_transform_type_possibilities" ]
    ] ],
    [ "TransformTypePossibility.cs", "_transform_type_possibility_8cs.html", [
      [ "TransformTypePossibility", "class_holo_gen_1_1_image_1_1_options_1_1_transform_type_possibility.html", "class_holo_gen_1_1_image_1_1_options_1_1_transform_type_possibility" ]
    ] ]
];