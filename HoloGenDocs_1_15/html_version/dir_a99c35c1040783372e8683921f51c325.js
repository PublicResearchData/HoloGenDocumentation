var dir_a99c35c1040783372e8683921f51c325 =
[
    [ "ShowCloseUnsavedFileWarning.cs", "_show_close_unsaved_file_warning_8cs.html", [
      [ "ShowCloseUnsavedFileWarning", "class_holo_gen_1_1_settings_1_1_general_1_1_file_1_1_show_close_unsaved_file_warning.html", "class_holo_gen_1_1_settings_1_1_general_1_1_file_1_1_show_close_unsaved_file_warning" ]
    ] ],
    [ "ShowLargeMatExportWarning.cs", "_show_large_mat_export_warning_8cs.html", [
      [ "ShowLargeMatExportWarning", "class_holo_gen_1_1_settings_1_1_general_1_1_file_1_1_show_large_mat_export_warning.html", "class_holo_gen_1_1_settings_1_1_general_1_1_file_1_1_show_large_mat_export_warning" ]
    ] ],
    [ "ShowLargeMatImportWarning.cs", "_show_large_mat_import_warning_8cs.html", [
      [ "ShowLargeMatImportWarning", "class_holo_gen_1_1_settings_1_1_general_1_1_file_1_1_show_large_mat_import_warning.html", "class_holo_gen_1_1_settings_1_1_general_1_1_file_1_1_show_large_mat_import_warning" ]
    ] ],
    [ "WarningSettingsFolder.cs", "_warning_settings_folder_8cs.html", [
      [ "WarningSettingsFolder", "class_holo_gen_1_1_settings_1_1_general_1_1_file_1_1_warning_settings_folder.html", "class_holo_gen_1_1_settings_1_1_general_1_1_file_1_1_warning_settings_folder" ]
    ] ]
];