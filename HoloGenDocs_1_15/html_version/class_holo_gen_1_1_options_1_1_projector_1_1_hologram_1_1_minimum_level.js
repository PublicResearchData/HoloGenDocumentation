var class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_minimum_level =
[
    [ "Default", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_minimum_level.html#a58eb0320015825c29b7c33b1eb4abc30", null ],
    [ "Enabled", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_minimum_level.html#a879f2ad55ef0f5d0cbf936ba4bdfc95b", null ],
    [ "MaxValue", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_minimum_level.html#abadf60243988fdc83e62798e13db8267", null ],
    [ "MinValue", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_minimum_level.html#a735a587e7b56a197ae54c5501e48190c", null ],
    [ "Name", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_minimum_level.html#adfc486840c3c2c9c0d197fdf170e4f43", null ],
    [ "ToolTip", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_minimum_level.html#abf6450a52444de1df011507984e54ee2", null ],
    [ "Watermark", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_minimum_level.html#af2e9505be436db51ea0f27631e76a27c", null ]
];