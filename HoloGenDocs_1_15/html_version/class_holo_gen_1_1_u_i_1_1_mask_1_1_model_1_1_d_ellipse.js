var class_holo_gen_1_1_u_i_1_1_mask_1_1_model_1_1_d_ellipse =
[
    [ "DEllipse", "class_holo_gen_1_1_u_i_1_1_mask_1_1_model_1_1_d_ellipse.html#a2221efc2b641e1baa105377f82925cab", null ],
    [ "EllipseBody", "class_holo_gen_1_1_u_i_1_1_mask_1_1_model_1_1_d_ellipse.html#a14146e90f9f06c64bfd354a394470c63", null ],
    [ "Height", "class_holo_gen_1_1_u_i_1_1_mask_1_1_model_1_1_d_ellipse.html#a9b0c13f33f67281aee414cf8918bbd71", null ],
    [ "Left", "class_holo_gen_1_1_u_i_1_1_mask_1_1_model_1_1_d_ellipse.html#a5e5b034eea4be48bfdc8ceff63c28d7c", null ],
    [ "P1", "class_holo_gen_1_1_u_i_1_1_mask_1_1_model_1_1_d_ellipse.html#abada0e971cd885c061870dfd2d814365", null ],
    [ "P2", "class_holo_gen_1_1_u_i_1_1_mask_1_1_model_1_1_d_ellipse.html#a6a2cd601d246a79c70efef08f9d42211", null ],
    [ "Top", "class_holo_gen_1_1_u_i_1_1_mask_1_1_model_1_1_d_ellipse.html#a18972627e9fad8ddd3ac58671e3b3c4f", null ],
    [ "Width", "class_holo_gen_1_1_u_i_1_1_mask_1_1_model_1_1_d_ellipse.html#a80282d7e89e87a48844a2126c0d7a899", null ]
];