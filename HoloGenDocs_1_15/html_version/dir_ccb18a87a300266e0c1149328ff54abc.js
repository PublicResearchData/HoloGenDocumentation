var dir_ccb18a87a300266e0c1149328ff54abc =
[
    [ "ShapeEllipse.cs", "_shape_ellipse_8cs.html", [
      [ "ShapeEllipse", "class_holo_gen_1_1_mask_1_1_options_1_1_shape_type_1_1_shape_ellipse.html", "class_holo_gen_1_1_mask_1_1_options_1_1_shape_type_1_1_shape_ellipse" ]
    ] ],
    [ "ShapeOption.cs", "_shape_option_8cs.html", [
      [ "ShapeOption", "class_holo_gen_1_1_mask_1_1_options_1_1_shape_type_1_1_shape_option.html", "class_holo_gen_1_1_mask_1_1_options_1_1_shape_type_1_1_shape_option" ]
    ] ],
    [ "ShapePoint.cs", "_shape_point_8cs.html", [
      [ "ShapePoint", "class_holo_gen_1_1_mask_1_1_options_1_1_shape_type_1_1_shape_point.html", "class_holo_gen_1_1_mask_1_1_options_1_1_shape_type_1_1_shape_point" ]
    ] ],
    [ "ShapePolygon.cs", "_shape_polygon_8cs.html", [
      [ "ShapePolygon", "class_holo_gen_1_1_mask_1_1_options_1_1_shape_type_1_1_shape_polygon.html", "class_holo_gen_1_1_mask_1_1_options_1_1_shape_type_1_1_shape_polygon" ]
    ] ],
    [ "ShapePossibilities.cs", "_shape_possibilities_8cs.html", [
      [ "ShapePossibilities", "class_holo_gen_1_1_mask_1_1_options_1_1_shape_type_1_1_shape_possibilities.html", "class_holo_gen_1_1_mask_1_1_options_1_1_shape_type_1_1_shape_possibilities" ]
    ] ],
    [ "ShapePossibility.cs", "_shape_possibility_8cs.html", [
      [ "ShapePossibility", "class_holo_gen_1_1_mask_1_1_options_1_1_shape_type_1_1_shape_possibility.html", null ]
    ] ],
    [ "ShapeRectangle.cs", "_shape_rectangle_8cs.html", [
      [ "ShapeRectangle", "class_holo_gen_1_1_mask_1_1_options_1_1_shape_type_1_1_shape_rectangle.html", "class_holo_gen_1_1_mask_1_1_options_1_1_shape_type_1_1_shape_rectangle" ]
    ] ]
];