var class_holo_gen_1_1_u_i_1_1_menus_1_1_view_models_1_1_file_menu_view_model =
[
    [ "FileMenuViewModel", "class_holo_gen_1_1_u_i_1_1_menus_1_1_view_models_1_1_file_menu_view_model.html#a7a21dccad5fcdeac204976c3ee1650e8", null ],
    [ "CloseCurrentTabCommand", "class_holo_gen_1_1_u_i_1_1_menus_1_1_view_models_1_1_file_menu_view_model.html#a35c11ddb13f13a23a6b247799fa88376", null ],
    [ "ExportImageFileCommand", "class_holo_gen_1_1_u_i_1_1_menus_1_1_view_models_1_1_file_menu_view_model.html#a28c972dae7740a745c7f06c5232bb236", null ],
    [ "ExportMatFileCommand", "class_holo_gen_1_1_u_i_1_1_menus_1_1_view_models_1_1_file_menu_view_model.html#a0e57378913aae57aa611e35a55a43ac6", null ],
    [ "ImportImageFileCommand", "class_holo_gen_1_1_u_i_1_1_menus_1_1_view_models_1_1_file_menu_view_model.html#a8aac7ab78935a15b43d65946b938521f", null ],
    [ "ImportMatFileCommand", "class_holo_gen_1_1_u_i_1_1_menus_1_1_view_models_1_1_file_menu_view_model.html#a6324356735fa27b72cee86c2e1cf7441", null ],
    [ "NewBatchFile", "class_holo_gen_1_1_u_i_1_1_menus_1_1_view_models_1_1_file_menu_view_model.html#a05e7deab145b56deb499e50967201ae7", null ],
    [ "NewSetupFile", "class_holo_gen_1_1_u_i_1_1_menus_1_1_view_models_1_1_file_menu_view_model.html#a61743a23f624f70d935e9473a230e118", null ],
    [ "OpenFileCommand", "class_holo_gen_1_1_u_i_1_1_menus_1_1_view_models_1_1_file_menu_view_model.html#ac889443c5b2c7271e6e0056433261caa", null ],
    [ "SaveAsFileCommand", "class_holo_gen_1_1_u_i_1_1_menus_1_1_view_models_1_1_file_menu_view_model.html#a99737b0532b72cb66fe22b57ec1fa0fd", null ],
    [ "SaveFileCommand", "class_holo_gen_1_1_u_i_1_1_menus_1_1_view_models_1_1_file_menu_view_model.html#a98865d0ffc5ad171c9abad443bbe04b7", null ]
];