var class_holo_gen_1_1_image_1_1_options_1_1_scale_location_possibilities =
[
    [ "ScaleLocationBottom", "class_holo_gen_1_1_image_1_1_options_1_1_scale_location_possibilities.html#a3c4481cdf5a23cd8377124d7359ff566", null ],
    [ "ScaleLocationLeft", "class_holo_gen_1_1_image_1_1_options_1_1_scale_location_possibilities.html#aeb74c4f08786230e4ba600051e259e07", null ],
    [ "ScaleLocationNone", "class_holo_gen_1_1_image_1_1_options_1_1_scale_location_possibilities.html#a726c29d02e112b0ae7aae63b8d802f2c", null ],
    [ "ScaleLocationRight", "class_holo_gen_1_1_image_1_1_options_1_1_scale_location_possibilities.html#aa5a2d75075dfc8753df872aad6b95738", null ],
    [ "ScaleLocationTop", "class_holo_gen_1_1_image_1_1_options_1_1_scale_location_possibilities.html#a46eaabaffcb07942caa9dfbc19e60fcc", null ]
];