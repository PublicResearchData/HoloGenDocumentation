var dir_5efa72fe2fc40ef2535e8da3f39ad414 =
[
    [ "Convertors", "dir_12dd083b6d389c1bfa4b93998672c261.html", "dir_12dd083b6d389c1bfa4b93998672c261" ],
    [ "FileLoader.cs", "_file_loader_8cs.html", [
      [ "FileLoader", "class_holo_gen_1_1_i_o_1_1_file_loader.html", "class_holo_gen_1_1_i_o_1_1_file_loader" ]
    ] ],
    [ "FileSaver.cs", "_file_saver_8cs.html", [
      [ "FileSaver", "class_holo_gen_1_1_i_o_1_1_file_saver.html", "class_holo_gen_1_1_i_o_1_1_file_saver" ]
    ] ],
    [ "JsonFileLoader.cs", "_json_file_loader_8cs.html", [
      [ "JsonFileLoader", "class_holo_gen_1_1_i_o_1_1_json_file_loader.html", "class_holo_gen_1_1_i_o_1_1_json_file_loader" ]
    ] ],
    [ "JsonFileSaver.cs", "_json_file_saver_8cs.html", [
      [ "JsonFileSaver", "class_holo_gen_1_1_i_o_1_1_json_file_saver.html", "class_holo_gen_1_1_i_o_1_1_json_file_saver" ]
    ] ],
    [ "JsonImageFileLoader.cs", "_json_image_file_loader_8cs.html", [
      [ "JsonImageFileSaver", "class_holo_gen_1_1_i_o_1_1_json_image_file_saver.html", null ]
    ] ],
    [ "JsonImageFileSaver.cs", "_json_image_file_saver_8cs.html", [
      [ "JsonImageFileLoader", "class_holo_gen_1_1_i_o_1_1_json_image_file_loader.html", null ]
    ] ],
    [ "JsonOptionsFileLoader.cs.cs", "_json_options_file_loader_8cs_8cs.html", [
      [ "JsonOptionsFileLoader", "class_holo_gen_1_1_i_o_1_1_json_options_file_loader.html", null ]
    ] ],
    [ "JsonOptionsFileSaver.cs", "_json_options_file_saver_8cs.html", [
      [ "JsonOptionsFileSaver", "class_holo_gen_1_1_i_o_1_1_json_options_file_saver.html", null ]
    ] ],
    [ "JsonSettingsFileLoader.cs", "_json_settings_file_loader_8cs.html", [
      [ "JsonSettingsFileLoader", "class_holo_gen_1_1_i_o_1_1_json_settings_file_loader.html", null ]
    ] ],
    [ "JsonSettingsFileSaver.cs", "_json_settings_file_saver_8cs.html", [
      [ "JsonSettingsFileSaver", "class_holo_gen_1_1_i_o_1_1_json_settings_file_saver.html", null ]
    ] ]
];