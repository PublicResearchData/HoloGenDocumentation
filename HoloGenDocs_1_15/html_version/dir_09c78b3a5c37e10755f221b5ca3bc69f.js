var dir_09c78b3a5c37e10755f221b5ca3bc69f =
[
    [ "HologramOptionsView.xaml.cs", "_hologram_options_view_8xaml_8cs.html", "_hologram_options_view_8xaml_8cs" ],
    [ "HologramTabView.xaml.cs", "_hologram_tab_view_8xaml_8cs.html", [
      [ "HologramTabView", "class_holo_gen_1_1_u_i_1_1_views_1_1_hologram_tab_view.html", "class_holo_gen_1_1_u_i_1_1_views_1_1_hologram_tab_view" ]
    ] ],
    [ "HologramView.xaml.cs", "_hologram_view_8xaml_8cs.html", "_hologram_view_8xaml_8cs" ],
    [ "OptionsCommandsPanel.xaml.cs", "_options_commands_panel_8xaml_8cs.html", [
      [ "OptionsCommandsPanel", "class_holo_gen_1_1_u_i_1_1_views_1_1_options_commands_panel.html", "class_holo_gen_1_1_u_i_1_1_views_1_1_options_commands_panel" ]
    ] ],
    [ "PdfTabView.xaml.cs", "_pdf_tab_view_8xaml_8cs.html", [
      [ "PdfTabView", "class_holo_gen_1_1_u_i_1_1_views_1_1_pdf_tab_view.html", "class_holo_gen_1_1_u_i_1_1_views_1_1_pdf_tab_view" ]
    ] ],
    [ "PdfView.xaml.cs", "_pdf_view_8xaml_8cs.html", "_pdf_view_8xaml_8cs" ],
    [ "ProcessTabView.xaml.cs", "_process_tab_view_8xaml_8cs.html", [
      [ "ProcessTabView", "class_holo_gen_1_1_u_i_1_1_views_1_1_process_tab_view.html", "class_holo_gen_1_1_u_i_1_1_views_1_1_process_tab_view" ]
    ] ],
    [ "ResetControl.xaml.cs", "_reset_control_8xaml_8cs.html", [
      [ "ResetControl", "class_holo_gen_1_1_u_i_1_1_views_1_1_reset_control.html", "class_holo_gen_1_1_u_i_1_1_views_1_1_reset_control" ]
    ] ],
    [ "RunControl.xaml.cs", "_run_control_8xaml_8cs.html", [
      [ "RunControl", "class_holo_gen_1_1_u_i_1_1_views_1_1_run_control.html", "class_holo_gen_1_1_u_i_1_1_views_1_1_run_control" ]
    ] ],
    [ "SetupTabView.xaml.cs", "_setup_tab_view_8xaml_8cs.html", [
      [ "SetupTabView", "class_holo_gen_1_1_u_i_1_1_views_1_1_setup_tab_view.html", "class_holo_gen_1_1_u_i_1_1_views_1_1_setup_tab_view" ]
    ] ],
    [ "SetupView.xaml.cs", "_setup_view_8xaml_8cs.html", "_setup_view_8xaml_8cs" ],
    [ "ShowMetadataControl.xaml.cs", "_show_metadata_control_8xaml_8cs.html", [
      [ "ShowMetadataControl", "class_holo_gen_1_1_u_i_1_1_views_1_1_show_metadata_control.html", "class_holo_gen_1_1_u_i_1_1_views_1_1_show_metadata_control" ]
    ] ]
];