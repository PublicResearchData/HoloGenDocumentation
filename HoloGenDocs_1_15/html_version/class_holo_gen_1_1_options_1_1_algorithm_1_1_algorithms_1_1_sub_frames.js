var class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_sub_frames =
[
    [ "Default", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_sub_frames.html#a3dbdb145d24bb75455658016b801574c", null ],
    [ "MaxValue", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_sub_frames.html#a5d9c57ca8517d7c7dcb5fbdfbf53263f", null ],
    [ "MinValue", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_sub_frames.html#ab35dc07552903944f48db669fcc529dd", null ],
    [ "Name", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_sub_frames.html#a2fecbeaef3ce1713c4a917cbbaf7b720", null ],
    [ "ToolTip", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_sub_frames.html#a790dc25d5b28e20c3410c951554f617a", null ],
    [ "Watermark", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_sub_frames.html#a2063357728fa1198c04699f172602887", null ]
];