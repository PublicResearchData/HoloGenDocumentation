var dir_5aad1cb7ecf46f8ce1aba5a3e17335f3 =
[
    [ "ErrorType1.cs", "_error_type1_8cs.html", [
      [ "ErrorType1", "class_holo_gen_1_1_options_1_1_algorithm_1_1_error_calculation_1_1_type_1_1_error_type1.html", "class_holo_gen_1_1_options_1_1_algorithm_1_1_error_calculation_1_1_type_1_1_error_type1" ]
    ] ],
    [ "ErrorType2.cs", "_error_type2_8cs.html", [
      [ "ErrorType2", "class_holo_gen_1_1_options_1_1_algorithm_1_1_error_calculation_1_1_type_1_1_error_type2.html", "class_holo_gen_1_1_options_1_1_algorithm_1_1_error_calculation_1_1_type_1_1_error_type2" ]
    ] ],
    [ "ErrorType3.cs", "_error_type3_8cs.html", [
      [ "ErrorType3", "class_holo_gen_1_1_options_1_1_algorithm_1_1_error_calculation_1_1_type_1_1_error_type3.html", "class_holo_gen_1_1_options_1_1_algorithm_1_1_error_calculation_1_1_type_1_1_error_type3" ]
    ] ],
    [ "ErrorTypeOption.cs", "_error_type_option_8cs.html", [
      [ "ErrorTypeOption", "class_holo_gen_1_1_options_1_1_algorithm_1_1_error_calculation_1_1_type_1_1_error_type_option.html", "class_holo_gen_1_1_options_1_1_algorithm_1_1_error_calculation_1_1_type_1_1_error_type_option" ]
    ] ],
    [ "ErrorTypePossibilities.cs", "_error_type_possibilities_8cs.html", [
      [ "ErrorTypePossibilities", "class_holo_gen_1_1_options_1_1_algorithm_1_1_error_calculation_1_1_type_1_1_error_type_possibilities.html", "class_holo_gen_1_1_options_1_1_algorithm_1_1_error_calculation_1_1_type_1_1_error_type_possibilities" ]
    ] ],
    [ "ErrorTypePossibility.cs", "_error_type_possibility_8cs.html", [
      [ "ErrorTypePossibility", "class_holo_gen_1_1_options_1_1_algorithm_1_1_error_calculation_1_1_type_1_1_error_type_possibility.html", "class_holo_gen_1_1_options_1_1_algorithm_1_1_error_calculation_1_1_type_1_1_error_type_possibility" ]
    ] ]
];