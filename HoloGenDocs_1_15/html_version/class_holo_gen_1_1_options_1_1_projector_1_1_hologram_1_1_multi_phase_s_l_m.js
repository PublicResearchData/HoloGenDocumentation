var class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_multi_phase_s_l_m =
[
    [ "Name", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_multi_phase_s_l_m.html#a7566b7fb3b7d6b28d2d7013d7d8199c5", null ],
    [ "ToolTip", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_multi_phase_s_l_m.html#ae6896470ab8bc4ccdc61b7db5f6a3ae9", null ],
    [ "MaximumAngle", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_multi_phase_s_l_m.html#aaec98cfb5f76bd4aaf42af225ec5b704", null ],
    [ "MinimumAngle", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_multi_phase_s_l_m.html#a6fd9c01ffbe1feb1fa05887cc667a75f", null ],
    [ "SLMLevels", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_multi_phase_s_l_m.html#a97fe6abf3ab68ccbcf1c896b46876bc1", null ]
];