var namespace_holo_gen_1_1_mask_1_1_options =
[
    [ "MouseType", "namespace_holo_gen_1_1_mask_1_1_options_1_1_mouse_type.html", "namespace_holo_gen_1_1_mask_1_1_options_1_1_mouse_type" ],
    [ "ShapeType", "namespace_holo_gen_1_1_mask_1_1_options_1_1_shape_type.html", "namespace_holo_gen_1_1_mask_1_1_options_1_1_shape_type" ],
    [ "MaskedImageOptions", "class_holo_gen_1_1_mask_1_1_options_1_1_masked_image_options.html", "class_holo_gen_1_1_mask_1_1_options_1_1_masked_image_options" ],
    [ "MaskFolder", "class_holo_gen_1_1_mask_1_1_options_1_1_mask_folder.html", "class_holo_gen_1_1_mask_1_1_options_1_1_mask_folder" ]
];