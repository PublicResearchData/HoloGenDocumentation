var class_holo_gen_1_1_options_1_1_output_1_1_reference_1_1_reference_text =
[
    [ "ReferenceText", "class_holo_gen_1_1_options_1_1_output_1_1_reference_1_1_reference_text.html#a3845a818205ead74a94b978f1bc0b0e8", null ],
    [ "CanClear", "class_holo_gen_1_1_options_1_1_output_1_1_reference_1_1_reference_text.html#ae7b37e8b6c5b23b22429309cb3fedd3a", null ],
    [ "Default", "class_holo_gen_1_1_options_1_1_output_1_1_reference_1_1_reference_text.html#a41a37d98a3ab21b37ca8c71db901c4c6", null ],
    [ "Name", "class_holo_gen_1_1_options_1_1_output_1_1_reference_1_1_reference_text.html#ad7d36cbecc8f9aade7f065952186b99c", null ],
    [ "ToolTip", "class_holo_gen_1_1_options_1_1_output_1_1_reference_1_1_reference_text.html#a4e313cbc1638fd50550eae2c99b58320", null ],
    [ "Watermark", "class_holo_gen_1_1_options_1_1_output_1_1_reference_1_1_reference_text.html#a85890efe62d9a57eb6eb746255e00f6c", null ]
];