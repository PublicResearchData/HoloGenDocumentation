var namespace_holo_gen =
[
    [ "Alg", "namespace_holo_gen_1_1_alg.html", "namespace_holo_gen_1_1_alg" ],
    [ "Controller", "namespace_holo_gen_1_1_controller.html", "namespace_holo_gen_1_1_controller" ],
    [ "Hierarchy", "namespace_holo_gen_1_1_hierarchy.html", "namespace_holo_gen_1_1_hierarchy" ],
    [ "Image", "namespace_holo_gen_1_1_image.html", "namespace_holo_gen_1_1_image" ],
    [ "IO", "namespace_holo_gen_1_1_i_o.html", "namespace_holo_gen_1_1_i_o" ],
    [ "Mask", "namespace_holo_gen_1_1_mask.html", "namespace_holo_gen_1_1_mask" ],
    [ "Options", "namespace_holo_gen_1_1_options.html", "namespace_holo_gen_1_1_options" ],
    [ "Process", "namespace_holo_gen_1_1_process.html", "namespace_holo_gen_1_1_process" ],
    [ "Serial", "namespace_holo_gen_1_1_serial.html", "namespace_holo_gen_1_1_serial" ],
    [ "Settings", "namespace_holo_gen_1_1_settings.html", "namespace_holo_gen_1_1_settings" ],
    [ "UI", "namespace_holo_gen_1_1_u_i.html", "namespace_holo_gen_1_1_u_i" ],
    [ "Utils", "namespace_holo_gen_1_1_utils.html", "namespace_holo_gen_1_1_utils" ],
    [ "Program", "class_holo_gen_1_1_program.html", null ]
];