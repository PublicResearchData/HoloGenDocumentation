var class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_hologram_folder =
[
    [ "HologramFolder", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_hologram_folder.html#af5d15b0cb52e566593829c856d232b15", null ],
    [ "Name", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_hologram_folder.html#ab18029aaacf899a3d02d565d3111315e", null ],
    [ "ToolTip", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_hologram_folder.html#a992d0db8a53bdbbb19e676f47d4ac612", null ],
    [ "FocalLength", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_hologram_folder.html#aecb36f7bef2703e2ba21518e48bc19bd", null ],
    [ "Oversampling", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_hologram_folder.html#a1e6154405d1d0a190ec1518042ef400b", null ],
    [ "PixelPitch", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_hologram_folder.html#a5c7e01868485f32596eb8acea50cbcb1", null ],
    [ "SeedOption", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_hologram_folder.html#ac52b0628749ec838dfa2ff0a2d1dfa13", null ],
    [ "SLMResolutionX", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_hologram_folder.html#a8dc10a0fd2ccb034c1c8522c015f3cb7", null ],
    [ "SLMResolutionY", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_hologram_folder.html#a92a8fdf92184bb15db7ed28fe54b31dd", null ],
    [ "SLMTypeOption", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_hologram_folder.html#a3564779e7a8ab179c7b865346b5f47cf", null ],
    [ "Wavelength", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_hologram_folder.html#a80031dbc24d3f74460716690a6be177f", null ]
];