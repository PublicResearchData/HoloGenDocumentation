var class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_s_l_m_levels =
[
    [ "Default", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_s_l_m_levels.html#a7ff1a6054c64a86f70f9cc3c1e532c71", null ],
    [ "MaxValue", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_s_l_m_levels.html#a6ff268e6b01ada51dc25751e16a4f966", null ],
    [ "MinValue", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_s_l_m_levels.html#a76ddb2519c243092476351431f3cb375", null ],
    [ "Name", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_s_l_m_levels.html#a34ed346598fa13d5c38e460917a3cbf3", null ],
    [ "ToolTip", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_s_l_m_levels.html#a60786a99e2c0b1439bc6721aa517d381", null ],
    [ "Watermark", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_s_l_m_levels.html#af27c0861c9f5d749ffc87ecfc0af66d7", null ]
];