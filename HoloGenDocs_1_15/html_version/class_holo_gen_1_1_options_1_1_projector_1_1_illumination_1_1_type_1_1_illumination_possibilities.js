var class_holo_gen_1_1_options_1_1_projector_1_1_illumination_1_1_type_1_1_illumination_possibilities =
[
    [ "IlluminationFromFile", "class_holo_gen_1_1_options_1_1_projector_1_1_illumination_1_1_type_1_1_illumination_possibilities.html#ae9d575da03f08819cfac54f190cedaef", null ],
    [ "IlluminationGaussian", "class_holo_gen_1_1_options_1_1_projector_1_1_illumination_1_1_type_1_1_illumination_possibilities.html#a4e8e9397abf71d236fcbe51b6ba96444", null ],
    [ "IlluminationPlanar", "class_holo_gen_1_1_options_1_1_projector_1_1_illumination_1_1_type_1_1_illumination_possibilities.html#a322ae44c474260a50cf4b9ce163ab6a1", null ]
];