var namespace_holo_gen_1_1_options_1_1_algorithm_1_1_target_1_1_type =
[
    [ "TargetBestEfficiency", "class_holo_gen_1_1_options_1_1_algorithm_1_1_target_1_1_type_1_1_target_best_efficiency.html", "class_holo_gen_1_1_options_1_1_algorithm_1_1_target_1_1_type_1_1_target_best_efficiency" ],
    [ "TargetBestError", "class_holo_gen_1_1_options_1_1_algorithm_1_1_target_1_1_type_1_1_target_best_error.html", "class_holo_gen_1_1_options_1_1_algorithm_1_1_target_1_1_type_1_1_target_best_error" ],
    [ "TargetOption", "class_holo_gen_1_1_options_1_1_algorithm_1_1_target_1_1_type_1_1_target_option.html", "class_holo_gen_1_1_options_1_1_algorithm_1_1_target_1_1_type_1_1_target_option" ],
    [ "TargetPossibilities", "class_holo_gen_1_1_options_1_1_algorithm_1_1_target_1_1_type_1_1_target_possibilities.html", "class_holo_gen_1_1_options_1_1_algorithm_1_1_target_1_1_type_1_1_target_possibilities" ],
    [ "TargetPossibility", "class_holo_gen_1_1_options_1_1_algorithm_1_1_target_1_1_type_1_1_target_possibility.html", null ]
];