var dir_39da25f943e7d35ccb4ea4f77996029c =
[
    [ "AbstractTabViewModel.cs", "_abstract_tab_view_model_8cs.html", [
      [ "AbstractTabViewModel", "class_holo_gen_1_1_u_i_1_1_view_models_1_1_abstract_tab_view_model.html", "class_holo_gen_1_1_u_i_1_1_view_models_1_1_abstract_tab_view_model" ]
    ] ],
    [ "BatchTabViewModel.cs", "_batch_tab_view_model_8cs.html", [
      [ "SetupTabViewModel", "class_holo_gen_1_1_u_i_1_1_view_models_1_1_setup_tab_view_model.html", "class_holo_gen_1_1_u_i_1_1_view_models_1_1_setup_tab_view_model" ]
    ] ],
    [ "BatchViewModel.cs", "_batch_view_model_8cs.html", [
      [ "SetupViewModel", "class_holo_gen_1_1_u_i_1_1_view_models_1_1_setup_view_model.html", "class_holo_gen_1_1_u_i_1_1_view_models_1_1_setup_view_model" ]
    ] ],
    [ "HologramTabViewModel.cs", "_hologram_tab_view_model_8cs.html", [
      [ "HologramTabViewModel", "class_holo_gen_1_1_u_i_1_1_view_models_1_1_hologram_tab_view_model.html", "class_holo_gen_1_1_u_i_1_1_view_models_1_1_hologram_tab_view_model" ]
    ] ],
    [ "HologramViewModel.cs", "_hologram_view_model_8cs.html", [
      [ "HologramViewModel", "class_holo_gen_1_1_u_i_1_1_view_models_1_1_hologram_view_model.html", "class_holo_gen_1_1_u_i_1_1_view_models_1_1_hologram_view_model" ]
    ] ],
    [ "PdfTabViewModel.cs", "_pdf_tab_view_model_8cs.html", [
      [ "PdfTabViewModel", "class_holo_gen_1_1_u_i_1_1_view_models_1_1_pdf_tab_view_model.html", "class_holo_gen_1_1_u_i_1_1_view_models_1_1_pdf_tab_view_model" ]
    ] ],
    [ "PdfViewModel.cs", "_pdf_view_model_8cs.html", [
      [ "PdfViewModel", "class_holo_gen_1_1_u_i_1_1_view_models_1_1_pdf_view_model.html", "class_holo_gen_1_1_u_i_1_1_view_models_1_1_pdf_view_model" ]
    ] ],
    [ "ProcessTabViewModel.cs", "_process_tab_view_model_8cs.html", [
      [ "ProcessTabViewModel", "class_holo_gen_1_1_u_i_1_1_view_models_1_1_process_tab_view_model.html", "class_holo_gen_1_1_u_i_1_1_view_models_1_1_process_tab_view_model" ]
    ] ],
    [ "SetupTabViewModel.cs", "_setup_tab_view_model_8cs.html", [
      [ "BatchTabViewModel", "class_holo_gen_1_1_u_i_1_1_view_models_1_1_batch_tab_view_model.html", "class_holo_gen_1_1_u_i_1_1_view_models_1_1_batch_tab_view_model" ]
    ] ],
    [ "SetupViewModel.cs", "_setup_view_model_8cs.html", [
      [ "BatchViewModel", "class_holo_gen_1_1_u_i_1_1_view_models_1_1_batch_view_model.html", "class_holo_gen_1_1_u_i_1_1_view_models_1_1_batch_view_model" ]
    ] ]
];