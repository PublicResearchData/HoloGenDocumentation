var class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_type_1_1_region_intensity =
[
    [ "Name", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_type_1_1_region_intensity.html#a1339044e02369ae2f0e902d840466065", null ],
    [ "ToolTip", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_type_1_1_region_intensity.html#abf99cc93e9896c3ea0792a32ba206237", null ],
    [ "PixelCount", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_type_1_1_region_intensity.html#a1ae54015e9e4967d9f3d1a177d869cd8", null ],
    [ "SymmetryOption", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_type_1_1_region_intensity.html#aabeebe2fd0ef39e6e63e5f8db505082e", null ],
    [ "VariantOption", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_type_1_1_region_intensity.html#a2c3d4843f4d4abc082c5f152f8489afb", null ]
];