var class_holo_gen_1_1_u_i_1_1_view_models_1_1_batch_tab_view_model =
[
    [ "BatchTabViewModel", "class_holo_gen_1_1_u_i_1_1_view_models_1_1_batch_tab_view_model.html#ae6f8249a45064461d0d4873f985c3a5c", null ],
    [ "BatchViewModel", "class_holo_gen_1_1_u_i_1_1_view_models_1_1_batch_tab_view_model.html#a1bff0aed10e0d06e65a606b7a7cd75fc", null ],
    [ "IsFileBased", "class_holo_gen_1_1_u_i_1_1_view_models_1_1_batch_tab_view_model.html#a26fb7a1a8d1a83f268ecb917423c2573", null ],
    [ "IsImageExportable", "class_holo_gen_1_1_u_i_1_1_view_models_1_1_batch_tab_view_model.html#ad8897f257aaa3a7b173a3b67af1cd0b4", null ],
    [ "SaveableData", "class_holo_gen_1_1_u_i_1_1_view_models_1_1_batch_tab_view_model.html#a41d10cd56127b6d551b2986da4ff5245", null ]
];