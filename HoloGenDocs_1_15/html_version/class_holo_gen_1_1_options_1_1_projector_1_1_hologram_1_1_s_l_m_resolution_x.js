var class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_s_l_m_resolution_x =
[
    [ "Default", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_s_l_m_resolution_x.html#a4caba4e870e6ed9c3806c7033246ea85", null ],
    [ "MaxValue", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_s_l_m_resolution_x.html#af1948668f9c0fe0921ef0187dbcf3aba", null ],
    [ "MinValue", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_s_l_m_resolution_x.html#a3a84fd3e2bf45c4a898abf4fa6559c33", null ],
    [ "Name", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_s_l_m_resolution_x.html#aec6131754412eb9dd999db993d79ab58", null ],
    [ "ToolTip", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_s_l_m_resolution_x.html#afb0fc368f36863b7604b03ff03f1a2f1", null ],
    [ "Watermark", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_s_l_m_resolution_x.html#a01badaf6552d61612d67d103b7712f12", null ]
];