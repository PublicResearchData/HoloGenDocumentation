var dir_3ffb452e0de405542eaa871fe1f3f3e5 =
[
    [ "DSVariantOption.cs", "_d_s_variant_option_8cs.html", [
      [ "DSVariantOption", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_variants_1_1_d_s_1_1_d_s_variant_option.html", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_variants_1_1_d_s_1_1_d_s_variant_option" ]
    ] ],
    [ "DSVariantPossibilities.cs", "_d_s_variant_possibilities_8cs.html", [
      [ "DSVariantPossibilities", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_variants_1_1_d_s_1_1_d_s_variant_possibilities.html", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_variants_1_1_d_s_1_1_d_s_variant_possibilities" ]
    ] ],
    [ "DSVariantPossibility.cs", "_d_s_variant_possibility_8cs.html", [
      [ "DSVariantPossibility", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_variants_1_1_d_s_1_1_d_s_variant_possibility.html", null ]
    ] ],
    [ "StandardDSVariant.cs", "_standard_d_s_variant_8cs.html", [
      [ "StandardDSVariant", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_variants_1_1_d_s_1_1_standard_d_s_variant.html", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_variants_1_1_d_s_1_1_standard_d_s_variant" ]
    ] ]
];