var class_holo_gen_1_1_settings_1_1_general_1_1_file_1_1_show_large_mat_import_warning =
[
    [ "Default", "class_holo_gen_1_1_settings_1_1_general_1_1_file_1_1_show_large_mat_import_warning.html#a8fd257a301bd7b2aa0fb0a3b6e6e497b", null ],
    [ "Name", "class_holo_gen_1_1_settings_1_1_general_1_1_file_1_1_show_large_mat_import_warning.html#ac13c042ab4b63b215fd1320332550f5d", null ],
    [ "OffText", "class_holo_gen_1_1_settings_1_1_general_1_1_file_1_1_show_large_mat_import_warning.html#a8308c575dda9f4661717e2370906d824", null ],
    [ "OnText", "class_holo_gen_1_1_settings_1_1_general_1_1_file_1_1_show_large_mat_import_warning.html#a634bd849f01ae70f61a67b17e566f1aa", null ],
    [ "ToolTip", "class_holo_gen_1_1_settings_1_1_general_1_1_file_1_1_show_large_mat_import_warning.html#ae4a7475701cf7816a5252bb926de2cd2", null ],
    [ "Watermark", "class_holo_gen_1_1_settings_1_1_general_1_1_file_1_1_show_large_mat_import_warning.html#ac3d39384e5482cc209ac4f0774c54c14", null ]
];