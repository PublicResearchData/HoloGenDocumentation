var class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_s_l_m_resolution_y =
[
    [ "Default", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_s_l_m_resolution_y.html#a83c0ae2fb4732613425ae862b65550ef", null ],
    [ "MaxValue", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_s_l_m_resolution_y.html#af013e44d7cd08509ae3cb56afaabf9b9", null ],
    [ "MinValue", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_s_l_m_resolution_y.html#a58f4d22c93fd17a41d3169bfa3e02989", null ],
    [ "Name", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_s_l_m_resolution_y.html#a61a47294050aff45dbc8500c1ea84e50", null ],
    [ "ToolTip", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_s_l_m_resolution_y.html#aa7105d38bdaba08d4b1214cb6ffef886", null ],
    [ "Watermark", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_s_l_m_resolution_y.html#a12b57665a456b10a3481a5e306a11b75", null ]
];