var dir_808e9b9f555f839c0f69b424bda00a66 =
[
    [ "Type", "dir_3edeae574e92825af0b1aef8a61b320a.html", "dir_3edeae574e92825af0b1aef8a61b320a" ],
    [ "EfficiencyLimit.cs", "_efficiency_limit_8cs.html", [
      [ "EfficiencyLimit", "class_holo_gen_1_1_options_1_1_algorithm_1_1_target_1_1_efficiency_limit.html", "class_holo_gen_1_1_options_1_1_algorithm_1_1_target_1_1_efficiency_limit" ]
    ] ],
    [ "ErrorLimit.cs", "_error_limit_8cs.html", [
      [ "ErrorLimit", "class_holo_gen_1_1_options_1_1_algorithm_1_1_target_1_1_error_limit.html", "class_holo_gen_1_1_options_1_1_algorithm_1_1_target_1_1_error_limit" ]
    ] ],
    [ "Target.cs", "_target_8cs.html", [
      [ "Target", "class_holo_gen_1_1_options_1_1_algorithm_1_1_target_1_1_target.html", "class_holo_gen_1_1_options_1_1_algorithm_1_1_target_1_1_target" ]
    ] ],
    [ "TargetFolder.cs", "_algorithm_2_target_2_target_folder_8cs.html", [
      [ "TargetFolder", "class_holo_gen_1_1_options_1_1_algorithm_1_1_target_1_1_target_folder.html", "class_holo_gen_1_1_options_1_1_algorithm_1_1_target_1_1_target_folder" ]
    ] ]
];