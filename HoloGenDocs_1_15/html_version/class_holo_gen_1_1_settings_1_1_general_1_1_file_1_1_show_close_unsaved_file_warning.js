var class_holo_gen_1_1_settings_1_1_general_1_1_file_1_1_show_close_unsaved_file_warning =
[
    [ "Default", "class_holo_gen_1_1_settings_1_1_general_1_1_file_1_1_show_close_unsaved_file_warning.html#a41003056971ffda1206fcf421aadfa94", null ],
    [ "Name", "class_holo_gen_1_1_settings_1_1_general_1_1_file_1_1_show_close_unsaved_file_warning.html#a86dd8bb30947ca4ff1c8cec8307b2841", null ],
    [ "OffText", "class_holo_gen_1_1_settings_1_1_general_1_1_file_1_1_show_close_unsaved_file_warning.html#a28d343c9b7411b9e9b7994a7cb08ad0b", null ],
    [ "OnText", "class_holo_gen_1_1_settings_1_1_general_1_1_file_1_1_show_close_unsaved_file_warning.html#a57599924b562e525da6f4541a4103a34", null ],
    [ "ToolTip", "class_holo_gen_1_1_settings_1_1_general_1_1_file_1_1_show_close_unsaved_file_warning.html#aa9b8f4166eae99b09468625fbeb8eb47", null ],
    [ "Watermark", "class_holo_gen_1_1_settings_1_1_general_1_1_file_1_1_show_close_unsaved_file_warning.html#a78c622cf136a0b0f301e03005537b2d8", null ]
];