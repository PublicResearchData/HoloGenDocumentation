/*
@ @licstart  The following is the entire license notice for the
JavaScript code in this file.

Copyright (C) 1997-2017 by Dimitri van Heesch

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

@licend  The above is the entire license notice
for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "HoloGen Documentation", "index.html", [
    [ "Packages", "namespaces.html", [
      [ "Packages", "namespaces.html", "namespaces_dup" ],
      [ "Package Functions", "namespacemembers.html", [
        [ "All", "namespacemembers.html", null ],
        [ "Functions", "namespacemembers_func.html", null ],
        [ "Enumerations", "namespacemembers_enum.html", null ]
      ] ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", "functions_func" ],
        [ "Variables", "functions_vars.html", "functions_vars" ],
        [ "Properties", "functions_prop.html", "functions_prop" ],
        [ "Events", "functions_evnt.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ],
      [ "File Members", "globals.html", [
        [ "All", "globals.html", null ],
        [ "Typedefs", "globals_type.html", null ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"_abstract_menu_view_model_8cs.html",
"_image_view_real_8cs.html",
"_target_option_8cs.html",
"class_holo_gen_1_1_image_1_1_complex_image_version.html",
"class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_d_s_algorithm.html#abb9a114af229cd8dbfd3e053bbcd9854",
"class_holo_gen_1_1_options_1_1_hardware_1_1_hardware_1_1_type_1_1_hardware_option.html#a013e80aae5d8d41097cd80c130e9f142",
"class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_seed_possibilities.html#ae50211019237839971dba5a7acf1a4ad",
"class_holo_gen_1_1_settings_1_1_general_1_1_file_1_1_show_large_mat_export_warning.html#aeb427444c9c2f86f92046d62af83fb6a",
"class_holo_gen_1_1_u_i_1_1_mask_1_1_mask_viewer.html#a62cfc6103ba846a11991b1aa7f0fcbde",
"class_holo_gen_1_1_u_i_1_1_view_models_1_1_process_tab_view_model.html#ad5b57eee646253e8bd56a1107f575e17",
"functions_prop_p.html",
"namespace_holo_gen_1_1_u_i_1_1_image_1_1_viewer.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';