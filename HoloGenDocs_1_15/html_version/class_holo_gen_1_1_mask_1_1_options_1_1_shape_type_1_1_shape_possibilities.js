var class_holo_gen_1_1_mask_1_1_options_1_1_shape_type_1_1_shape_possibilities =
[
    [ "ShapeEllipse", "class_holo_gen_1_1_mask_1_1_options_1_1_shape_type_1_1_shape_possibilities.html#a2c02dc8d5a1035b87c48dfcf004e5e4b", null ],
    [ "ShapePoint", "class_holo_gen_1_1_mask_1_1_options_1_1_shape_type_1_1_shape_possibilities.html#adabc55d9b1bd43737baa8337be06468b", null ],
    [ "ShapePolygon", "class_holo_gen_1_1_mask_1_1_options_1_1_shape_type_1_1_shape_possibilities.html#aa3f93f48c732b09081702ade7e92830d", null ],
    [ "ShapeRectangle", "class_holo_gen_1_1_mask_1_1_options_1_1_shape_type_1_1_shape_possibilities.html#afa43610bd3e2cc122f19f8ba1269b961", null ]
];