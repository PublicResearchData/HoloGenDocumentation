var dir_6c1635440837e1cba0a5cdbd536fdf5a =
[
    [ "AccentMenuView.xaml.cs", "_accent_menu_view_8xaml_8cs.html", [
      [ "AccentMenuView", "class_holo_gen_1_1_u_i_1_1_menus_1_1_views_1_1_accent_menu_view.html", "class_holo_gen_1_1_u_i_1_1_menus_1_1_views_1_1_accent_menu_view" ]
    ] ],
    [ "FileMenuView.xaml.cs", "_file_menu_view_8xaml_8cs.html", [
      [ "FileMenuView", "class_holo_gen_1_1_u_i_1_1_menus_1_1_views_1_1_file_menu_view.html", "class_holo_gen_1_1_u_i_1_1_menus_1_1_views_1_1_file_menu_view" ]
    ] ],
    [ "HelpMenuView.xaml.cs", "_help_menu_view_8xaml_8cs.html", [
      [ "HelpMenuView", "class_holo_gen_1_1_u_i_1_1_menus_1_1_views_1_1_help_menu_view.html", "class_holo_gen_1_1_u_i_1_1_menus_1_1_views_1_1_help_menu_view" ]
    ] ],
    [ "MenuView.xaml.cs", "_menu_view_8xaml_8cs.html", [
      [ "MenuView", "class_holo_gen_1_1_u_i_1_1_menus_1_1_views_1_1_menu_view.html", "class_holo_gen_1_1_u_i_1_1_menus_1_1_views_1_1_menu_view" ]
    ] ],
    [ "SettingsMenuPageView.xaml.cs", "_settings_menu_page_view_8xaml_8cs.html", [
      [ "SettingsMenuPageView", "class_holo_gen_1_1_u_i_1_1_menus_1_1_views_1_1_settings_menu_page_view.html", "class_holo_gen_1_1_u_i_1_1_menus_1_1_views_1_1_settings_menu_page_view" ]
    ] ],
    [ "SettingsMenuView.xaml.cs", "_settings_menu_view_8xaml_8cs.html", [
      [ "SettingsMenuView", "class_holo_gen_1_1_u_i_1_1_menus_1_1_views_1_1_settings_menu_view.html", "class_holo_gen_1_1_u_i_1_1_menus_1_1_views_1_1_settings_menu_view" ]
    ] ],
    [ "ThemeMenuView.xaml.cs", "_theme_menu_view_8xaml_8cs.html", [
      [ "ThemeMenuView", "class_holo_gen_1_1_u_i_1_1_menus_1_1_views_1_1_theme_menu_view.html", "class_holo_gen_1_1_u_i_1_1_menus_1_1_views_1_1_theme_menu_view" ]
    ] ]
];