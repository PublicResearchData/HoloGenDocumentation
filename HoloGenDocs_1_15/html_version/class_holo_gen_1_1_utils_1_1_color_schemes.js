var class_holo_gen_1_1_utils_1_1_color_schemes =
[
    [ "BlackonWhite", "class_holo_gen_1_1_utils_1_1_color_schemes.html#af75f739f4ea92d84b3db22cca2db0e1a", null ],
    [ "BlueonBlack", "class_holo_gen_1_1_utils_1_1_color_schemes.html#aefcec8e1ffb6543254ba1d77c4711211", null ],
    [ "BlueonWhite", "class_holo_gen_1_1_utils_1_1_color_schemes.html#afcfc4b162cc8185aa66b03795585fbd8", null ],
    [ "GreenonBlack", "class_holo_gen_1_1_utils_1_1_color_schemes.html#a58a8d311c2af48a399017de40d10f4f9", null ],
    [ "GreenonWhite", "class_holo_gen_1_1_utils_1_1_color_schemes.html#a66f513ecc36d0de003d9367839d3b9f9", null ],
    [ "OrangeonBlack", "class_holo_gen_1_1_utils_1_1_color_schemes.html#a0445eb968cb8dfe2e133cc756a0eda33", null ],
    [ "OrangeonWhite", "class_holo_gen_1_1_utils_1_1_color_schemes.html#a83bb611639998bac884f2874475b454e", null ],
    [ "PurpleonBlack", "class_holo_gen_1_1_utils_1_1_color_schemes.html#af1cb3b5d2204044959e604e9d8370abd", null ],
    [ "PurpleonWhite", "class_holo_gen_1_1_utils_1_1_color_schemes.html#a134e630e82a54cfbb4ed75c5a9282822", null ],
    [ "RedonBlack", "class_holo_gen_1_1_utils_1_1_color_schemes.html#ae30f1b478a4ee3e68f4eb38128e15665", null ],
    [ "RedonWhite", "class_holo_gen_1_1_utils_1_1_color_schemes.html#aa5b02b610c787d557e21fcb1010d6a34", null ],
    [ "WhiteonBlack", "class_holo_gen_1_1_utils_1_1_color_schemes.html#ab7654134e624f672a4a34cb5cb5370cd", null ],
    [ "YellowonBlack", "class_holo_gen_1_1_utils_1_1_color_schemes.html#aa442a669137af9f4d994603d79c1e7c5", null ],
    [ "YellowonWhite", "class_holo_gen_1_1_utils_1_1_color_schemes.html#a8ae5e06e65e0aaba7fc7036f6b8f2c4c", null ]
];