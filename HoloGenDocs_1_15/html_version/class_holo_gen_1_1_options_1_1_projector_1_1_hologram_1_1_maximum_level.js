var class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_maximum_level =
[
    [ "Default", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_maximum_level.html#a36aacbb3a8001baec3c4a3eb6799ed28", null ],
    [ "MaxValue", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_maximum_level.html#ab9af0b7384a8f11522353f5b82931918", null ],
    [ "MinValue", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_maximum_level.html#ad52d4869af57e87c696f08e929039999", null ],
    [ "Name", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_maximum_level.html#a3b71b41de3a75c2ae3e99e45716479a5", null ],
    [ "ToolTip", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_maximum_level.html#a593aad2a062e6e88161996e40976bb7d", null ],
    [ "Watermark", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_maximum_level.html#aff0b9f13a1814d65ab8ccc977680b209", null ]
];