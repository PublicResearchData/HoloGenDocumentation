var dir_ba4f460854edd16462a28f5f262dc276 =
[
    [ "SAVariantOption.cs", "_s_a_variant_option_8cs.html", [
      [ "SAVariantOption", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_variants_1_1_s_a_1_1_s_a_variant_option.html", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_variants_1_1_s_a_1_1_s_a_variant_option" ]
    ] ],
    [ "SAVariantPossibilities.cs", "_s_a_variant_possibilities_8cs.html", [
      [ "SAVariantPossibilities", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_variants_1_1_s_a_1_1_s_a_variant_possibilities.html", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_variants_1_1_s_a_1_1_s_a_variant_possibilities" ]
    ] ],
    [ "SAVariantPossibility.cs", "_s_a_variant_possibility_8cs.html", [
      [ "SAVariantPossibility", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_variants_1_1_s_a_1_1_s_a_variant_possibility.html", null ]
    ] ],
    [ "StandardSAVariant.cs", "_standard_s_a_variant_8cs.html", [
      [ "StandardSAVariant", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_variants_1_1_s_a_1_1_standard_s_a_variant.html", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_variants_1_1_s_a_1_1_standard_s_a_variant" ]
    ] ]
];