var class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_s_a_algorithm =
[
    [ "Name", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_s_a_algorithm.html#a7f0ca9e2cf2f038ef6ae5e6475273a9a", null ],
    [ "ToolTip", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_s_a_algorithm.html#af813224ea9ecff76b54cdcf34d1f9039", null ],
    [ "VariantOption", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_s_a_algorithm.html#a32b94fa5a457533321785090df43e40d", null ],
    [ "StartingTemperature", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_s_a_algorithm.html#a10624b95d86b0cecf3c37938899db9be", null ],
    [ "SubFrames", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_s_a_algorithm.html#a55e485e44f35c466acde64998ee7798a", null ],
    [ "TemperatureCoefficient", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_s_a_algorithm.html#a50b418d81a18cb08a9c23f3aa7830abf", null ],
    [ "VariantOption2", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_s_a_algorithm.html#ab5d9e9095d20885aa2eb8c65380ac2c8", null ]
];