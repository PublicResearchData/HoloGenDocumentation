var class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_oversampling =
[
    [ "Default", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_oversampling.html#a3c7b8237d2a8208af3458d411e783991", null ],
    [ "Enabled", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_oversampling.html#a26f6b845aec5688119fd2ab943157e6c", null ],
    [ "MaxValue", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_oversampling.html#afd09a79ecc6dbe81018d2568ccc72996", null ],
    [ "MinValue", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_oversampling.html#abb9b60584df992dccd3f9249e5c2816c", null ],
    [ "Name", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_oversampling.html#a1674a26ab17c97cbf056a81db727434f", null ],
    [ "ToolTip", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_oversampling.html#a7ec0bf0a9c142573eb0b9f864b9cb28b", null ],
    [ "Watermark", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_oversampling.html#a3d30c592ad19a0db3b12f15cc733e43e", null ]
];