var dir_c844814dfb270cdb1a748b224e5d39a5 =
[
    [ "IlluminationFromFile.cs", "_illumination_from_file_8cs.html", [
      [ "IlluminationFromFile", "class_holo_gen_1_1_options_1_1_projector_1_1_illumination_1_1_type_1_1_illumination_from_file.html", "class_holo_gen_1_1_options_1_1_projector_1_1_illumination_1_1_type_1_1_illumination_from_file" ]
    ] ],
    [ "IlluminationGaussian.cs", "_illumination_gaussian_8cs.html", [
      [ "IlluminationGaussian", "class_holo_gen_1_1_options_1_1_projector_1_1_illumination_1_1_type_1_1_illumination_gaussian.html", "class_holo_gen_1_1_options_1_1_projector_1_1_illumination_1_1_type_1_1_illumination_gaussian" ]
    ] ],
    [ "IlluminationOption.cs", "_illumination_option_8cs.html", [
      [ "IlluminationOption", "class_holo_gen_1_1_options_1_1_projector_1_1_illumination_1_1_type_1_1_illumination_option.html", "class_holo_gen_1_1_options_1_1_projector_1_1_illumination_1_1_type_1_1_illumination_option" ]
    ] ],
    [ "IlluminationPlanar.cs", "_illumination_planar_8cs.html", [
      [ "IlluminationPlanar", "class_holo_gen_1_1_options_1_1_projector_1_1_illumination_1_1_type_1_1_illumination_planar.html", "class_holo_gen_1_1_options_1_1_projector_1_1_illumination_1_1_type_1_1_illumination_planar" ]
    ] ],
    [ "IlluminationPossibilities.cs", "_illumination_possibilities_8cs.html", [
      [ "IlluminationPossibilities", "class_holo_gen_1_1_options_1_1_projector_1_1_illumination_1_1_type_1_1_illumination_possibilities.html", "class_holo_gen_1_1_options_1_1_projector_1_1_illumination_1_1_type_1_1_illumination_possibilities" ]
    ] ],
    [ "IlluminationPossibility.cs", "_illumination_possibility_8cs.html", [
      [ "IlluminationPossibility", "class_holo_gen_1_1_options_1_1_projector_1_1_illumination_1_1_type_1_1_illumination_possibility.html", "class_holo_gen_1_1_options_1_1_projector_1_1_illumination_1_1_type_1_1_illumination_possibility" ]
    ] ]
];