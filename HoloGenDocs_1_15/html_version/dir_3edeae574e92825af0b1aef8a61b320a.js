var dir_3edeae574e92825af0b1aef8a61b320a =
[
    [ "TargetBestEfficiency.cs", "_target_best_efficiency_8cs.html", [
      [ "TargetBestEfficiency", "class_holo_gen_1_1_options_1_1_algorithm_1_1_target_1_1_type_1_1_target_best_efficiency.html", "class_holo_gen_1_1_options_1_1_algorithm_1_1_target_1_1_type_1_1_target_best_efficiency" ]
    ] ],
    [ "TargetBestError.cs", "_target_best_error_8cs.html", [
      [ "TargetBestError", "class_holo_gen_1_1_options_1_1_algorithm_1_1_target_1_1_type_1_1_target_best_error.html", "class_holo_gen_1_1_options_1_1_algorithm_1_1_target_1_1_type_1_1_target_best_error" ]
    ] ],
    [ "TargetOption.cs", "_target_option_8cs.html", [
      [ "TargetOption", "class_holo_gen_1_1_options_1_1_algorithm_1_1_target_1_1_type_1_1_target_option.html", "class_holo_gen_1_1_options_1_1_algorithm_1_1_target_1_1_type_1_1_target_option" ]
    ] ],
    [ "TargetPossibilities.cs", "_target_possibilities_8cs.html", [
      [ "TargetPossibilities", "class_holo_gen_1_1_options_1_1_algorithm_1_1_target_1_1_type_1_1_target_possibilities.html", "class_holo_gen_1_1_options_1_1_algorithm_1_1_target_1_1_type_1_1_target_possibilities" ]
    ] ],
    [ "TargetPossibility.cs", "_target_possibility_8cs.html", [
      [ "TargetPossibility", "class_holo_gen_1_1_options_1_1_algorithm_1_1_target_1_1_type_1_1_target_possibility.html", null ]
    ] ]
];