var dir_0b6d2792f80d578a9622edfb6b1c2ecf =
[
    [ "GSVariantOption.cs", "_g_s_variant_option_8cs.html", [
      [ "GSVariantOption", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_variants_1_1_g_s_1_1_g_s_variant_option.html", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_variants_1_1_g_s_1_1_g_s_variant_option" ]
    ] ],
    [ "GSVariantPossibilities.cs", "_g_s_variant_possibilities_8cs.html", [
      [ "GSVariantPossibilities", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_variants_1_1_g_s_1_1_g_s_variant_possibilities.html", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_variants_1_1_g_s_1_1_g_s_variant_possibilities" ]
    ] ],
    [ "GSVariantPossibility.cs", "_g_s_variant_possibility_8cs.html", [
      [ "GSVariantPossibility", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_variants_1_1_g_s_1_1_g_s_variant_possibility.html", null ]
    ] ],
    [ "StandardGSVariant.cs", "_standard_g_s_variant_8cs.html", [
      [ "StandardGSVariant", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_variants_1_1_g_s_1_1_standard_g_s_variant.html", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_variants_1_1_g_s_1_1_standard_g_s_variant" ]
    ] ]
];