var namespace_holo_gen_1_1_u_i_1_1_utils =
[
    [ "Commands", "namespace_holo_gen_1_1_u_i_1_1_utils_1_1_commands.html", "namespace_holo_gen_1_1_u_i_1_1_utils_1_1_commands" ],
    [ "MVVM", "namespace_holo_gen_1_1_u_i_1_1_utils_1_1_m_v_v_m.html", "namespace_holo_gen_1_1_u_i_1_1_utils_1_1_m_v_v_m" ],
    [ "FileLocations", "class_holo_gen_1_1_u_i_1_1_utils_1_1_file_locations.html", null ],
    [ "ITabHandler", "interface_holo_gen_1_1_u_i_1_1_utils_1_1_i_tab_handler.html", "interface_holo_gen_1_1_u_i_1_1_utils_1_1_i_tab_handler" ],
    [ "ITabViewModel", "interface_holo_gen_1_1_u_i_1_1_utils_1_1_i_tab_view_model.html", "interface_holo_gen_1_1_u_i_1_1_utils_1_1_i_tab_view_model" ],
    [ "SimpleCommand", "class_holo_gen_1_1_u_i_1_1_utils_1_1_simple_command.html", "class_holo_gen_1_1_u_i_1_1_utils_1_1_simple_command" ],
    [ "TabHandlerFramework", "class_holo_gen_1_1_u_i_1_1_utils_1_1_tab_handler_framework.html", null ],
    [ "WaitCursor", "class_holo_gen_1_1_u_i_1_1_utils_1_1_wait_cursor.html", "class_holo_gen_1_1_u_i_1_1_utils_1_1_wait_cursor" ]
];