var namespace_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type =
[
    [ "Variants", "namespace_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_variants.html", "namespace_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_variants" ],
    [ "AlgorithmOption", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_algorithm_option.html", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_algorithm_option" ],
    [ "AlgorithmPossibilities", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_algorithm_possibilities.html", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_algorithm_possibilities" ],
    [ "AlgorithmPossibility", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_algorithm_possibility.html", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_algorithm_possibility" ],
    [ "CommonAlgorithm", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_common_algorithm.html", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_common_algorithm" ],
    [ "DSAlgorithm", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_d_s_algorithm.html", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_d_s_algorithm" ],
    [ "GSAlgorithm", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_g_s_algorithm.html", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_g_s_algorithm" ],
    [ "OSPRAlgorithm", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_o_s_p_r_algorithm.html", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_o_s_p_r_algorithm" ],
    [ "SAAlgorithm", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_s_a_algorithm.html", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_s_a_algorithm" ]
];