var namespace_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms =
[
    [ "Type", "namespace_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type.html", "namespace_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type" ],
    [ "Variants", "namespace_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_variants.html", "namespace_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_variants" ],
    [ "AlgorithmsFolder", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_algorithms_folder.html", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_algorithms_folder" ],
    [ "StartingTemperature", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_starting_temperature.html", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_starting_temperature" ],
    [ "SubFrames", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_sub_frames.html", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_sub_frames" ],
    [ "TemperatureCoefficient", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_temperature_coefficient.html", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_temperature_coefficient" ]
];