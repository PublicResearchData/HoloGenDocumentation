var namespace_holo_gen_1_1_u_i_1_1_commands =
[
    [ "CloseCurrentTabCommand", "class_holo_gen_1_1_u_i_1_1_commands_1_1_close_current_tab_command.html", "class_holo_gen_1_1_u_i_1_1_commands_1_1_close_current_tab_command" ],
    [ "ConnectedCommand", "class_holo_gen_1_1_u_i_1_1_commands_1_1_connected_command.html", "class_holo_gen_1_1_u_i_1_1_commands_1_1_connected_command" ],
    [ "ExportImageFileCommand", "class_holo_gen_1_1_u_i_1_1_commands_1_1_export_image_file_command.html", "class_holo_gen_1_1_u_i_1_1_commands_1_1_export_image_file_command" ],
    [ "ExportMatFileCommand", "class_holo_gen_1_1_u_i_1_1_commands_1_1_export_mat_file_command.html", "class_holo_gen_1_1_u_i_1_1_commands_1_1_export_mat_file_command" ],
    [ "ImportCommand", "class_holo_gen_1_1_u_i_1_1_commands_1_1_import_command.html", "class_holo_gen_1_1_u_i_1_1_commands_1_1_import_command" ],
    [ "ImportImageFileCommand", "class_holo_gen_1_1_u_i_1_1_commands_1_1_import_image_file_command.html", "class_holo_gen_1_1_u_i_1_1_commands_1_1_import_image_file_command" ],
    [ "ImportMatFileCommand", "class_holo_gen_1_1_u_i_1_1_commands_1_1_import_mat_file_command.html", "class_holo_gen_1_1_u_i_1_1_commands_1_1_import_mat_file_command" ],
    [ "NewBatchFile", "class_holo_gen_1_1_u_i_1_1_commands_1_1_new_batch_file.html", "class_holo_gen_1_1_u_i_1_1_commands_1_1_new_batch_file" ],
    [ "NewSetupFile", "class_holo_gen_1_1_u_i_1_1_commands_1_1_new_setup_file.html", "class_holo_gen_1_1_u_i_1_1_commands_1_1_new_setup_file" ],
    [ "OpenFileCommand", "class_holo_gen_1_1_u_i_1_1_commands_1_1_open_file_command.html", "class_holo_gen_1_1_u_i_1_1_commands_1_1_open_file_command" ],
    [ "SaveAsFileCommand", "class_holo_gen_1_1_u_i_1_1_commands_1_1_save_as_file_command.html", "class_holo_gen_1_1_u_i_1_1_commands_1_1_save_as_file_command" ],
    [ "SaveFileCommand", "class_holo_gen_1_1_u_i_1_1_commands_1_1_save_file_command.html", "class_holo_gen_1_1_u_i_1_1_commands_1_1_save_file_command" ],
    [ "ShowAboutCommand", "class_holo_gen_1_1_u_i_1_1_commands_1_1_show_about_command.html", "class_holo_gen_1_1_u_i_1_1_commands_1_1_show_about_command" ],
    [ "ShowHelpCommand", "class_holo_gen_1_1_u_i_1_1_commands_1_1_show_help_command.html", "class_holo_gen_1_1_u_i_1_1_commands_1_1_show_help_command" ]
];