var dir_2073a72544f72bcc9b115a40ca90d764 =
[
    [ "AlgorithmOption.cs", "_algorithm_option_8cs.html", [
      [ "AlgorithmOption", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_algorithm_option.html", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_algorithm_option" ]
    ] ],
    [ "AlgorithmPossibilities.cs", "_algorithm_possibilities_8cs.html", [
      [ "AlgorithmPossibilities", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_algorithm_possibilities.html", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_algorithm_possibilities" ]
    ] ],
    [ "AlgorithmPossibility.cs", "_algorithm_possibility_8cs.html", [
      [ "AlgorithmPossibility", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_algorithm_possibility.html", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_algorithm_possibility" ]
    ] ],
    [ "CommonAlgorithm.cs", "_common_algorithm_8cs.html", [
      [ "CommonAlgorithm", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_common_algorithm.html", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_common_algorithm" ]
    ] ],
    [ "DSAlgorithm.cs", "_d_s_algorithm_8cs.html", [
      [ "DSAlgorithm", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_d_s_algorithm.html", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_d_s_algorithm" ]
    ] ],
    [ "GSAlgorithm.cs", "_g_s_algorithm_8cs.html", [
      [ "GSAlgorithm", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_g_s_algorithm.html", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_g_s_algorithm" ]
    ] ],
    [ "OSPRAlgorithm.cs", "_o_s_p_r_algorithm_8cs.html", [
      [ "OSPRAlgorithm", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_o_s_p_r_algorithm.html", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_o_s_p_r_algorithm" ]
    ] ],
    [ "SAAlgorithm.cs", "_s_a_algorithm_8cs.html", [
      [ "SAAlgorithm", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_s_a_algorithm.html", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_s_a_algorithm" ]
    ] ]
];