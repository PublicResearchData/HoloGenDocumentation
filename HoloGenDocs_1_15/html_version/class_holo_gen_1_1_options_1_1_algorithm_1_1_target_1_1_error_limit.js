var class_holo_gen_1_1_options_1_1_algorithm_1_1_target_1_1_error_limit =
[
    [ "Default", "class_holo_gen_1_1_options_1_1_algorithm_1_1_target_1_1_error_limit.html#a525e615eb45eb867807a8763151d447d", null ],
    [ "MaxValue", "class_holo_gen_1_1_options_1_1_algorithm_1_1_target_1_1_error_limit.html#a3e34fdf1703266c2cc0069fa7961e4f3", null ],
    [ "MinValue", "class_holo_gen_1_1_options_1_1_algorithm_1_1_target_1_1_error_limit.html#adac8c291389389da7e84efce2202702a", null ],
    [ "Name", "class_holo_gen_1_1_options_1_1_algorithm_1_1_target_1_1_error_limit.html#afdf369398689c73581b74c99203695f3", null ],
    [ "ToolTip", "class_holo_gen_1_1_options_1_1_algorithm_1_1_target_1_1_error_limit.html#a190ea9d4a33b5c212b3d813564737797", null ],
    [ "Watermark", "class_holo_gen_1_1_options_1_1_algorithm_1_1_target_1_1_error_limit.html#ad5d0597c3bd74d36f36773aace57af07", null ]
];