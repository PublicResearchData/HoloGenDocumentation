var dir_4b00cec90a89ea10d9a26c7e7cd7ecd5 =
[
    [ "CommonVariantOption.cs", "_common_variant_option_8cs.html", [
      [ "CommonVariantOption", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_variants_1_1_common_1_1_common_variant_option.html", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_variants_1_1_common_1_1_common_variant_option" ]
    ] ],
    [ "CommonVariantPossibilities.cs", "_common_variant_possibilities_8cs.html", [
      [ "CommonVariantPossibilities", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_variants_1_1_common_1_1_common_variant_possibilities.html", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_variants_1_1_common_1_1_common_variant_possibilities" ]
    ] ],
    [ "CommonVariantPossibility.cs", "_common_variant_possibility_8cs.html", [
      [ "CommonVariantPossibility", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_variants_1_1_common_1_1_common_variant_possibility.html", null ]
    ] ],
    [ "FFTFwdCommonVariant.cs", "_f_f_t_fwd_common_variant_8cs.html", [
      [ "FFTFwdCommonVariant", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_variants_1_1_common_1_1_f_f_t_fwd_common_variant.html", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_variants_1_1_common_1_1_f_f_t_fwd_common_variant" ]
    ] ],
    [ "FFTInvCommonVariant.cs", "_f_f_t_inv_common_variant_8cs.html", [
      [ "FFTInvCommonVariant", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_variants_1_1_common_1_1_f_f_t_inv_common_variant.html", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_variants_1_1_common_1_1_f_f_t_inv_common_variant" ]
    ] ]
];