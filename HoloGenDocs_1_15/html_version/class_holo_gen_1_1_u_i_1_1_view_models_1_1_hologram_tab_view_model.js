var class_holo_gen_1_1_u_i_1_1_view_models_1_1_hologram_tab_view_model =
[
    [ "HologramTabViewModel", "class_holo_gen_1_1_u_i_1_1_view_models_1_1_hologram_tab_view_model.html#a2c904e7f96f091d97497b4e32b96e6f0", null ],
    [ "ExportableImage", "class_holo_gen_1_1_u_i_1_1_view_models_1_1_hologram_tab_view_model.html#a64452134d0831091e4c8a93731277753", null ],
    [ "ExportableImageWithoutScale", "class_holo_gen_1_1_u_i_1_1_view_models_1_1_hologram_tab_view_model.html#aeeb24f5e9ec82ac3742b7f303b88db4d", null ],
    [ "IsFileBased", "class_holo_gen_1_1_u_i_1_1_view_models_1_1_hologram_tab_view_model.html#abff5d7d0d50748407ae85adac489e513", null ],
    [ "IsImageExportable", "class_holo_gen_1_1_u_i_1_1_view_models_1_1_hologram_tab_view_model.html#ac7a33e4ed2ab6ad119c370e33442a0fe", null ],
    [ "SaveableData", "class_holo_gen_1_1_u_i_1_1_view_models_1_1_hologram_tab_view_model.html#a69191345b612862aff4fc9aff2a9f0f9", null ],
    [ "ImageViewModel", "class_holo_gen_1_1_u_i_1_1_view_models_1_1_hologram_tab_view_model.html#a5252a087288a580e118afbb41ca70b52", null ]
];