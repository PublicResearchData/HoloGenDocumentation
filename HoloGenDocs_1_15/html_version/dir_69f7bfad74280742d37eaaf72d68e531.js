var dir_69f7bfad74280742d37eaaf72d68e531 =
[
    [ "RegionFromFile.cs", "_region_from_file_8cs.html", [
      [ "RegionFromFile", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_type_1_1_region_from_file.html", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_type_1_1_region_from_file" ]
    ] ],
    [ "RegionIntensity.cs", "_region_intensity_8cs.html", [
      [ "RegionIntensity", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_type_1_1_region_intensity.html", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_type_1_1_region_intensity" ]
    ] ],
    [ "RegionNone.cs", "_region_none_8cs.html", [
      [ "RegionNone", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_type_1_1_region_none.html", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_type_1_1_region_none" ]
    ] ],
    [ "RegionOption.cs", "_region_option_8cs.html", [
      [ "RegionOption", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_type_1_1_region_option.html", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_type_1_1_region_option" ]
    ] ],
    [ "RegionPossibilities.cs", "_region_possibilities_8cs.html", [
      [ "RegionPossibilities", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_type_1_1_region_possibilities.html", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_type_1_1_region_possibilities" ]
    ] ],
    [ "RegionPossibility.cs", "_region_possibility_8cs.html", [
      [ "RegionPossibility", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_type_1_1_region_possibility.html", null ]
    ] ],
    [ "SymmetryHorizontal.cs", "_symmetry_horizontal_8cs.html", [
      [ "SymmetryHorizontal", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_type_1_1_symmetry_horizontal.html", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_type_1_1_symmetry_horizontal" ]
    ] ],
    [ "SymmetryNone.cs", "_symmetry_none_8cs.html", [
      [ "SymmetryNone", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_type_1_1_symmetry_none.html", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_type_1_1_symmetry_none" ]
    ] ],
    [ "SymmetryOption.cs", "_symmetry_option_8cs.html", [
      [ "SymmetryOption", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_type_1_1_symmetry_option.html", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_type_1_1_symmetry_option" ]
    ] ],
    [ "SymmetryPossibilities.cs", "_symmetry_possibilities_8cs.html", [
      [ "SymmetryPossibilities", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_type_1_1_symmetry_possibilities.html", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_type_1_1_symmetry_possibilities" ]
    ] ],
    [ "SymmetryPossibility.cs", "_symmetry_possibility_8cs.html", [
      [ "SymmetryPossibility", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_type_1_1_symmetry_possibility.html", null ]
    ] ],
    [ "SymmetryVertical.cs", "_symmetry_vertical_8cs.html", [
      [ "SymmetryVertical", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_type_1_1_symmetry_vertical.html", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_type_1_1_symmetry_vertical" ]
    ] ],
    [ "VariantOption.cs", "_target_2_region_2_type_2_variant_option_8cs.html", [
      [ "VariantOption", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_type_1_1_variant_option.html", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_type_1_1_variant_option" ]
    ] ],
    [ "VariantPixel.cs", "_variant_pixel_8cs.html", [
      [ "VariantPixel", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_type_1_1_variant_pixel.html", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_type_1_1_variant_pixel" ]
    ] ],
    [ "VariantPossibilities.cs", "_variant_possibilities_8cs.html", [
      [ "VariantPossibilities", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_type_1_1_variant_possibilities.html", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_type_1_1_variant_possibilities" ]
    ] ],
    [ "VariantPossibility.cs", "_target_2_region_2_type_2_variant_possibility_8cs.html", [
      [ "VariantPossibility", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_type_1_1_variant_possibility.html", null ]
    ] ],
    [ "VariantRound.cs", "_variant_round_8cs.html", [
      [ "VariantRound", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_type_1_1_variant_round.html", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_type_1_1_variant_round" ]
    ] ],
    [ "VariantSquare.cs", "_variant_square_8cs.html", [
      [ "VariantSquare", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_type_1_1_variant_square.html", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_type_1_1_variant_square" ]
    ] ]
];