var dir_9a5ab845a08032e47b4db36f061a3d75 =
[
    [ "CloseCurrentTabCommand.cs", "_close_current_tab_command_8cs.html", [
      [ "CloseCurrentTabCommand", "class_holo_gen_1_1_u_i_1_1_commands_1_1_close_current_tab_command.html", "class_holo_gen_1_1_u_i_1_1_commands_1_1_close_current_tab_command" ]
    ] ],
    [ "ConnectedCommand.cs", "_connected_command_8cs.html", [
      [ "ConnectedCommand", "class_holo_gen_1_1_u_i_1_1_commands_1_1_connected_command.html", "class_holo_gen_1_1_u_i_1_1_commands_1_1_connected_command" ]
    ] ],
    [ "ExportImageFileCommand.cs", "_export_image_file_command_8cs.html", [
      [ "ExportImageFileCommand", "class_holo_gen_1_1_u_i_1_1_commands_1_1_export_image_file_command.html", "class_holo_gen_1_1_u_i_1_1_commands_1_1_export_image_file_command" ]
    ] ],
    [ "ExportMatFileCommand.cs", "_export_mat_file_command_8cs.html", [
      [ "ExportMatFileCommand", "class_holo_gen_1_1_u_i_1_1_commands_1_1_export_mat_file_command.html", "class_holo_gen_1_1_u_i_1_1_commands_1_1_export_mat_file_command" ]
    ] ],
    [ "ImportCommand.cs", "_import_command_8cs.html", [
      [ "ImportCommand", "class_holo_gen_1_1_u_i_1_1_commands_1_1_import_command.html", "class_holo_gen_1_1_u_i_1_1_commands_1_1_import_command" ]
    ] ],
    [ "ImportImageFileCommand.cs", "_import_image_file_command_8cs.html", [
      [ "ImportImageFileCommand", "class_holo_gen_1_1_u_i_1_1_commands_1_1_import_image_file_command.html", "class_holo_gen_1_1_u_i_1_1_commands_1_1_import_image_file_command" ]
    ] ],
    [ "ImportMatFileCommand.cs", "_import_mat_file_command_8cs.html", [
      [ "ImportMatFileCommand", "class_holo_gen_1_1_u_i_1_1_commands_1_1_import_mat_file_command.html", "class_holo_gen_1_1_u_i_1_1_commands_1_1_import_mat_file_command" ]
    ] ],
    [ "NewBatchFile.cs", "_new_batch_file_8cs.html", [
      [ "NewBatchFile", "class_holo_gen_1_1_u_i_1_1_commands_1_1_new_batch_file.html", "class_holo_gen_1_1_u_i_1_1_commands_1_1_new_batch_file" ]
    ] ],
    [ "NewSetupFile.cs", "_new_setup_file_8cs.html", [
      [ "NewSetupFile", "class_holo_gen_1_1_u_i_1_1_commands_1_1_new_setup_file.html", "class_holo_gen_1_1_u_i_1_1_commands_1_1_new_setup_file" ]
    ] ],
    [ "OpenFileCommand.cs", "_open_file_command_8cs.html", [
      [ "OpenFileCommand", "class_holo_gen_1_1_u_i_1_1_commands_1_1_open_file_command.html", "class_holo_gen_1_1_u_i_1_1_commands_1_1_open_file_command" ]
    ] ],
    [ "SaveAsFileCommand.cs", "_save_as_file_command_8cs.html", [
      [ "SaveAsFileCommand", "class_holo_gen_1_1_u_i_1_1_commands_1_1_save_as_file_command.html", "class_holo_gen_1_1_u_i_1_1_commands_1_1_save_as_file_command" ]
    ] ],
    [ "SaveFileCommand.cs", "_save_file_command_8cs.html", [
      [ "SaveFileCommand", "class_holo_gen_1_1_u_i_1_1_commands_1_1_save_file_command.html", "class_holo_gen_1_1_u_i_1_1_commands_1_1_save_file_command" ]
    ] ],
    [ "ShowAboutCommand.cs", "_show_about_command_8cs.html", [
      [ "ShowAboutCommand", "class_holo_gen_1_1_u_i_1_1_commands_1_1_show_about_command.html", "class_holo_gen_1_1_u_i_1_1_commands_1_1_show_about_command" ]
    ] ],
    [ "ShowHelpCommand.cs", "_show_help_command_8cs.html", [
      [ "ShowHelpCommand", "class_holo_gen_1_1_u_i_1_1_commands_1_1_show_help_command.html", "class_holo_gen_1_1_u_i_1_1_commands_1_1_show_help_command" ]
    ] ]
];