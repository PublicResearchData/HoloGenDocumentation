var searchData=
[
  ['newbatchfile',['NewBatchFile',['../class_holo_gen_1_1_u_i_1_1_commands_1_1_new_batch_file.html',1,'HoloGen::UI::Commands']]],
  ['newsetupfile',['NewSetupFile',['../class_holo_gen_1_1_u_i_1_1_commands_1_1_new_setup_file.html',1,'HoloGen::UI::Commands']]],
  ['nulltounsetvalueconverter',['NullToUnsetValueConverter',['../class_holo_gen_1_1_u_i_1_1_hamburger_1_1_null_to_unset_value_converter.html',1,'HoloGen::UI::Hamburger']]],
  ['numericoption',['NumericOption',['../class_holo_gen_1_1_hierarchy_1_1_option_types_1_1_numeric_option.html',1,'HoloGen::Hierarchy::OptionTypes']]],
  ['numericoption_3c_20double_20_3e',['NumericOption&lt; double &gt;',['../class_holo_gen_1_1_hierarchy_1_1_option_types_1_1_numeric_option.html',1,'HoloGen::Hierarchy::OptionTypes']]],
  ['numericoption_3c_20int_20_3e',['NumericOption&lt; int &gt;',['../class_holo_gen_1_1_hierarchy_1_1_option_types_1_1_numeric_option.html',1,'HoloGen::Hierarchy::OptionTypes']]],
  ['numiterations',['NumIterations',['../class_holo_gen_1_1_options_1_1_algorithm_1_1_termination_1_1_num_iterations.html',1,'HoloGen::Options::Algorithm::Termination']]]
];
