var searchData=
[
  ['onexit',['OnExit',['../class_holo_gen_1_1_u_i_1_1_app.html#a7b2c77f7abf2da02b397b1c5f35edd19',1,'HoloGen::UI::App']]],
  ['onpropertychanged',['OnPropertyChanged',['../class_holo_gen_1_1_u_i_1_1_hamburger_1_1_views_1_1_hamburger_tab_view.html#af201258fd79a1f15c9636f2cd509f3a8',1,'HoloGen.UI.Hamburger.Views.HamburgerTabView.OnPropertyChanged()'],['../class_holo_gen_1_1_u_i_1_1_utils_1_1_m_v_v_m_1_1_bindable_base.html#a36e8b11f13f008e5ef060a0a0ba1b450',1,'HoloGen.UI.Utils.MVVM.BindableBase.OnPropertyChanged()'],['../class_holo_gen_1_1_utils_1_1_observable_object.html#a69d19116966ab0ad48c06a7fce7f878d',1,'HoloGen.Utils.ObservableObject.OnPropertyChanged()']]],
  ['onstartup',['OnStartup',['../class_holo_gen_1_1_u_i_1_1_app.html#a7261c86cc33bffc3ba1626691394a312',1,'HoloGen::UI::App']]],
  ['openfilecommand',['OpenFileCommand',['../class_holo_gen_1_1_u_i_1_1_commands_1_1_open_file_command.html#ab1d424fb8bbd13703885b563f32e7d6f',1,'HoloGen::UI::Commands::OpenFileCommand']]],
  ['option',['Option',['../class_holo_gen_1_1_hierarchy_1_1_option_types_1_1_option.html#aa532af457829da27912c9ade10d963c4',1,'HoloGen.Hierarchy.OptionTypes.Option.Option()'],['../class_holo_gen_1_1_hierarchy_1_1_option_types_1_1_option.html#afbade1fc754009392de14ebd7c870241',1,'HoloGen.Hierarchy.OptionTypes.Option.Option(T value)']]],
  ['optionscommandspanel',['OptionsCommandsPanel',['../class_holo_gen_1_1_u_i_1_1_views_1_1_options_commands_panel.html#abea6c26e82b6b0b27b19213d954d45d8',1,'HoloGen::UI::Views::OptionsCommandsPanel']]],
  ['optionsroot',['OptionsRoot',['../class_holo_gen_1_1_options_1_1_options_root.html#a83047023a6a5f189048ac74d016297bf',1,'HoloGen::Options::OptionsRoot']]]
];
