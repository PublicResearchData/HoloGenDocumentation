var searchData=
[
  ['generalsettingspage',['GeneralSettingsPage',['../class_holo_gen_1_1_settings_1_1_general_1_1_general_settings_page.html',1,'HoloGen::Settings::General']]],
  ['genericcache',['GenericCache',['../class_holo_gen_1_1_utils_1_1_generic_cache.html',1,'HoloGen.Utils.GenericCache&lt; K1, A, B &gt;'],['../class_holo_gen_1_1_utils_1_1_generic_cache.html',1,'HoloGen.Utils.GenericCache&lt; K1, K2, A, B, C &gt;'],['../class_holo_gen_1_1_utils_1_1_generic_cache.html',1,'HoloGen.Utils.GenericCache&lt; K1, K2, K3, A, B, C, D &gt;'],['../class_holo_gen_1_1_utils_1_1_generic_cache.html',1,'HoloGen.Utils.GenericCache&lt; K1, K2, K3, K4, A, B, C, D, E &gt;']]],
  ['genericcache_3c_20transformtype_2c_20imageviewtype_2c_20colorscheme_2c_20imagescaletype_2c_20hologen_3a_3aimage_3a_3acompleximage_2c_20transformimage_2c_20factoredimage_2c_20coloredimage_2c_20hologen_3a_3aimage_3a_3aimageview_20_3e',['GenericCache&lt; TransformType, ImageViewType, ColorScheme, ImageScaleType, HoloGen::Image::ComplexImage, TransformImage, FactoredImage, ColoredImage, HoloGen::Image::ImageView &gt;',['../class_holo_gen_1_1_utils_1_1_generic_cache.html',1,'HoloGen::Utils']]],
  ['globals',['Globals',['../class_holo_gen_1_1_alg_1_1_base_1_1_cuda_1_1_globals.html',1,'HoloGen::Alg::Base::Cuda']]],
  ['graphitemviewmodel',['GraphItemViewModel',['../class_holo_gen_1_1_u_i_1_1_graph_1_1_view_models_1_1_graph_item_view_model.html',1,'HoloGen::UI::Graph::ViewModels']]],
  ['graphview',['GraphView',['../class_holo_gen_1_1_u_i_1_1_graph_1_1_views_1_1_graph_view.html',1,'HoloGen::UI::Graph::Views']]],
  ['graphviewmodel',['GraphViewModel',['../class_holo_gen_1_1_u_i_1_1_graph_1_1_view_models_1_1_graph_view_model.html',1,'HoloGen::UI::Graph::ViewModels']]],
  ['gsalgorithm',['GSAlgorithm',['../class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_g_s_algorithm.html',1,'HoloGen::Options::Algorithm::Algorithms::Type']]],
  ['gsvariantoption',['GSVariantOption',['../class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_variants_1_1_g_s_1_1_g_s_variant_option.html',1,'HoloGen::Options::Algorithm::Algorithms::Variants::GS']]],
  ['gsvariantpossibilities',['GSVariantPossibilities',['../class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_variants_1_1_g_s_1_1_g_s_variant_possibilities.html',1,'HoloGen::Options::Algorithm::Algorithms::Type::Variants::GS']]],
  ['gsvariantpossibility',['GSVariantPossibility',['../class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_variants_1_1_g_s_1_1_g_s_variant_possibility.html',1,'HoloGen::Options::Algorithm::Algorithms::Type::Variants::GS']]]
];
