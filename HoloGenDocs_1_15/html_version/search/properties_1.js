var searchData=
[
  ['backprojectseed',['BackProjectSeed',['../class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_seed_possibilities.html#af7f12183f4294df9fc2f2e1da6a407d2',1,'HoloGen::Options::Projector::Hologram::SeedPossibilities']]],
  ['binaryampslm',['BinaryAmpSLM',['../class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_s_l_m_possibilities.html#aef22f5a40fdc4300a664f837a2624928',1,'HoloGen::Options::Projector::Hologram::SLMPossibilities']]],
  ['binaryphaseslm',['BinaryPhaseSLM',['../class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_s_l_m_possibilities.html#a66bf3746bdb0b1dd825b513bf8246fe2',1,'HoloGen::Options::Projector::Hologram::SLMPossibilities']]],
  ['blackonwhite',['BlackonWhite',['../class_holo_gen_1_1_utils_1_1_color_schemes.html#af75f739f4ea92d84b3db22cca2db0e1a',1,'HoloGen::Utils::ColorSchemes']]],
  ['blocks',['Blocks',['../class_holo_gen_1_1_options_1_1_hardware_1_1_hardware_1_1_type_1_1_hardware_g_p_u.html#a66474d4fe71ebbfbd4a8db2e731e332e',1,'HoloGen::Options::Hardware::Hardware::Type::HardwareGPU']]],
  ['blueonblack',['BlueonBlack',['../class_holo_gen_1_1_utils_1_1_color_schemes.html#aefcec8e1ffb6543254ba1d77c4711211',1,'HoloGen::Utils::ColorSchemes']]],
  ['blueonwhite',['BlueonWhite',['../class_holo_gen_1_1_utils_1_1_color_schemes.html#afcfc4b162cc8185aa66b03795585fbd8',1,'HoloGen::Utils::ColorSchemes']]],
  ['booleandatatemplate',['BooleanDataTemplate',['../class_holo_gen_1_1_u_i_1_1_option_editors_1_1_option_data_template_selector.html#a92b1a3957d6b09cee9a20c193d056b49',1,'HoloGen::UI::OptionEditors::OptionDataTemplateSelector']]],
  ['bordercolorbrush',['BorderColorBrush',['../class_holo_gen_1_1_u_i_1_1_menus_1_1_theme_1_1_accent_color_menu_data.html#a47521754d2a665106899e0e4d56303ff',1,'HoloGen::UI::Menus::Theme::AccentColorMenuData']]],
  ['bottompane',['BottomPane',['../class_holo_gen_1_1_u_i_1_1_hamburger_1_1_views_1_1_hamburger_tab_view.html#ad73bf38daee651871d7c9f7a747dd985',1,'HoloGen::UI::Hamburger::Views::HamburgerTabView']]]
];
