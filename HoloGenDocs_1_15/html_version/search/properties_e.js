var searchData=
[
  ['p1',['P1',['../class_holo_gen_1_1_u_i_1_1_mask_1_1_model_1_1_d_ellipse.html#abada0e971cd885c061870dfd2d814365',1,'HoloGen.UI.Mask.Model.DEllipse.P1()'],['../class_holo_gen_1_1_u_i_1_1_mask_1_1_model_1_1_d_rect.html#aab49632b244cb1af6d641f436ef52985',1,'HoloGen.UI.Mask.Model.DRect.P1()']]],
  ['p2',['P2',['../class_holo_gen_1_1_u_i_1_1_mask_1_1_model_1_1_d_ellipse.html#a6a2cd601d246a79c70efef08f9d42211',1,'HoloGen.UI.Mask.Model.DEllipse.P2()'],['../class_holo_gen_1_1_u_i_1_1_mask_1_1_model_1_1_d_rect.html#a8fbb5a85fc1fee402acefce30b974501',1,'HoloGen.UI.Mask.Model.DRect.P2()']]],
  ['pagedatatemplate',['PageDataTemplate',['../class_holo_gen_1_1_u_i_1_1_hamburger_1_1_page_data_template_selector.html#a18d9465c8588aba274a0ed635eb1aa6f',1,'HoloGen::UI::Hamburger::PageDataTemplateSelector']]],
  ['pathdatatemplate',['PathDataTemplate',['../class_holo_gen_1_1_u_i_1_1_option_editors_1_1_option_data_template_selector.html#a378ebcb6c08f293f1a1193b36bc99543',1,'HoloGen::UI::OptionEditors::OptionDataTemplateSelector']]],
  ['pdftabdatatemplate',['PdfTabDataTemplate',['../class_holo_gen_1_1_u_i_1_1_common_1_1_tab_data_template_selector.html#a9d48e4625bf504645312dd2856fcd32a',1,'HoloGen::UI::Common::TabDataTemplateSelector']]],
  ['pdfviewmodel',['PdfViewModel',['../class_holo_gen_1_1_u_i_1_1_view_models_1_1_pdf_tab_view_model.html#abb104088646a242c0ae21cfb8257ed61',1,'HoloGen::UI::ViewModels::PdfTabViewModel']]],
  ['pixelcount',['PixelCount',['../class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_type_1_1_region_intensity.html#a1ae54015e9e4967d9f3d1a177d869cd8',1,'HoloGen::Options::Target::Region::Type::RegionIntensity']]],
  ['pixelpitch',['PixelPitch',['../class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_hologram_folder.html#a5c7e01868485f32596eb8acea50cbcb1',1,'HoloGen::Options::Projector::Hologram::HologramFolder']]],
  ['plottervisibility',['PlotterVisibility',['../class_holo_gen_1_1_u_i_1_1_view_models_1_1_hologram_view_model.html#a8cb1edbc0f872666219c8582272ef9fc',1,'HoloGen::UI::ViewModels::HologramViewModel']]],
  ['pointbody',['PointBody',['../class_holo_gen_1_1_u_i_1_1_mask_1_1_model_1_1_d_point.html#ab7edf66c0e9ef99485afbb8531e18b7f',1,'HoloGen::UI::Mask::Model::DPoint']]],
  ['pointdrawingbody',['PointDrawingBody',['../class_holo_gen_1_1_u_i_1_1_mask_1_1_model_1_1_d_point.html#a9d71f67be5074c2a11febbefbfc2cbc0',1,'HoloGen::UI::Mask::Model::DPoint']]],
  ['polygonbody',['PolygonBody',['../class_holo_gen_1_1_u_i_1_1_mask_1_1_model_1_1_d_polygon.html#a1de4553618093788690c082d609f06a0',1,'HoloGen::UI::Mask::Model::DPolygon']]],
  ['possibilities',['Possibilities',['../class_holo_gen_1_1_hierarchy_1_1_option_types_1_1_select_option.html#addebf61eae22b7c82a6f6f76aeeb6468',1,'HoloGen::Hierarchy::OptionTypes::SelectOption']]],
  ['processtabdatatemplate',['ProcessTabDataTemplate',['../class_holo_gen_1_1_u_i_1_1_common_1_1_tab_data_template_selector.html#ac1efabb4db4f86bc87e492493f844e53',1,'HoloGen::UI::Common::TabDataTemplateSelector']]],
  ['processviewefficiency',['ProcessViewEfficiency',['../class_holo_gen_1_1_process_1_1_options_1_1_process_view_possibilities.html#aed8fa9fb0837da7f06463b6cd4387571',1,'HoloGen::Process::Options::ProcessViewPossibilities']]],
  ['processviewerror',['ProcessViewError',['../class_holo_gen_1_1_process_1_1_options_1_1_process_view_possibilities.html#aa31d21f85419bd9f2ba23ee003413b74',1,'HoloGen::Process::Options::ProcessViewPossibilities']]],
  ['processviewfolder',['ProcessViewFolder',['../class_holo_gen_1_1_process_1_1_options_1_1_process_options.html#a4fbbf13fae922c5ef46dca5f0554866b',1,'HoloGen::Process::Options::ProcessOptions']]],
  ['processviewoption',['ProcessViewOption',['../class_holo_gen_1_1_process_1_1_options_1_1_process_view_folder.html#ade557011924cefe1c9d46a8d3cb872a0',1,'HoloGen::Process::Options::ProcessViewFolder']]],
  ['processviewtype',['ProcessViewType',['../class_holo_gen_1_1_process_1_1_options_1_1_process_view_possibility.html#a94584d48f754d70405e20701a82bdfd4',1,'HoloGen.Process.Options.ProcessViewPossibility.ProcessViewType()'],['../class_holo_gen_1_1_u_i_1_1_graph_1_1_view_models_1_1_graph_view_model.html#a11d637b14f149cb3c0e02037d23954fa',1,'HoloGen.UI.Graph.ViewModels.GraphViewModel.ProcessViewType()']]],
  ['projectorpage',['ProjectorPage',['../class_holo_gen_1_1_options_1_1_options_root.html#a9dfbd97475715152ee03ded2468d1048',1,'HoloGen::Options::OptionsRoot']]],
  ['purpleonblack',['PurpleonBlack',['../class_holo_gen_1_1_utils_1_1_color_schemes.html#af1cb3b5d2204044959e604e9d8370abd',1,'HoloGen::Utils::ColorSchemes']]],
  ['purpleonwhite',['PurpleonWhite',['../class_holo_gen_1_1_utils_1_1_color_schemes.html#a134e630e82a54cfbb4ed75c5a9282822',1,'HoloGen::Utils::ColorSchemes']]]
];
