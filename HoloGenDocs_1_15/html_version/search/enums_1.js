var searchData=
[
  ['fftdirection',['FFTDirection',['../namespace_holo_gen_1_1_alg_1_1_base_1_1_man.html#aa30a3f5dd0066cb213ed7c40ba2cdff0',1,'HoloGen::Alg::Base::Man']]],
  ['fftdirectioncuda',['FFTDirectionCuda',['../namespace_holo_gen_1_1_alg_1_1_base_1_1_cuda.html#a2341e297641a2ef6f9c251a3e1beec29',1,'HoloGen::Alg::Base::Cuda']]],
  ['fftscaletype',['FFTScaleType',['../namespace_holo_gen_1_1_alg_1_1_base_1_1_cuda.html#acd8b906c63c6bda4eda08c7913aa8aee',1,'HoloGen::Alg::Base::Cuda']]]
];
