var searchData=
[
  ['offtext',['OffText',['../class_holo_gen_1_1_hierarchy_1_1_option_types_1_1_boolean_option.html#aeba5a7bcb95c31c4637453da678d0940',1,'HoloGen::Hierarchy::OptionTypes::BooleanOption']]],
  ['ontext',['OnText',['../class_holo_gen_1_1_hierarchy_1_1_option_types_1_1_boolean_option.html#ab88131adc4a69ee94f19bff1418dfe4f',1,'HoloGen::Hierarchy::OptionTypes::BooleanOption']]],
  ['openfilecommand',['OpenFileCommand',['../class_holo_gen_1_1_u_i_1_1_main_window_view_model.html#a957af9787e026b589df1806b647c590a',1,'HoloGen.UI.MainWindowViewModel.OpenFileCommand()'],['../class_holo_gen_1_1_u_i_1_1_menus_1_1_view_models_1_1_file_menu_view_model.html#ac889443c5b2c7271e6e0056433261caa',1,'HoloGen.UI.Menus.ViewModels.FileMenuViewModel.OpenFileCommand()']]],
  ['operationmode',['OperationMode',['../class_holo_gen_1_1_u_i_1_1_mask_1_1_mask_view_model.html#acfe735cc64df3dabc62c85d43278c69b',1,'HoloGen::UI::Mask::MaskViewModel']]],
  ['option',['Option',['../class_holo_gen_1_1_u_i_1_1_option_editors_1_1_select_editor.html#ae610a5c8b4997f5df9ee1c766a07e5af',1,'HoloGen::UI::OptionEditors::SelectEditor']]],
  ['options',['Options',['../class_holo_gen_1_1_u_i_1_1_view_models_1_1_setup_view_model.html#aa6b604df10dfdb755c16e651ac0376dc',1,'HoloGen.UI.ViewModels.SetupViewModel.Options()'],['../class_holo_gen_1_1_u_i_1_1_view_models_1_1_hologram_view_model.html#a246ed6fa72b11bf3e6868a6d08ccdeea',1,'HoloGen.UI.ViewModels.HologramViewModel.Options()'],['../class_holo_gen_1_1_u_i_1_1_view_models_1_1_batch_view_model.html#a5098c4614dca1ca912a36b69342525d0',1,'HoloGen.UI.ViewModels.BatchViewModel.Options()'],['../class_holo_gen_1_1_u_i_1_1_process_1_1_monitor_1_1_view_models_1_1_process_view_model.html#a7ae03060032a14566248affa85eb2ac8',1,'HoloGen.UI.Process.Monitor.ViewModels.ProcessViewModel.Options()']]],
  ['optionsdatatemplate',['OptionsDataTemplate',['../class_holo_gen_1_1_u_i_1_1_hamburger_1_1_page_data_template_selector.html#ac999f160e7d50548a93dac60c07ade56',1,'HoloGen::UI::Hamburger::PageDataTemplateSelector']]],
  ['optionsmetadata',['OptionsMetadata',['../class_holo_gen_1_1_image_1_1_complex_image.html#adc2d749aa8d7d9751b4c773541d179e8',1,'HoloGen::Image::ComplexImage']]],
  ['optionsmetadataversion',['OptionsMetadataVersion',['../class_holo_gen_1_1_image_1_1_complex_image.html#acc8a5c94b873d01076c83acf7937e101',1,'HoloGen::Image::ComplexImage']]],
  ['orangeonblack',['OrangeonBlack',['../class_holo_gen_1_1_utils_1_1_color_schemes.html#a0445eb968cb8dfe2e133cc756a0eda33',1,'HoloGen::Utils::ColorSchemes']]],
  ['orangeonwhite',['OrangeonWhite',['../class_holo_gen_1_1_utils_1_1_color_schemes.html#a83bb611639998bac884f2874475b454e',1,'HoloGen::Utils::ColorSchemes']]],
  ['ospralgorithm',['OSPRAlgorithm',['../class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_algorithm_possibilities.html#a99105e00100b9c93cb1c000a602be697',1,'HoloGen::Options::Algorithm::Algorithms::Type::AlgorithmPossibilities']]],
  ['outputpage',['OutputPage',['../class_holo_gen_1_1_options_1_1_options_root.html#a795832c52b6be9070ed1a14f70bbacc0',1,'HoloGen::Options::OptionsRoot']]],
  ['oversampling',['Oversampling',['../class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_hologram_folder.html#a1e6154405d1d0a190ec1518042ef400b',1,'HoloGen::Options::Projector::Hologram::HologramFolder']]]
];
