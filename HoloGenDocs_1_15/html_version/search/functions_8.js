var searchData=
[
  ['illuminationfolder',['IlluminationFolder',['../class_holo_gen_1_1_options_1_1_projector_1_1_illumination_1_1_illumination_folder.html#a73f32262d896119c86b60a921a418421',1,'HoloGen::Options::Projector::Illumination::IlluminationFolder']]],
  ['imagecache',['ImageCache',['../class_holo_gen_1_1_image_1_1_image_cache.html#aea476bfef9e586be36041b466372dcb3',1,'HoloGen::Image::ImageCache']]],
  ['imageoptions',['ImageOptions',['../class_holo_gen_1_1_image_1_1_options_1_1_image_options.html#a4310d49286af9e5e04d332a8a538ac45',1,'HoloGen.Image.Options.ImageOptions.ImageOptions()'],['../class_holo_gen_1_1_image_1_1_options_1_1_type_1_1_image_options.html#add94d36cb7e4410c645024391c1c8657',1,'HoloGen.Image.Options.Type.ImageOptions.ImageOptions()']]],
  ['imageview',['ImageView',['../class_holo_gen_1_1_image_1_1_image_view.html#a4c41d74af8b68d7fa081733a064ef75b',1,'HoloGen::Image::ImageView']]],
  ['imageviewer',['ImageViewer',['../class_holo_gen_1_1_u_i_1_1_image_1_1_viewer_1_1_image_viewer.html#a8635398053032e35509c4de2d6827c35',1,'HoloGen::UI::Image::Viewer::ImageViewer']]],
  ['imageviewfolder',['ImageViewFolder',['../class_holo_gen_1_1_image_1_1_options_1_1_image_view_folder.html#a0267f0a59f607183dfdeb0ffcaafa375',1,'HoloGen::Image::Options::ImageViewFolder']]],
  ['imageviewmodel',['ImageViewModel',['../class_holo_gen_1_1_u_i_1_1_image_1_1_viewer_1_1_image_view_model.html#a1c4506d57270c6b54a23aff8502dcb04',1,'HoloGen::UI::Image::Viewer::ImageViewModel']]],
  ['import',['Import',['../class_holo_gen_1_1_i_o_1_1_convertors_1_1_import_1_1_mat_importer.html#ab8f8671038f0412642a60af0cf5e6036',1,'HoloGen::IO::Convertors::Import::MatImporter']]],
  ['importcommand',['ImportCommand',['../class_holo_gen_1_1_u_i_1_1_commands_1_1_import_command.html#a4cb715af685d4f89f8de900fd771d09c',1,'HoloGen::UI::Commands::ImportCommand']]],
  ['importimagefilecommand',['ImportImageFileCommand',['../class_holo_gen_1_1_u_i_1_1_commands_1_1_import_image_file_command.html#acd1e4b81b6f343908df78b9b06ef93a0',1,'HoloGen::UI::Commands::ImportImageFileCommand']]],
  ['importmatfilecommand',['ImportMatFileCommand',['../class_holo_gen_1_1_u_i_1_1_commands_1_1_import_mat_file_command.html#ae54ef08b0c9399231d1135972ab71747',1,'HoloGen::UI::Commands::ImportMatFileCommand']]],
  ['incrementaxislimits',['IncrementAxisLimits',['../class_holo_gen_1_1_u_i_1_1_graph_1_1_view_models_1_1_graph_view_model.html#a004f343e6829c21fa6995ee5f619d4b4',1,'HoloGen::UI::Graph::ViewModels::GraphViewModel']]],
  ['initialize',['Initialize',['../class_holo_gen_1_1_u_i_1_1_image_1_1_viewer_1_1_zoom_border.html#a33f6e838a98f08e7758f639152fef915',1,'HoloGen::UI::Image::Viewer::ZoomBorder']]],
  ['integereditor',['IntegerEditor',['../class_holo_gen_1_1_u_i_1_1_option_editors_1_1_integer_editor.html#a1e78af4140469ee301b2e9a45c845f1a',1,'HoloGen::UI::OptionEditors::IntegerEditor']]],
  ['integeroption',['IntegerOption',['../class_holo_gen_1_1_hierarchy_1_1_option_types_1_1_integer_option.html#aa6e052e808a1c7913e4701118dcf9505',1,'HoloGen.Hierarchy.OptionTypes.IntegerOption.IntegerOption()'],['../class_holo_gen_1_1_hierarchy_1_1_option_types_1_1_integer_option.html#a1fb3d60c86504f7b610264715b71c006',1,'HoloGen.Hierarchy.OptionTypes.IntegerOption.IntegerOption(int value)']]],
  ['interpolate',['interpolate',['../class_holo_gen_1_1_utils_1_1_color_scheme.html#a8522de35059c0b3b12a5b197db39f1ee',1,'HoloGen::Utils::ColorScheme']]],
  ['interpolatebetween',['InterpolateBetween',['../class_holo_gen_1_1_utils_1_1_color_interpolator.html#a96af7fce64e367f326bd668d79f6608d',1,'HoloGen::Utils::ColorInterpolator']]]
];
