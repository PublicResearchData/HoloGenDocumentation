var searchData=
[
  ['generalsettingspage',['GeneralSettingsPage',['../class_holo_gen_1_1_settings_1_1_application_settings.html#ae8fa9cdecb2f068e797fb96769873889',1,'HoloGen::Settings::ApplicationSettings']]],
  ['graphviewmodel',['GraphViewModel',['../class_holo_gen_1_1_u_i_1_1_process_1_1_monitor_1_1_view_models_1_1_process_view_model.html#a1930019fef5eb5a544cc38587ba34e02',1,'HoloGen::UI::Process::Monitor::ViewModels::ProcessViewModel']]],
  ['greenonblack',['GreenonBlack',['../class_holo_gen_1_1_utils_1_1_color_schemes.html#a58a8d311c2af48a399017de40d10f4f9',1,'HoloGen::Utils::ColorSchemes']]],
  ['greenonwhite',['GreenonWhite',['../class_holo_gen_1_1_utils_1_1_color_schemes.html#a66f513ecc36d0de003d9367839d3b9f9',1,'HoloGen::Utils::ColorSchemes']]],
  ['gsalgorithm',['GSAlgorithm',['../class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_algorithm_possibilities.html#a7bcdfd879ef2e19e8cd13aefb3de469e',1,'HoloGen::Options::Algorithm::Algorithms::Type::AlgorithmPossibilities']]]
];
