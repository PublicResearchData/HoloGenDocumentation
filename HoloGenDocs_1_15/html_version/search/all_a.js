var searchData=
[
  ['jsondeserialiser',['JsonDeserialiser',['../class_holo_gen_1_1_serial_1_1_json_deserialiser.html',1,'HoloGen::Serial']]],
  ['jsondeserialiser_2ecs',['JsonDeserialiser.cs',['../_json_deserialiser_8cs.html',1,'']]],
  ['jsondeserialiser_3c_20applicationsettings_2c_20applicationsettingsversion_20_3e',['JsonDeserialiser&lt; ApplicationSettings, ApplicationSettingsVersion &gt;',['../class_holo_gen_1_1_serial_1_1_json_deserialiser.html',1,'HoloGen::Serial']]],
  ['jsondeserialiser_3c_20compleximage_2c_20compleximageversion_20_3e',['JsonDeserialiser&lt; ComplexImage, ComplexImageVersion &gt;',['../class_holo_gen_1_1_serial_1_1_json_deserialiser.html',1,'HoloGen::Serial']]],
  ['jsondeserialiser_3c_20optionsroot_2c_20optionsrootversion_20_3e',['JsonDeserialiser&lt; OptionsRoot, OptionsRootVersion &gt;',['../class_holo_gen_1_1_serial_1_1_json_deserialiser.html',1,'HoloGen::Serial']]],
  ['jsonexport',['JsonExport',['../class_holo_gen_1_1_image_1_1_complex_image.html#ad1a6284319e0d1654b41e98c7f9dc6ba',1,'HoloGen::Image::ComplexImage']]],
  ['jsonfileloader',['JsonFileLoader',['../class_holo_gen_1_1_i_o_1_1_json_file_loader.html',1,'HoloGen::IO']]],
  ['jsonfileloader_2ecs',['JsonFileLoader.cs',['../_json_file_loader_8cs.html',1,'']]],
  ['jsonfileloader_3c_20applicationsettings_2c_20applicationsettingsversion_2c_20jsonsettingsdeserialiser_20_3e',['JsonFileLoader&lt; ApplicationSettings, ApplicationSettingsVersion, JsonSettingsDeserialiser &gt;',['../class_holo_gen_1_1_i_o_1_1_json_file_loader.html',1,'HoloGen::IO']]],
  ['jsonfileloader_3c_20compleximage_2c_20compleximageversion_2c_20jsonimagedeserialiser_20_3e',['JsonFileLoader&lt; ComplexImage, ComplexImageVersion, JsonImageDeserialiser &gt;',['../class_holo_gen_1_1_i_o_1_1_json_file_loader.html',1,'HoloGen::IO']]],
  ['jsonfileloader_3c_20optionsroot_2c_20optionsrootversion_2c_20jsonoptionsdeserialiser_20_3e',['JsonFileLoader&lt; OptionsRoot, OptionsRootVersion, JsonOptionsDeserialiser &gt;',['../class_holo_gen_1_1_i_o_1_1_json_file_loader.html',1,'HoloGen::IO']]],
  ['jsonfilesaver',['JsonFileSaver',['../class_holo_gen_1_1_i_o_1_1_json_file_saver.html',1,'HoloGen::IO']]],
  ['jsonfilesaver_2ecs',['JsonFileSaver.cs',['../_json_file_saver_8cs.html',1,'']]],
  ['jsonfilesaver_3c_20applicationsettings_2c_20applicationsettingsversion_2c_20jsonsettingsserialiser_20_3e',['JsonFileSaver&lt; ApplicationSettings, ApplicationSettingsVersion, JsonSettingsSerialiser &gt;',['../class_holo_gen_1_1_i_o_1_1_json_file_saver.html',1,'HoloGen::IO']]],
  ['jsonfilesaver_3c_20compleximage_2c_20compleximageversion_2c_20jsonimageserialiser_20_3e',['JsonFileSaver&lt; ComplexImage, ComplexImageVersion, JsonImageSerialiser &gt;',['../class_holo_gen_1_1_i_o_1_1_json_file_saver.html',1,'HoloGen::IO']]],
  ['jsonfilesaver_3c_20optionsroot_2c_20optionsrootversion_2c_20jsonoptionsserialiser_20_3e',['JsonFileSaver&lt; OptionsRoot, OptionsRootVersion, JsonOptionsSerialiser &gt;',['../class_holo_gen_1_1_i_o_1_1_json_file_saver.html',1,'HoloGen::IO']]],
  ['jsonimagedeserialiser',['JsonImageDeserialiser',['../class_holo_gen_1_1_serial_1_1_json_image_deserialiser.html',1,'HoloGen::Serial']]],
  ['jsonimagedeserialiser_2ecs',['JsonImageDeserialiser.cs',['../_json_image_deserialiser_8cs.html',1,'']]],
  ['jsonimagefileloader',['JsonImageFileLoader',['../class_holo_gen_1_1_i_o_1_1_json_image_file_loader.html',1,'HoloGen::IO']]],
  ['jsonimagefileloader_2ecs',['JsonImageFileLoader.cs',['../_json_image_file_loader_8cs.html',1,'']]],
  ['jsonimagefilesaver',['JsonImageFileSaver',['../class_holo_gen_1_1_i_o_1_1_json_image_file_saver.html',1,'HoloGen::IO']]],
  ['jsonimagefilesaver_2ecs',['JsonImageFileSaver.cs',['../_json_image_file_saver_8cs.html',1,'']]],
  ['jsonimageserialiser',['JsonImageSerialiser',['../class_holo_gen_1_1_serial_1_1_json_image_serialiser.html',1,'HoloGen::Serial']]],
  ['jsonimageserialiser_2ecs',['JsonImageSerialiser.cs',['../_json_image_serialiser_8cs.html',1,'']]],
  ['jsonoptionsdeserialiser',['JsonOptionsDeserialiser',['../class_holo_gen_1_1_serial_1_1_json_options_deserialiser.html',1,'HoloGen::Serial']]],
  ['jsonoptionsdeserialiser_2ecs',['JsonOptionsDeserialiser.cs',['../_json_options_deserialiser_8cs.html',1,'']]],
  ['jsonoptionsfileloader',['JsonOptionsFileLoader',['../class_holo_gen_1_1_i_o_1_1_json_options_file_loader.html',1,'HoloGen::IO']]],
  ['jsonoptionsfileloader_2ecs_2ecs',['JsonOptionsFileLoader.cs.cs',['../_json_options_file_loader_8cs_8cs.html',1,'']]],
  ['jsonoptionsfilesaver',['JsonOptionsFileSaver',['../class_holo_gen_1_1_i_o_1_1_json_options_file_saver.html',1,'HoloGen::IO']]],
  ['jsonoptionsfilesaver_2ecs',['JsonOptionsFileSaver.cs',['../_json_options_file_saver_8cs.html',1,'']]],
  ['jsonoptionsserialiser',['JsonOptionsSerialiser',['../class_holo_gen_1_1_serial_1_1_json_options_serialiser.html',1,'HoloGen::Serial']]],
  ['jsonoptionsserialiser_2ecs',['JsonOptionsSerialiser.cs',['../_json_options_serialiser_8cs.html',1,'']]],
  ['jsonserialiser',['JsonSerialiser',['../class_holo_gen_1_1_serial_1_1_json_serialiser.html',1,'HoloGen.Serial.JsonSerialiser&lt; S, T &gt;'],['../class_holo_gen_1_1_serial_1_1_json_serialiser.html#ab5bd0251c4207ec162808810c752682f',1,'HoloGen.Serial.JsonSerialiser.JsonSerialiser()']]],
  ['jsonserialiser_2ecs',['JsonSerialiser.cs',['../_json_serialiser_8cs.html',1,'']]],
  ['jsonserialiser_3c_20applicationsettings_2c_20applicationsettingsversion_20_3e',['JsonSerialiser&lt; ApplicationSettings, ApplicationSettingsVersion &gt;',['../class_holo_gen_1_1_serial_1_1_json_serialiser.html',1,'HoloGen::Serial']]],
  ['jsonserialiser_3c_20compleximage_2c_20compleximageversion_20_3e',['JsonSerialiser&lt; ComplexImage, ComplexImageVersion &gt;',['../class_holo_gen_1_1_serial_1_1_json_serialiser.html',1,'HoloGen::Serial']]],
  ['jsonserialiser_3c_20optionsroot_2c_20optionsrootversion_20_3e',['JsonSerialiser&lt; OptionsRoot, OptionsRootVersion &gt;',['../class_holo_gen_1_1_serial_1_1_json_serialiser.html',1,'HoloGen::Serial']]],
  ['jsonsettingsdeserialiser',['JsonSettingsDeserialiser',['../class_holo_gen_1_1_serial_1_1_json_settings_deserialiser.html',1,'HoloGen::Serial']]],
  ['jsonsettingsdeserialiser_2ecs',['JsonSettingsDeserialiser.cs',['../_json_settings_deserialiser_8cs.html',1,'']]],
  ['jsonsettingsfileloader',['JsonSettingsFileLoader',['../class_holo_gen_1_1_i_o_1_1_json_settings_file_loader.html',1,'HoloGen::IO']]],
  ['jsonsettingsfileloader_2ecs',['JsonSettingsFileLoader.cs',['../_json_settings_file_loader_8cs.html',1,'']]],
  ['jsonsettingsfilesaver',['JsonSettingsFileSaver',['../class_holo_gen_1_1_i_o_1_1_json_settings_file_saver.html',1,'HoloGen::IO']]],
  ['jsonsettingsfilesaver_2ecs',['JsonSettingsFileSaver.cs',['../_json_settings_file_saver_8cs.html',1,'']]],
  ['jsonsettingsserialiser',['JsonSettingsSerialiser',['../class_holo_gen_1_1_serial_1_1_json_settings_serialiser.html',1,'HoloGen::Serial']]],
  ['jsonsettingsserialiser_2ecs_2ecs',['JsonSettingsSerialiser.cs.cs',['../_json_settings_serialiser_8cs_8cs.html',1,'']]]
];
