var searchData=
[
  ['phase',['Phase',['../namespace_holo_gen_1_1_alg_1_1_base_1_1_cuda.html#ae4bd67066d1a8e17de09ac30819bd6f0a5e35d7fff7b903516cba242ff68fc6d3',1,'HoloGen::Alg::Base::Cuda::Phase()'],['../namespace_holo_gen_1_1_alg_1_1_base_1_1_man.html#a994d68c7d0278e51b98791528b2175dda5e35d7fff7b903516cba242ff68fc6d3',1,'HoloGen::Alg::Base::Man::Phase()'],['../namespace_holo_gen_1_1_image.html#ac3e20ca9c7b958bf55430787ad5f0cd8a5e35d7fff7b903516cba242ff68fc6d3',1,'HoloGen.Image.Phase()']]],
  ['planar',['Planar',['../namespace_holo_gen_1_1_alg_1_1_base_1_1_cuda.html#abaa59b69be9fc34f31f3e640528451e6a93a097009b6443e711996c50b5354adf',1,'HoloGen::Alg::Base::Cuda::Planar()'],['../namespace_holo_gen_1_1_alg_1_1_base_1_1_man.html#ad304b1889f2982e4c26ba4387efb0097a93a097009b6443e711996c50b5354adf',1,'HoloGen::Alg::Base::Man::Planar()']]],
  ['point',['Point',['../namespace_holo_gen_1_1_u_i_1_1_mask_1_1_drawer.html#a6b82669a8a43d9ad72760238a9e26802a2a3cd5946cfd317eb99c3d32e35e2d4c',1,'HoloGen::UI::Mask::Drawer']]]
];
