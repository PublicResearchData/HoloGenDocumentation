var dir_49e583c1cd82d035c4ffaea82bc0d8d8 =
[
    [ "Type", "dir_014ece8eba3f668e6dabece04c5956d4.html", "dir_014ece8eba3f668e6dabece04c5956d4" ],
    [ "ErrorValue.cs", "_error_value_8cs.html", [
      [ "ErrorValue", "class_holo_gen_1_1_options_1_1_algorithm_1_1_termination_1_1_error_value.html", "class_holo_gen_1_1_options_1_1_algorithm_1_1_termination_1_1_error_value" ]
    ] ],
    [ "NumIterations.cs", "_num_iterations_8cs.html", [
      [ "NumIterations", "class_holo_gen_1_1_options_1_1_algorithm_1_1_termination_1_1_num_iterations.html", "class_holo_gen_1_1_options_1_1_algorithm_1_1_termination_1_1_num_iterations" ]
    ] ],
    [ "TerminationFolder.cs", "_termination_folder_8cs.html", [
      [ "TerminationFolder", "class_holo_gen_1_1_options_1_1_algorithm_1_1_termination_1_1_type_1_1_termination_folder.html", "class_holo_gen_1_1_options_1_1_algorithm_1_1_termination_1_1_type_1_1_termination_folder" ]
    ] ]
];