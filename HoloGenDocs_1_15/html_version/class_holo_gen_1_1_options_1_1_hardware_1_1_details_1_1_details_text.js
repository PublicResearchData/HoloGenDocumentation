var class_holo_gen_1_1_options_1_1_hardware_1_1_details_1_1_details_text =
[
    [ "DetailsText", "class_holo_gen_1_1_options_1_1_hardware_1_1_details_1_1_details_text.html#a9adb515feb0a91a528ae3ae2a2453f60", null ],
    [ "_default", "class_holo_gen_1_1_options_1_1_hardware_1_1_details_1_1_details_text.html#a1afa2c7797125854c6571872c1ba7f13", null ],
    [ "_value2", "class_holo_gen_1_1_options_1_1_hardware_1_1_details_1_1_details_text.html#ae9ab9c8f388f056b0aaec40112ee0db4", null ],
    [ "CanClear", "class_holo_gen_1_1_options_1_1_hardware_1_1_details_1_1_details_text.html#a582d8c736cca496a73d80c47e444549c", null ],
    [ "Default", "class_holo_gen_1_1_options_1_1_hardware_1_1_details_1_1_details_text.html#a308776efe8040d375bec884744d4cbe3", null ],
    [ "Error", "class_holo_gen_1_1_options_1_1_hardware_1_1_details_1_1_details_text.html#a23cc5a937c347eeb46408bfe99d75f99", null ],
    [ "Name", "class_holo_gen_1_1_options_1_1_hardware_1_1_details_1_1_details_text.html#aac0fa79d99da6d1bc7d258a72d8790a8", null ],
    [ "ToolTip", "class_holo_gen_1_1_options_1_1_hardware_1_1_details_1_1_details_text.html#acb9e48cedd1ae89bd0e80037a20e25d6", null ],
    [ "Valid", "class_holo_gen_1_1_options_1_1_hardware_1_1_details_1_1_details_text.html#aba82f8b8a761e143b0066d829edf26be", null ],
    [ "Watermark", "class_holo_gen_1_1_options_1_1_hardware_1_1_details_1_1_details_text.html#a6a85bc2a80dd96f5c2ff9d251c2e3187", null ],
    [ "Value", "class_holo_gen_1_1_options_1_1_hardware_1_1_details_1_1_details_text.html#a173408572d3a70aa48e2a984922236ff", null ]
];