var namespace_holo_gen_1_1_options_1_1_algorithm =
[
    [ "Algorithms", "namespace_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms.html", "namespace_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms" ],
    [ "ErrorCalculation", "namespace_holo_gen_1_1_options_1_1_algorithm_1_1_error_calculation.html", "namespace_holo_gen_1_1_options_1_1_algorithm_1_1_error_calculation" ],
    [ "Target", "namespace_holo_gen_1_1_options_1_1_algorithm_1_1_target.html", "namespace_holo_gen_1_1_options_1_1_algorithm_1_1_target" ],
    [ "Termination", "namespace_holo_gen_1_1_options_1_1_algorithm_1_1_termination.html", "namespace_holo_gen_1_1_options_1_1_algorithm_1_1_termination" ],
    [ "AlgorithmsPage", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_page.html", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_page" ]
];