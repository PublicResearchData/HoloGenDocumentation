var class_holo_gen_1_1_alg_1_1_base_1_1_man_1_1_algorithm_man =
[
    [ "AlgorithmMan", "class_holo_gen_1_1_alg_1_1_base_1_1_man_1_1_algorithm_man.html#a7fb1d35fb293ee12b4a2d9972f460e63", null ],
    [ "~AlgorithmMan", "class_holo_gen_1_1_alg_1_1_base_1_1_man_1_1_algorithm_man.html#ab5de239fb1aa5c881c30ab18643792b1", null ],
    [ "AddIlluminationImage", "class_holo_gen_1_1_alg_1_1_base_1_1_man_1_1_algorithm_man.html#af4f3da16d6e743388ea9b8cdfb6fecb7", null ],
    [ "AddInitalImage", "class_holo_gen_1_1_alg_1_1_base_1_1_man_1_1_algorithm_man.html#a94ba9f95f71fadad4f4f763293286221", null ],
    [ "AddTargetImage", "class_holo_gen_1_1_alg_1_1_base_1_1_man_1_1_algorithm_man.html#a78cd711e8764035243373debf1491670", null ],
    [ "GetResult", "class_holo_gen_1_1_alg_1_1_base_1_1_man_1_1_algorithm_man.html#a97a642a163156c5d3aa538d18d6f1d7e", null ],
    [ "RunCleanup", "class_holo_gen_1_1_alg_1_1_base_1_1_man_1_1_algorithm_man.html#a4e4d021d78da222bc8c622e4af5985f7", null ],
    [ "RunIterations", "class_holo_gen_1_1_alg_1_1_base_1_1_man_1_1_algorithm_man.html#a1e7e76ef4eadc5adca9b9d0cb6e8b0ef", null ],
    [ "RunStartup", "class_holo_gen_1_1_alg_1_1_base_1_1_man_1_1_algorithm_man.html#a2957651fcab6d71a6852c106a3bbba2b", null ],
    [ "SetBaseParameters", "class_holo_gen_1_1_alg_1_1_base_1_1_man_1_1_algorithm_man.html#ab24268b167d60cc3404bf5b8e47d1fe7", null ],
    [ "SetLogger", "class_holo_gen_1_1_alg_1_1_base_1_1_man_1_1_algorithm_man.html#a97beb80ddf919eb7bfe2f67657239e2a", null ],
    [ "nativeAlgBase", "class_holo_gen_1_1_alg_1_1_base_1_1_man_1_1_algorithm_man.html#a0b8cf816c248aab0b710aa0f8a97ad8c", null ]
];