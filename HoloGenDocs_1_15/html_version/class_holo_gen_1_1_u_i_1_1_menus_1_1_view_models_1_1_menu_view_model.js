var class_holo_gen_1_1_u_i_1_1_menus_1_1_view_models_1_1_menu_view_model =
[
    [ "MenuViewModel", "class_holo_gen_1_1_u_i_1_1_menus_1_1_view_models_1_1_menu_view_model.html#a78c48dea6c3b7c7870e3ea6e5111fc0c", null ],
    [ "AccentColors", "class_holo_gen_1_1_u_i_1_1_menus_1_1_view_models_1_1_menu_view_model.html#abdc25afc68eab460cc64b723ca8088ea", null ],
    [ "AccentMenuViewModel", "class_holo_gen_1_1_u_i_1_1_menus_1_1_view_models_1_1_menu_view_model.html#ae20d7122ea3301ba22e395795b689c7b", null ],
    [ "AppThemes", "class_holo_gen_1_1_u_i_1_1_menus_1_1_view_models_1_1_menu_view_model.html#ae03659c8aa048ec439aa1c841e5512a7", null ],
    [ "FileMenuViewModel", "class_holo_gen_1_1_u_i_1_1_menus_1_1_view_models_1_1_menu_view_model.html#a38994a211a9b8d2e496f2c60c084a20e", null ],
    [ "HelpMenuViewModel", "class_holo_gen_1_1_u_i_1_1_menus_1_1_view_models_1_1_menu_view_model.html#a2b92f08d0f76a4323cf7e2a72d2b7881", null ],
    [ "SettingsMenuViewModel", "class_holo_gen_1_1_u_i_1_1_menus_1_1_view_models_1_1_menu_view_model.html#a9731d07c29ec7b299161ddce996dfc6b", null ],
    [ "ThemeMenuViewModel", "class_holo_gen_1_1_u_i_1_1_menus_1_1_view_models_1_1_menu_view_model.html#ae9cf79551772785ef80da8306c202655", null ]
];