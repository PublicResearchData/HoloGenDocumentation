var _deserialise_result_8cs =
[
    [ "DeserialiseResult", "_deserialise_result_8cs.html#a3907f9fec61396010defe99a363560e6", [
      [ "Success", "_deserialise_result_8cs.html#a3907f9fec61396010defe99a363560e6a505a83f220c02df2f85c3810cd9ceb38", null ],
      [ "UnknownError", "_deserialise_result_8cs.html#a3907f9fec61396010defe99a363560e6abfaef30f1c8011c5cefa38ae470fb7aa", null ],
      [ "NoSuchFile", "_deserialise_result_8cs.html#a3907f9fec61396010defe99a363560e6ae13bda1c3f7ac029e44f9887fe887c95", null ],
      [ "FileFromEarlierVersion", "_deserialise_result_8cs.html#a3907f9fec61396010defe99a363560e6aa093cedc8a302f7141afac87811bfc1e", null ],
      [ "FileFromLaterVersion", "_deserialise_result_8cs.html#a3907f9fec61396010defe99a363560e6a023b3709e129bcbc4f7067e515b11885", null ],
      [ "BadFormat", "_deserialise_result_8cs.html#a3907f9fec61396010defe99a363560e6a56644c26470cd88df08b0b3dc5827aa0", null ]
    ] ]
];