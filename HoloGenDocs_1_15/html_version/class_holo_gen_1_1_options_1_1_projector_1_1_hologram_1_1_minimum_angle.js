var class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_minimum_angle =
[
    [ "Default", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_minimum_angle.html#a38e975a5c9ca8720958cebb5899e2fa2", null ],
    [ "Enabled", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_minimum_angle.html#ac31d5961261274740afd3a9b6ffad4e7", null ],
    [ "MaxValue", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_minimum_angle.html#a10a4d3932307407feb2703a55f6db58e", null ],
    [ "MinValue", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_minimum_angle.html#a5b3dcf2ca26465e96aa4c4c3ca6460a2", null ],
    [ "Name", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_minimum_angle.html#a68fed7affa9e5b1d6739cd0fc54984c5", null ],
    [ "ToolTip", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_minimum_angle.html#a771732ef853688bf73ad08812357b592", null ],
    [ "Watermark", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_minimum_angle.html#a9c33b72d1812de92e4df137a00a6d30c", null ]
];