var class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_wavelength =
[
    [ "Default", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_wavelength.html#a2e22c6e3a70a57e34ce78ba0f4b0280d", null ],
    [ "Enabled", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_wavelength.html#a7a8b09feb915fb9963430caf80956318", null ],
    [ "MaxValue", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_wavelength.html#ae8459c401bcdb7063b260fb6a819108f", null ],
    [ "MinValue", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_wavelength.html#a79a930c3d3d9527cd470ce9b7d13b193", null ],
    [ "Name", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_wavelength.html#a62fb8de647de9d96edfc8ba388fe5afd", null ],
    [ "ToolTip", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_wavelength.html#a1db415df6084d35b8cb8b364a4c1f14b", null ],
    [ "Watermark", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_wavelength.html#a232a32d4315240db15b605e53867f851", null ]
];