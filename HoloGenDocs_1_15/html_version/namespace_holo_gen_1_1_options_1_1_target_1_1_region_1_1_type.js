var namespace_holo_gen_1_1_options_1_1_target_1_1_region_1_1_type =
[
    [ "RegionFromFile", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_type_1_1_region_from_file.html", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_type_1_1_region_from_file" ],
    [ "RegionIntensity", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_type_1_1_region_intensity.html", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_type_1_1_region_intensity" ],
    [ "RegionNone", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_type_1_1_region_none.html", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_type_1_1_region_none" ],
    [ "RegionOption", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_type_1_1_region_option.html", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_type_1_1_region_option" ],
    [ "RegionPossibilities", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_type_1_1_region_possibilities.html", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_type_1_1_region_possibilities" ],
    [ "RegionPossibility", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_type_1_1_region_possibility.html", null ],
    [ "SymmetryHorizontal", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_type_1_1_symmetry_horizontal.html", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_type_1_1_symmetry_horizontal" ],
    [ "SymmetryNone", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_type_1_1_symmetry_none.html", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_type_1_1_symmetry_none" ],
    [ "SymmetryOption", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_type_1_1_symmetry_option.html", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_type_1_1_symmetry_option" ],
    [ "SymmetryPossibilities", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_type_1_1_symmetry_possibilities.html", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_type_1_1_symmetry_possibilities" ],
    [ "SymmetryPossibility", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_type_1_1_symmetry_possibility.html", null ],
    [ "SymmetryVertical", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_type_1_1_symmetry_vertical.html", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_type_1_1_symmetry_vertical" ],
    [ "VariantOption", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_type_1_1_variant_option.html", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_type_1_1_variant_option" ],
    [ "VariantPixel", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_type_1_1_variant_pixel.html", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_type_1_1_variant_pixel" ],
    [ "VariantPossibilities", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_type_1_1_variant_possibilities.html", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_type_1_1_variant_possibilities" ],
    [ "VariantPossibility", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_type_1_1_variant_possibility.html", null ],
    [ "VariantRound", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_type_1_1_variant_round.html", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_type_1_1_variant_round" ],
    [ "VariantSquare", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_type_1_1_variant_square.html", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_type_1_1_variant_square" ]
];