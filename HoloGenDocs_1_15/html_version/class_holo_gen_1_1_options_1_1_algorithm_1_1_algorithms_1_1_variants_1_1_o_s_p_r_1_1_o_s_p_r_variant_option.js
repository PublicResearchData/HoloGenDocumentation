var class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_variants_1_1_o_s_p_r_1_1_o_s_p_r_variant_option =
[
    [ "Name", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_variants_1_1_o_s_p_r_1_1_o_s_p_r_variant_option.html#af7251e2e2aaa2bd9c295ea9d5e07e7d6", null ],
    [ "Possibilities", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_variants_1_1_o_s_p_r_1_1_o_s_p_r_variant_option.html#a1652bd6652a011ae04a1af17964dc1db", null ],
    [ "Possibilities2", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_variants_1_1_o_s_p_r_1_1_o_s_p_r_variant_option.html#a35ee054cca9077811a31dd5201fb15e9", null ],
    [ "ToolTip", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_variants_1_1_o_s_p_r_1_1_o_s_p_r_variant_option.html#a1fc29fb120c759f43268557ed2301947", null ],
    [ "VariantPossibility", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_variants_1_1_o_s_p_r_1_1_o_s_p_r_variant_option.html#aab91a1d3829a25c5b62f182eeaa4fe95", null ]
];