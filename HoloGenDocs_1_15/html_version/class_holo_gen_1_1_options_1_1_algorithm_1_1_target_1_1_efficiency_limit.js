var class_holo_gen_1_1_options_1_1_algorithm_1_1_target_1_1_efficiency_limit =
[
    [ "Default", "class_holo_gen_1_1_options_1_1_algorithm_1_1_target_1_1_efficiency_limit.html#ac79283efad0cde592ee6709b199da855", null ],
    [ "MaxValue", "class_holo_gen_1_1_options_1_1_algorithm_1_1_target_1_1_efficiency_limit.html#aab9e66d0c5ec9a324d671ea749cce755", null ],
    [ "MinValue", "class_holo_gen_1_1_options_1_1_algorithm_1_1_target_1_1_efficiency_limit.html#a33113241e57ae74631bc6469a466e04a", null ],
    [ "Name", "class_holo_gen_1_1_options_1_1_algorithm_1_1_target_1_1_efficiency_limit.html#ae12f226a4bb59951fa25aee5e7088087", null ],
    [ "ToolTip", "class_holo_gen_1_1_options_1_1_algorithm_1_1_target_1_1_efficiency_limit.html#a4ba335d7e8b9daef0d8527c65ae108cb", null ],
    [ "Watermark", "class_holo_gen_1_1_options_1_1_algorithm_1_1_target_1_1_efficiency_limit.html#abb3ac789f18b722370ce26ab1ae07c63", null ]
];