var class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_pixel_count =
[
    [ "Default", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_pixel_count.html#a5ee106bdaa27f3d2235a3e489aacffc1", null ],
    [ "MaxValue", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_pixel_count.html#a4def287012e395fc5f7cf152c39a8108", null ],
    [ "MinValue", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_pixel_count.html#a8918da569e92643be3f1ee30d23d1fb1", null ],
    [ "Name", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_pixel_count.html#a6543b9b70a6757e19538376d3a1b21b2", null ],
    [ "ToolTip", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_pixel_count.html#a0f79862bfe5a973503b73cc3337d36be", null ],
    [ "Watermark", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_pixel_count.html#a1366d70e8fd8cfb1b36cd7d69a8c3574", null ]
];