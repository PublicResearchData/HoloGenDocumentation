var class_holo_gen_1_1_options_1_1_algorithm_1_1_error_calculation_1_1_error_folder =
[
    [ "ErrorFolder", "class_holo_gen_1_1_options_1_1_algorithm_1_1_error_calculation_1_1_error_folder.html#a3aec3b76c10cf536b824b291e91e2829", null ],
    [ "Name", "class_holo_gen_1_1_options_1_1_algorithm_1_1_error_calculation_1_1_error_folder.html#a60ed38b7d3144afeb6d9454e72b4183c", null ],
    [ "ToolTip", "class_holo_gen_1_1_options_1_1_algorithm_1_1_error_calculation_1_1_error_folder.html#a6b9055a4c41ae31b57d3c98e6a03393c", null ],
    [ "ErrorNormalisation", "class_holo_gen_1_1_options_1_1_algorithm_1_1_error_calculation_1_1_error_folder.html#ab14f10e568f854ed9908f6703846a9c9", null ],
    [ "ErrorTypeOption", "class_holo_gen_1_1_options_1_1_algorithm_1_1_error_calculation_1_1_error_folder.html#a96b1c7d0ca415dde7785d1e1922bd0f5", null ]
];