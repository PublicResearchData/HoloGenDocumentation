var class_holo_gen_1_1_settings_1_1_general_1_1_file_1_1_warning_settings_folder =
[
    [ "WarningSettingsFolder", "class_holo_gen_1_1_settings_1_1_general_1_1_file_1_1_warning_settings_folder.html#a99bef121438c4cf7e587d0dd70cd691a", null ],
    [ "Name", "class_holo_gen_1_1_settings_1_1_general_1_1_file_1_1_warning_settings_folder.html#aeb833e6b99e625ffa8ec158fb6cc6fa9", null ],
    [ "ToolTip", "class_holo_gen_1_1_settings_1_1_general_1_1_file_1_1_warning_settings_folder.html#ace0ec2bfee7007ce1f8e9a67332973ae", null ],
    [ "ShowCloseUnsavedFileWarning", "class_holo_gen_1_1_settings_1_1_general_1_1_file_1_1_warning_settings_folder.html#a1a22afab5a870a62bee8e2948a78c688", null ],
    [ "ShowLargeMatExportWarning", "class_holo_gen_1_1_settings_1_1_general_1_1_file_1_1_warning_settings_folder.html#acc29fe9ab01fb675128c89b5d9b7eaad", null ],
    [ "ShowLargeMatImportWarning", "class_holo_gen_1_1_settings_1_1_general_1_1_file_1_1_warning_settings_folder.html#ae213e7292cc1900e34f88047968a757d", null ]
];