var hierarchy =
[
    [ "HoloGen.UI.ViewModels.AbstractTabViewModel< ComplexImage >", "class_holo_gen_1_1_u_i_1_1_view_models_1_1_abstract_tab_view_model.html", [
      [ "HoloGen.UI.ViewModels.HologramTabViewModel", "class_holo_gen_1_1_u_i_1_1_view_models_1_1_hologram_tab_view_model.html", null ]
    ] ],
    [ "HoloGen.UI.ViewModels.AbstractTabViewModel< FileInfo >", "class_holo_gen_1_1_u_i_1_1_view_models_1_1_abstract_tab_view_model.html", [
      [ "HoloGen.UI.ViewModels.PdfTabViewModel", "class_holo_gen_1_1_u_i_1_1_view_models_1_1_pdf_tab_view_model.html", null ]
    ] ],
    [ "HoloGen.UI.ViewModels.AbstractTabViewModel< object >", "class_holo_gen_1_1_u_i_1_1_view_models_1_1_abstract_tab_view_model.html", [
      [ "HoloGen.UI.ViewModels.ProcessTabViewModel", "class_holo_gen_1_1_u_i_1_1_view_models_1_1_process_tab_view_model.html", null ]
    ] ],
    [ "HoloGen.UI.ViewModels.AbstractTabViewModel< OptionsRoot >", "class_holo_gen_1_1_u_i_1_1_view_models_1_1_abstract_tab_view_model.html", [
      [ "HoloGen.UI.ViewModels.BatchTabViewModel", "class_holo_gen_1_1_u_i_1_1_view_models_1_1_batch_tab_view_model.html", null ],
      [ "HoloGen.UI.ViewModels.SetupTabViewModel", "class_holo_gen_1_1_u_i_1_1_view_models_1_1_setup_tab_view_model.html", null ]
    ] ],
    [ "HoloGen.Controller.AlgorithmController", "class_holo_gen_1_1_controller_1_1_algorithm_controller.html", null ],
    [ "HoloGen::Alg::Base::Man::AlgorithmMan", "class_holo_gen_1_1_alg_1_1_base_1_1_man_1_1_algorithm_man.html", [
      [ "HoloGen::Alg::Common::Man::AlgorithmFFTMan", "class_holo_gen_1_1_alg_1_1_common_1_1_man_1_1_algorithm_f_f_t_man.html", null ],
      [ "HoloGen::Alg::GS::Man::AlgorithmGSMan", "class_holo_gen_1_1_alg_1_1_g_s_1_1_man_1_1_algorithm_g_s_man.html", [
        [ "HoloGen::Alg::GS::Man::VariantGSStandardMan", "class_holo_gen_1_1_alg_1_1_g_s_1_1_man_1_1_variant_g_s_standard_man.html", null ]
      ] ]
    ] ],
    [ "HoloGen.Alg.Base.Wrap.AlgorithmWrap", "class_holo_gen_1_1_alg_1_1_base_1_1_wrap_1_1_algorithm_wrap.html", [
      [ "HoloGen.Alg.Common.AlgorithmFFTWrap", "class_holo_gen_1_1_alg_1_1_common_1_1_algorithm_f_f_t_wrap.html", null ],
      [ "HoloGen.Alg.GS.AlgorithmGSWrap", "class_holo_gen_1_1_alg_1_1_g_s_1_1_algorithm_g_s_wrap.html", [
        [ "HoloGen.Alg.GS.VariantGSStandardWrap", "class_holo_gen_1_1_alg_1_1_g_s_1_1_variant_g_s_standard_wrap.html", null ]
      ] ]
    ] ],
    [ "Application", null, [
      [ "HoloGen.UI.App", "class_holo_gen_1_1_u_i_1_1_app.html", null ]
    ] ],
    [ "HoloGen.Utils.BitmapUtils", "class_holo_gen_1_1_utils_1_1_bitmap_utils.html", null ],
    [ "Border", null, [
      [ "HoloGen.UI.Image.Viewer.ZoomBorder", "class_holo_gen_1_1_u_i_1_1_image_1_1_viewer_1_1_zoom_border.html", null ]
    ] ],
    [ "HoloGen.Utils.ColorInterpolator", "class_holo_gen_1_1_utils_1_1_color_interpolator.html", null ],
    [ "HoloGen.Utils.ColorScheme", "class_holo_gen_1_1_utils_1_1_color_scheme.html", null ],
    [ "HoloGen.UI.Mask.Model.CommonProperty", "class_holo_gen_1_1_u_i_1_1_mask_1_1_model_1_1_common_property.html", null ],
    [ "DataTemplateSelector", null, [
      [ "HoloGen.UI.Common.TabDataTemplateSelector", "class_holo_gen_1_1_u_i_1_1_common_1_1_tab_data_template_selector.html", null ],
      [ "HoloGen.UI.Hamburger.PageDataTemplateSelector", "class_holo_gen_1_1_u_i_1_1_hamburger_1_1_page_data_template_selector.html", null ],
      [ "HoloGen.UI.OptionEditors.OptionDataTemplateSelector", "class_holo_gen_1_1_u_i_1_1_option_editors_1_1_option_data_template_selector.html", null ]
    ] ],
    [ "HoloGen.Serial.Deserialiser< S, T >", "class_holo_gen_1_1_serial_1_1_deserialiser.html", [
      [ "HoloGen.Serial.JsonDeserialiser< S, T >", "class_holo_gen_1_1_serial_1_1_json_deserialiser.html", null ]
    ] ],
    [ "HoloGen.Utils.Dimension", "class_holo_gen_1_1_utils_1_1_dimension.html", null ],
    [ "HoloGen.UI.Mask.Drawer.Drawer", "class_holo_gen_1_1_u_i_1_1_mask_1_1_drawer_1_1_drawer.html", [
      [ "HoloGen.UI.Mask.Drawer.EllipseDrawer", "class_holo_gen_1_1_u_i_1_1_mask_1_1_drawer_1_1_ellipse_drawer.html", null ],
      [ "HoloGen.UI.Mask.Drawer.PointDrawer", "class_holo_gen_1_1_u_i_1_1_mask_1_1_drawer_1_1_point_drawer.html", null ],
      [ "HoloGen.UI.Mask.Drawer.PolygonDrawer", "class_holo_gen_1_1_u_i_1_1_mask_1_1_drawer_1_1_polygon_drawer.html", null ],
      [ "HoloGen.UI.Mask.Drawer.RectDrawer", "class_holo_gen_1_1_u_i_1_1_mask_1_1_drawer_1_1_rect_drawer.html", null ]
    ] ],
    [ "HoloGen.UI.Mask.Model.DShape", "class_holo_gen_1_1_u_i_1_1_mask_1_1_model_1_1_d_shape.html", [
      [ "HoloGen.UI.Mask.Model.DEllipse", "class_holo_gen_1_1_u_i_1_1_mask_1_1_model_1_1_d_ellipse.html", null ],
      [ "HoloGen.UI.Mask.Model.DPoint", "class_holo_gen_1_1_u_i_1_1_mask_1_1_model_1_1_d_point.html", null ],
      [ "HoloGen.UI.Mask.Model.DPolygon", "class_holo_gen_1_1_u_i_1_1_mask_1_1_model_1_1_d_polygon.html", null ],
      [ "HoloGen.UI.Mask.Model.DRect", "class_holo_gen_1_1_u_i_1_1_mask_1_1_model_1_1_d_rect.html", null ]
    ] ],
    [ "HoloGen.IO.FileLoader< S, T, U >", "class_holo_gen_1_1_i_o_1_1_file_loader.html", [
      [ "HoloGen.IO.JsonFileLoader< S, T, U >", "class_holo_gen_1_1_i_o_1_1_json_file_loader.html", null ]
    ] ],
    [ "HoloGen.UI.Utils.FileLocations", "class_holo_gen_1_1_u_i_1_1_utils_1_1_file_locations.html", null ],
    [ "HoloGen.IO.FileSaver< S, T, U >", "class_holo_gen_1_1_i_o_1_1_file_saver.html", [
      [ "HoloGen.IO.JsonFileSaver< S, T, U >", "class_holo_gen_1_1_i_o_1_1_json_file_saver.html", null ]
    ] ],
    [ "HoloGen.Utils.FileUtils", "class_holo_gen_1_1_utils_1_1_file_utils.html", null ],
    [ "HoloGen.Utils.GenericCache< K1, A, B >", "class_holo_gen_1_1_utils_1_1_generic_cache.html", null ],
    [ "HoloGen.Utils.GenericCache< K1, K2, A, B, C >", "class_holo_gen_1_1_utils_1_1_generic_cache.html", null ],
    [ "HoloGen.Utils.GenericCache< K1, K2, K3, A, B, C, D >", "class_holo_gen_1_1_utils_1_1_generic_cache.html", null ],
    [ "HoloGen.Utils.GenericCache< K1, K2, K3, K4, A, B, C, D, E >", "class_holo_gen_1_1_utils_1_1_generic_cache.html", null ],
    [ "HoloGen.Utils.GenericCache< TransformType, ImageViewType, ColorScheme, ImageScaleType, HoloGen.Image.ComplexImage, TransformImage, FactoredImage, ColoredImage, HoloGen.Image.ImageView >", "class_holo_gen_1_1_utils_1_1_generic_cache.html", null ],
    [ "HoloGen::Alg::Base::Cuda::Globals", "class_holo_gen_1_1_alg_1_1_base_1_1_cuda_1_1_globals.html", null ],
    [ "HoloGen.UI.Graph.ViewModels.GraphItemViewModel", "class_holo_gen_1_1_u_i_1_1_graph_1_1_view_models_1_1_graph_item_view_model.html", null ],
    [ "HoloGen.UI.Hamburger.ViewModels.HamburgerItem< HierarchyPage >", "class_holo_gen_1_1_u_i_1_1_hamburger_1_1_view_models_1_1_hamburger_item.html", [
      [ "HoloGen.UI.Hamburger.ViewModels.HamburgerMenuItem", "class_holo_gen_1_1_u_i_1_1_hamburger_1_1_view_models_1_1_hamburger_menu_item.html", null ]
    ] ],
    [ "HoloGen.Hierarchy.Hierarchy.HierarchyElement< HierarchyFolder >", "class_holo_gen_1_1_hierarchy_1_1_hierarchy_1_1_hierarchy_element.html", [
      [ "HoloGen.Hierarchy.Hierarchy.HierarchyPage", "class_holo_gen_1_1_hierarchy_1_1_hierarchy_1_1_hierarchy_page.html", [
        [ "HoloGen.Image.Options.ImageOptions", "class_holo_gen_1_1_image_1_1_options_1_1_image_options.html", [
          [ "HoloGen.Mask.Options.MaskedImageOptions", "class_holo_gen_1_1_mask_1_1_options_1_1_masked_image_options.html", null ]
        ] ],
        [ "HoloGen.Image.Options.Type.ImageOptions", "class_holo_gen_1_1_image_1_1_options_1_1_type_1_1_image_options.html", null ],
        [ "HoloGen.Options.Algorithm.AlgorithmsPage", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_page.html", null ],
        [ "HoloGen.Options.Hardware.HardwarePage", "class_holo_gen_1_1_options_1_1_hardware_1_1_hardware_page.html", null ],
        [ "HoloGen.Options.OutputPage", "class_holo_gen_1_1_options_1_1_output_page.html", null ],
        [ "HoloGen.Options.Projector.ProjectorPage", "class_holo_gen_1_1_options_1_1_projector_1_1_projector_page.html", null ],
        [ "HoloGen.Options.Target.TargetPage", "class_holo_gen_1_1_options_1_1_target_1_1_target_page.html", null ],
        [ "HoloGen.Process.Options.ProcessOptions", "class_holo_gen_1_1_process_1_1_options_1_1_process_options.html", null ],
        [ "HoloGen.Settings.General.GeneralSettingsPage", "class_holo_gen_1_1_settings_1_1_general_1_1_general_settings_page.html", null ]
      ] ]
    ] ],
    [ "HoloGen.Hierarchy.Hierarchy.HierarchyElement< HierarchyPage >", "class_holo_gen_1_1_hierarchy_1_1_hierarchy_1_1_hierarchy_element.html", [
      [ "HoloGen.Hierarchy.Hierarchy.HierarchyRoot", "class_holo_gen_1_1_hierarchy_1_1_hierarchy_1_1_hierarchy_root.html", [
        [ "HoloGen.Options.OptionsRoot", "class_holo_gen_1_1_options_1_1_options_root.html", null ],
        [ "HoloGen.Settings.ApplicationSettings", "class_holo_gen_1_1_settings_1_1_application_settings.html", null ]
      ] ]
    ] ],
    [ "HoloGen.Hierarchy.Hierarchy.HierarchyElement< IOption >", "class_holo_gen_1_1_hierarchy_1_1_hierarchy_1_1_hierarchy_element.html", [
      [ "HoloGen.Hierarchy.Hierarchy.HierarchyFolder", "class_holo_gen_1_1_hierarchy_1_1_hierarchy_1_1_hierarchy_folder.html", [
        [ "HoloGen.Image.Options.ImageViewFolder", "class_holo_gen_1_1_image_1_1_options_1_1_image_view_folder.html", null ],
        [ "HoloGen.Mask.Options.MaskFolder", "class_holo_gen_1_1_mask_1_1_options_1_1_mask_folder.html", null ],
        [ "HoloGen.Options.Algorithm.Algorithms.AlgorithmsFolder", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_algorithms_folder.html", null ],
        [ "HoloGen.Options.Algorithm.ErrorCalculation.ErrorFolder", "class_holo_gen_1_1_options_1_1_algorithm_1_1_error_calculation_1_1_error_folder.html", null ],
        [ "HoloGen.Options.Algorithm.Target.TargetFolder", "class_holo_gen_1_1_options_1_1_algorithm_1_1_target_1_1_target_folder.html", null ],
        [ "HoloGen.Options.Algorithm.Termination.Type.TerminationFolder", "class_holo_gen_1_1_options_1_1_algorithm_1_1_termination_1_1_type_1_1_termination_folder.html", null ],
        [ "HoloGen.Options.Hardware.Details.DetailsFolder", "class_holo_gen_1_1_options_1_1_hardware_1_1_details_1_1_details_folder.html", null ],
        [ "HoloGen.Options.Hardware.Hardware.HardwareFolder", "class_holo_gen_1_1_options_1_1_hardware_1_1_hardware_1_1_hardware_folder.html", null ],
        [ "HoloGen.Options.Output.Error.ErrorFolder", "class_holo_gen_1_1_options_1_1_output_1_1_error_1_1_error_folder.html", null ],
        [ "HoloGen.Options.Output.Reference.ReferenceFolder", "class_holo_gen_1_1_options_1_1_output_1_1_reference_1_1_reference_folder.html", null ],
        [ "HoloGen.Options.Output.Run.RunFolder", "class_holo_gen_1_1_options_1_1_output_1_1_run_1_1_run_folder.html", null ],
        [ "HoloGen.Options.Projector.Hologram.HologramFolder", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_hologram_folder.html", null ],
        [ "HoloGen.Options.Projector.Illumination.IlluminationFolder", "class_holo_gen_1_1_options_1_1_projector_1_1_illumination_1_1_illumination_folder.html", null ],
        [ "HoloGen.Options.Target.Region.RegionFolder", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_region_folder.html", null ],
        [ "HoloGen.Options.Target.Target.TargetFolder", "class_holo_gen_1_1_options_1_1_target_1_1_target_1_1_target_folder.html", null ],
        [ "HoloGen.Process.Options.ProcessViewFolder", "class_holo_gen_1_1_process_1_1_options_1_1_process_view_folder.html", null ],
        [ "HoloGen.Settings.General.File.FileSettingsFolder", "class_holo_gen_1_1_settings_1_1_general_1_1_file_1_1_file_settings_folder.html", null ],
        [ "HoloGen.Settings.General.File.WarningSettingsFolder", "class_holo_gen_1_1_settings_1_1_general_1_1_file_1_1_warning_settings_folder.html", null ]
      ] ],
      [ "HoloGen.Hierarchy.Hierarchy.OptionCollection", "class_holo_gen_1_1_hierarchy_1_1_hierarchy_1_1_option_collection.html", [
        [ "HoloGen.Hierarchy.OptionTypes.Possibility", "class_holo_gen_1_1_hierarchy_1_1_option_types_1_1_possibility.html", [
          [ "HoloGen.Image.Options.ColorSchemePossibility", "class_holo_gen_1_1_image_1_1_options_1_1_color_scheme_possibility.html", null ],
          [ "HoloGen.Image.Options.DimensionPossibility", "class_holo_gen_1_1_image_1_1_options_1_1_dimension_possibility.html", [
            [ "HoloGen.Image.Options.Dimension2D", "class_holo_gen_1_1_image_1_1_options_1_1_dimension2_d.html", null ],
            [ "HoloGen.Image.Options.Dimension3D", "class_holo_gen_1_1_image_1_1_options_1_1_dimension3_d.html", null ]
          ] ],
          [ "HoloGen.Image.Options.ImageViewPossibility", "class_holo_gen_1_1_image_1_1_options_1_1_image_view_possibility.html", [
            [ "HoloGen.Image.Options.ImageViewImaginary", "class_holo_gen_1_1_image_1_1_options_1_1_image_view_imaginary.html", null ],
            [ "HoloGen.Image.Options.ImageViewMagnitude", "class_holo_gen_1_1_image_1_1_options_1_1_image_view_magnitude.html", null ],
            [ "HoloGen.Image.Options.ImageViewPhase", "class_holo_gen_1_1_image_1_1_options_1_1_image_view_phase.html", null ],
            [ "HoloGen.Image.Options.ImageViewReal", "class_holo_gen_1_1_image_1_1_options_1_1_image_view_real.html", null ]
          ] ],
          [ "HoloGen.Image.Options.ScaleLocationPossibility", "class_holo_gen_1_1_image_1_1_options_1_1_scale_location_possibility.html", [
            [ "HoloGen.Image.Options.ScaleLocationBottom", "class_holo_gen_1_1_image_1_1_options_1_1_scale_location_bottom.html", null ],
            [ "HoloGen.Image.Options.ScaleLocationLeft", "class_holo_gen_1_1_image_1_1_options_1_1_scale_location_left.html", null ],
            [ "HoloGen.Image.Options.ScaleLocationNone", "class_holo_gen_1_1_image_1_1_options_1_1_scale_location_none.html", null ],
            [ "HoloGen.Image.Options.ScaleLocationRight", "class_holo_gen_1_1_image_1_1_options_1_1_scale_location_right.html", null ],
            [ "HoloGen.Image.Options.ScaleLocationTop", "class_holo_gen_1_1_image_1_1_options_1_1_scale_location_top.html", null ]
          ] ],
          [ "HoloGen.Image.Options.TransformTypePossibility", "class_holo_gen_1_1_image_1_1_options_1_1_transform_type_possibility.html", [
            [ "HoloGen.Image.Options.TransformTypeFFT", "class_holo_gen_1_1_image_1_1_options_1_1_transform_type_f_f_t.html", null ],
            [ "HoloGen.Image.Options.TransformTypeIFFT", "class_holo_gen_1_1_image_1_1_options_1_1_transform_type_i_f_f_t.html", null ],
            [ "HoloGen.Image.Options.TransformTypeNone", "class_holo_gen_1_1_image_1_1_options_1_1_transform_type_none.html", null ]
          ] ],
          [ "HoloGen.Mask.Options.MouseType.MousePossibility", "class_holo_gen_1_1_mask_1_1_options_1_1_mouse_type_1_1_mouse_possibility.html", [
            [ "HoloGen.Mask.Options.MouseType.MouseDraw", "class_holo_gen_1_1_mask_1_1_options_1_1_mouse_type_1_1_mouse_draw.html", null ],
            [ "HoloGen.Mask.Options.MouseType.MousePoint", "class_holo_gen_1_1_mask_1_1_options_1_1_mouse_type_1_1_mouse_point.html", null ]
          ] ],
          [ "HoloGen.Mask.Options.ShapeType.ShapePossibility", "class_holo_gen_1_1_mask_1_1_options_1_1_shape_type_1_1_shape_possibility.html", [
            [ "HoloGen.Mask.Options.ShapeType.ShapeEllipse", "class_holo_gen_1_1_mask_1_1_options_1_1_shape_type_1_1_shape_ellipse.html", null ],
            [ "HoloGen.Mask.Options.ShapeType.ShapePoint", "class_holo_gen_1_1_mask_1_1_options_1_1_shape_type_1_1_shape_point.html", null ],
            [ "HoloGen.Mask.Options.ShapeType.ShapePolygon", "class_holo_gen_1_1_mask_1_1_options_1_1_shape_type_1_1_shape_polygon.html", null ],
            [ "HoloGen.Mask.Options.ShapeType.ShapeRectangle", "class_holo_gen_1_1_mask_1_1_options_1_1_shape_type_1_1_shape_rectangle.html", null ]
          ] ],
          [ "HoloGen.Options.Algorithm.Algorithms.Type.AlgorithmPossibility", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_algorithm_possibility.html", [
            [ "HoloGen.Options.Algorithm.Algorithms.Type.CommonAlgorithm", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_common_algorithm.html", null ],
            [ "HoloGen.Options.Algorithm.Algorithms.Type.DSAlgorithm", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_d_s_algorithm.html", null ],
            [ "HoloGen.Options.Algorithm.Algorithms.Type.GSAlgorithm", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_g_s_algorithm.html", null ],
            [ "HoloGen.Options.Algorithm.Algorithms.Type.OSPRAlgorithm", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_o_s_p_r_algorithm.html", null ],
            [ "HoloGen.Options.Algorithm.Algorithms.Type.SAAlgorithm", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_s_a_algorithm.html", null ]
          ] ],
          [ "HoloGen.Options.Algorithm.Algorithms.Variants.VariantPossibility", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_variants_1_1_variant_possibility.html", [
            [ "HoloGen.Options.Algorithm.Algorithms.Type.Variants.Common.CommonVariantPossibility", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_variants_1_1_common_1_1_common_variant_possibility.html", [
              [ "HoloGen.Options.Algorithm.Algorithms.Type.Variants.Common.FFTFwdCommonVariant", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_variants_1_1_common_1_1_f_f_t_fwd_common_variant.html", null ],
              [ "HoloGen.Options.Algorithm.Algorithms.Type.Variants.Common.FFTInvCommonVariant", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_variants_1_1_common_1_1_f_f_t_inv_common_variant.html", null ]
            ] ],
            [ "HoloGen.Options.Algorithm.Algorithms.Type.Variants.DS.DSVariantPossibility", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_variants_1_1_d_s_1_1_d_s_variant_possibility.html", [
              [ "HoloGen.Options.Algorithm.Algorithms.Type.Variants.DS.StandardDSVariant", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_variants_1_1_d_s_1_1_standard_d_s_variant.html", null ]
            ] ],
            [ "HoloGen.Options.Algorithm.Algorithms.Type.Variants.GS.GSVariantPossibility", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_variants_1_1_g_s_1_1_g_s_variant_possibility.html", [
              [ "HoloGen.Options.Algorithm.Algorithms.Type.Variants.GS.StandardGSVariant", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_variants_1_1_g_s_1_1_standard_g_s_variant.html", null ]
            ] ],
            [ "HoloGen.Options.Algorithm.Algorithms.Type.Variants.OSPR.OSPRVariantPossibility", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_variants_1_1_o_s_p_r_1_1_o_s_p_r_variant_possibility.html", [
              [ "HoloGen.Options.Algorithm.Algorithms.Type.Variants.OSPR.AdaptiveOSPRVariant", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_variants_1_1_o_s_p_r_1_1_adaptive_o_s_p_r_variant.html", null ],
              [ "HoloGen.Options.Algorithm.Algorithms.Type.Variants.OSPR.StandardOSPRVariant", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_variants_1_1_o_s_p_r_1_1_standard_o_s_p_r_variant.html", null ]
            ] ],
            [ "HoloGen.Options.Algorithm.Algorithms.Type.Variants.SA.SAVariantPossibility", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_variants_1_1_s_a_1_1_s_a_variant_possibility.html", [
              [ "HoloGen.Options.Algorithm.Algorithms.Type.Variants.SA.StandardSAVariant", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_variants_1_1_s_a_1_1_standard_s_a_variant.html", null ]
            ] ]
          ] ],
          [ "HoloGen.Options.Algorithm.ErrorCalculation.Type.ErrorTypePossibility", "class_holo_gen_1_1_options_1_1_algorithm_1_1_error_calculation_1_1_type_1_1_error_type_possibility.html", [
            [ "HoloGen.Options.Algorithm.ErrorCalculation.Type.ErrorType1", "class_holo_gen_1_1_options_1_1_algorithm_1_1_error_calculation_1_1_type_1_1_error_type1.html", null ],
            [ "HoloGen.Options.Algorithm.ErrorCalculation.Type.ErrorType2", "class_holo_gen_1_1_options_1_1_algorithm_1_1_error_calculation_1_1_type_1_1_error_type2.html", null ],
            [ "HoloGen.Options.Algorithm.ErrorCalculation.Type.ErrorType3", "class_holo_gen_1_1_options_1_1_algorithm_1_1_error_calculation_1_1_type_1_1_error_type3.html", null ]
          ] ],
          [ "HoloGen.Options.Algorithm.Target.Type.TargetPossibility", "class_holo_gen_1_1_options_1_1_algorithm_1_1_target_1_1_type_1_1_target_possibility.html", [
            [ "HoloGen.Options.Algorithm.Target.Type.TargetBestEfficiency", "class_holo_gen_1_1_options_1_1_algorithm_1_1_target_1_1_type_1_1_target_best_efficiency.html", null ],
            [ "HoloGen.Options.Algorithm.Target.Type.TargetBestError", "class_holo_gen_1_1_options_1_1_algorithm_1_1_target_1_1_type_1_1_target_best_error.html", null ]
          ] ],
          [ "HoloGen.Options.Algorithm.Termination.Type.TerminationPossibility", "class_holo_gen_1_1_options_1_1_algorithm_1_1_termination_1_1_type_1_1_termination_possibility.html", [
            [ "HoloGen.Options.Algorithm.Termination.Type.TerminationError", "class_holo_gen_1_1_options_1_1_algorithm_1_1_termination_1_1_type_1_1_termination_error.html", null ],
            [ "HoloGen.Options.Algorithm.Termination.Type.TerminationFirst", "class_holo_gen_1_1_options_1_1_algorithm_1_1_termination_1_1_type_1_1_termination_first.html", null ],
            [ "HoloGen.Options.Algorithm.Termination.Type.TerminationIterations", "class_holo_gen_1_1_options_1_1_algorithm_1_1_termination_1_1_type_1_1_termination_iterations.html", null ]
          ] ],
          [ "HoloGen.Options.Hardware.Hardware.Type.HardwarePossibility", "class_holo_gen_1_1_options_1_1_hardware_1_1_hardware_1_1_type_1_1_hardware_possibility.html", [
            [ "HoloGen.Options.Hardware.Hardware.Type.HardwareCPU", "class_holo_gen_1_1_options_1_1_hardware_1_1_hardware_1_1_type_1_1_hardware_c_p_u.html", null ],
            [ "HoloGen.Options.Hardware.Hardware.Type.HardwareGPU", "class_holo_gen_1_1_options_1_1_hardware_1_1_hardware_1_1_type_1_1_hardware_g_p_u.html", null ]
          ] ],
          [ "HoloGen.Options.Projector.Hologram.SeedPossibility", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_seed_possibility.html", [
            [ "HoloGen.Options.Projector.Hologram.BackProjectSeed", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_back_project_seed.html", null ],
            [ "HoloGen.Options.Projector.Hologram.FromFileSeed", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_from_file_seed.html", null ],
            [ "HoloGen.Options.Projector.Hologram.RandomSeed", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_random_seed.html", null ]
          ] ],
          [ "HoloGen.Options.Projector.Hologram.SLMPossibility", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_s_l_m_possibility.html", [
            [ "HoloGen.Options.Projector.Hologram.BinaryAmpSLM", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_binary_amp_s_l_m.html", null ],
            [ "HoloGen.Options.Projector.Hologram.BinaryPhaseSLM", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_binary_phase_s_l_m.html", null ],
            [ "HoloGen.Options.Projector.Hologram.MultiAmpSLM", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_multi_amp_s_l_m.html", null ],
            [ "HoloGen.Options.Projector.Hologram.MultiPhaseSLM", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_multi_phase_s_l_m.html", null ]
          ] ],
          [ "HoloGen.Options.Projector.Illumination.Type.IlluminationPossibility", "class_holo_gen_1_1_options_1_1_projector_1_1_illumination_1_1_type_1_1_illumination_possibility.html", [
            [ "HoloGen.Options.Projector.Illumination.Type.IlluminationFromFile", "class_holo_gen_1_1_options_1_1_projector_1_1_illumination_1_1_type_1_1_illumination_from_file.html", null ],
            [ "HoloGen.Options.Projector.Illumination.Type.IlluminationGaussian", "class_holo_gen_1_1_options_1_1_projector_1_1_illumination_1_1_type_1_1_illumination_gaussian.html", null ],
            [ "HoloGen.Options.Projector.Illumination.Type.IlluminationPlanar", "class_holo_gen_1_1_options_1_1_projector_1_1_illumination_1_1_type_1_1_illumination_planar.html", null ]
          ] ],
          [ "HoloGen.Options.Target.Region.Type.RegionPossibility", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_type_1_1_region_possibility.html", [
            [ "HoloGen.Options.Target.Region.Type.RegionFromFile", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_type_1_1_region_from_file.html", null ],
            [ "HoloGen.Options.Target.Region.Type.RegionIntensity", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_type_1_1_region_intensity.html", null ],
            [ "HoloGen.Options.Target.Region.Type.RegionNone", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_type_1_1_region_none.html", null ]
          ] ],
          [ "HoloGen.Options.Target.Region.Type.SymmetryPossibility", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_type_1_1_symmetry_possibility.html", [
            [ "HoloGen.Options.Target.Region.Type.SymmetryHorizontal", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_type_1_1_symmetry_horizontal.html", null ],
            [ "HoloGen.Options.Target.Region.Type.SymmetryNone", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_type_1_1_symmetry_none.html", null ],
            [ "HoloGen.Options.Target.Region.Type.SymmetryVertical", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_type_1_1_symmetry_vertical.html", null ]
          ] ],
          [ "HoloGen.Options.Target.Region.Type.VariantPossibility", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_type_1_1_variant_possibility.html", [
            [ "HoloGen.Options.Target.Region.Type.VariantPixel", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_type_1_1_variant_pixel.html", null ],
            [ "HoloGen.Options.Target.Region.Type.VariantRound", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_type_1_1_variant_round.html", null ],
            [ "HoloGen.Options.Target.Region.Type.VariantSquare", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_type_1_1_variant_square.html", null ]
          ] ],
          [ "HoloGen.Process.Options.ProcessViewPossibility", "class_holo_gen_1_1_process_1_1_options_1_1_process_view_possibility.html", [
            [ "HoloGen.Process.Options.ProcessViewEfficiency", "class_holo_gen_1_1_process_1_1_options_1_1_process_view_efficiency.html", null ],
            [ "HoloGen.Process.Options.ProcessViewError", "class_holo_gen_1_1_process_1_1_options_1_1_process_view_error.html", null ]
          ] ]
        ] ]
      ] ]
    ] ],
    [ "HoloGen.Hierarchy.Hierarchy.HierarchyVersion", "class_holo_gen_1_1_hierarchy_1_1_hierarchy_1_1_hierarchy_version.html", [
      [ "HoloGen.Image.ComplexImageVersion", "class_holo_gen_1_1_image_1_1_complex_image_version.html", null ],
      [ "HoloGen.Options.OptionsRootVersion", "class_holo_gen_1_1_options_1_1_options_root_version.html", null ],
      [ "HoloGen.Settings.ApplicationSettingsVersion", "class_holo_gen_1_1_settings_1_1_application_settings_version.html", null ]
    ] ],
    [ "HoloGen.Hierarchy.Hierarchy.ICanEnable", "interface_holo_gen_1_1_hierarchy_1_1_hierarchy_1_1_i_can_enable.html", [
      [ "HoloGen.Hierarchy.Hierarchy.HierarchyFolder", "class_holo_gen_1_1_hierarchy_1_1_hierarchy_1_1_hierarchy_folder.html", null ],
      [ "HoloGen.Hierarchy.Hierarchy.HierarchyRoot", "class_holo_gen_1_1_hierarchy_1_1_hierarchy_1_1_hierarchy_root.html", null ],
      [ "HoloGen.Hierarchy.Hierarchy.ICanRecursivelyEnable", "interface_holo_gen_1_1_hierarchy_1_1_hierarchy_1_1_i_can_recursively_enable.html", [
        [ "HoloGen.Hierarchy.Hierarchy.IHierarchyElement", "interface_holo_gen_1_1_hierarchy_1_1_hierarchy_1_1_i_hierarchy_element.html", [
          [ "HoloGen.Hierarchy.Hierarchy.HierarchyElement< T >", "class_holo_gen_1_1_hierarchy_1_1_hierarchy_1_1_hierarchy_element.html", null ]
        ] ]
      ] ],
      [ "HoloGen.Hierarchy.Hierarchy.IHierarchyElement", "interface_holo_gen_1_1_hierarchy_1_1_hierarchy_1_1_i_hierarchy_element.html", null ],
      [ "HoloGen.Hierarchy.OptionTypes.IOption", "interface_holo_gen_1_1_hierarchy_1_1_option_types_1_1_i_option.html", [
        [ "HoloGen.Hierarchy.OptionTypes.ILargeOption", "interface_holo_gen_1_1_hierarchy_1_1_option_types_1_1_i_large_option.html", [
          [ "HoloGen.Hierarchy.OptionTypes.LargeTextOption", "class_holo_gen_1_1_hierarchy_1_1_option_types_1_1_large_text_option.html", [
            [ "HoloGen.Options.Hardware.Details.DetailsText", "class_holo_gen_1_1_options_1_1_hardware_1_1_details_1_1_details_text.html", null ],
            [ "HoloGen.Options.Output.Error.ErrorText", "class_holo_gen_1_1_options_1_1_output_1_1_error_1_1_error_text.html", null ],
            [ "HoloGen.Options.Output.Reference.ReferenceText", "class_holo_gen_1_1_options_1_1_output_1_1_reference_1_1_reference_text.html", null ]
          ] ],
          [ "HoloGen.Hierarchy.OptionTypes.PathOption", "class_holo_gen_1_1_hierarchy_1_1_option_types_1_1_path_option.html", [
            [ "HoloGen.Options.ImagePathOption", "class_holo_gen_1_1_options_1_1_image_path_option.html", [
              [ "HoloGen.Options.Projector.Hologram.SeedFile", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_seed_file.html", null ],
              [ "HoloGen.Options.Projector.Illumination.IlluminationFile", "class_holo_gen_1_1_options_1_1_projector_1_1_illumination_1_1_illumination_file.html", null ],
              [ "HoloGen.Options.Target.Region.RegionFile", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_region_file.html", null ],
              [ "HoloGen.Options.Target.Target.TargetFile", "class_holo_gen_1_1_options_1_1_target_1_1_target_1_1_target_file.html", null ]
            ] ],
            [ "HoloGen.Options.Output.Run.RunFile", "class_holo_gen_1_1_options_1_1_output_1_1_run_1_1_run_file.html", null ]
          ] ]
        ] ],
        [ "HoloGen.Hierarchy.OptionTypes.Option< T >", "class_holo_gen_1_1_hierarchy_1_1_option_types_1_1_option.html", [
          [ "HoloGen.Hierarchy.OptionTypes.NumericOption< T >", "class_holo_gen_1_1_hierarchy_1_1_option_types_1_1_numeric_option.html", null ],
          [ "HoloGen.Hierarchy.OptionTypes.SelectOption< T >", "class_holo_gen_1_1_hierarchy_1_1_option_types_1_1_select_option.html", null ]
        ] ],
        [ "HoloGen.Hierarchy.OptionTypes.SelectOption< T >", "class_holo_gen_1_1_hierarchy_1_1_option_types_1_1_select_option.html", null ]
      ] ]
    ] ],
    [ "HoloGen.Hierarchy.Hierarchy.ICanError", "interface_holo_gen_1_1_hierarchy_1_1_hierarchy_1_1_i_can_error.html", [
      [ "HoloGen.Hierarchy.Hierarchy.IHierarchyElement", "interface_holo_gen_1_1_hierarchy_1_1_hierarchy_1_1_i_hierarchy_element.html", null ],
      [ "HoloGen.Hierarchy.OptionTypes.IOption", "interface_holo_gen_1_1_hierarchy_1_1_option_types_1_1_i_option.html", null ]
    ] ],
    [ "HoloGen.Hierarchy.Hierarchy.ICanReset", "interface_holo_gen_1_1_hierarchy_1_1_hierarchy_1_1_i_can_reset.html", [
      [ "HoloGen.Hierarchy.Hierarchy.IHierarchyElement", "interface_holo_gen_1_1_hierarchy_1_1_hierarchy_1_1_i_hierarchy_element.html", null ],
      [ "HoloGen.Hierarchy.OptionTypes.IOption", "interface_holo_gen_1_1_hierarchy_1_1_option_types_1_1_i_option.html", null ]
    ] ],
    [ "ICommand", null, [
      [ "HoloGen.UI.Commands.ConnectedCommand", "class_holo_gen_1_1_u_i_1_1_commands_1_1_connected_command.html", [
        [ "HoloGen.UI.Commands.CloseCurrentTabCommand", "class_holo_gen_1_1_u_i_1_1_commands_1_1_close_current_tab_command.html", null ],
        [ "HoloGen.UI.Commands.ImportCommand", "class_holo_gen_1_1_u_i_1_1_commands_1_1_import_command.html", [
          [ "HoloGen.UI.Commands.ExportImageFileCommand", "class_holo_gen_1_1_u_i_1_1_commands_1_1_export_image_file_command.html", null ],
          [ "HoloGen.UI.Commands.ExportMatFileCommand", "class_holo_gen_1_1_u_i_1_1_commands_1_1_export_mat_file_command.html", null ],
          [ "HoloGen.UI.Commands.ImportImageFileCommand", "class_holo_gen_1_1_u_i_1_1_commands_1_1_import_image_file_command.html", null ],
          [ "HoloGen.UI.Commands.ImportMatFileCommand", "class_holo_gen_1_1_u_i_1_1_commands_1_1_import_mat_file_command.html", null ]
        ] ],
        [ "HoloGen.UI.Commands.NewBatchFile", "class_holo_gen_1_1_u_i_1_1_commands_1_1_new_batch_file.html", null ],
        [ "HoloGen.UI.Commands.NewSetupFile", "class_holo_gen_1_1_u_i_1_1_commands_1_1_new_setup_file.html", null ],
        [ "HoloGen.UI.Commands.OpenFileCommand", "class_holo_gen_1_1_u_i_1_1_commands_1_1_open_file_command.html", null ],
        [ "HoloGen.UI.Commands.SaveAsFileCommand", "class_holo_gen_1_1_u_i_1_1_commands_1_1_save_as_file_command.html", null ],
        [ "HoloGen.UI.Commands.SaveFileCommand", "class_holo_gen_1_1_u_i_1_1_commands_1_1_save_file_command.html", null ],
        [ "HoloGen.UI.Commands.ShowAboutCommand", "class_holo_gen_1_1_u_i_1_1_commands_1_1_show_about_command.html", null ],
        [ "HoloGen.UI.Commands.ShowHelpCommand", "class_holo_gen_1_1_u_i_1_1_commands_1_1_show_help_command.html", null ]
      ] ],
      [ "HoloGen.UI.Utils.Commands.RelayCommand", "class_holo_gen_1_1_u_i_1_1_utils_1_1_commands_1_1_relay_command.html", [
        [ "HoloGen.UI.Utils.Commands.RelayAsyncCommand", "class_holo_gen_1_1_u_i_1_1_utils_1_1_commands_1_1_relay_async_command.html", null ]
      ] ],
      [ "HoloGen.UI.Utils.MVVM.DelegateCommand", "class_holo_gen_1_1_u_i_1_1_utils_1_1_m_v_v_m_1_1_delegate_command.html", null ],
      [ "HoloGen.UI.Utils.SimpleCommand", "class_holo_gen_1_1_u_i_1_1_utils_1_1_simple_command.html", null ]
    ] ],
    [ "IDataErrorInfo", null, [
      [ "HoloGen.UI.OptionEditors.DoubleEditor", "class_holo_gen_1_1_u_i_1_1_option_editors_1_1_double_editor.html", null ],
      [ "HoloGen.UI.OptionEditors.IntegerEditor", "class_holo_gen_1_1_u_i_1_1_option_editors_1_1_integer_editor.html", null ],
      [ "HoloGen.UI.OptionEditors.PathEditor", "class_holo_gen_1_1_u_i_1_1_option_editors_1_1_path_editor.html", null ]
    ] ],
    [ "IDisposable", null, [
      [ "HoloGen.UI.Utils.WaitCursor", "class_holo_gen_1_1_u_i_1_1_utils_1_1_wait_cursor.html", null ]
    ] ],
    [ "HoloGen.Hierarchy.Hierarchy.IHasName", "interface_holo_gen_1_1_hierarchy_1_1_hierarchy_1_1_i_has_name.html", [
      [ "HoloGen.Hierarchy.Hierarchy.IHierarchyElement", "interface_holo_gen_1_1_hierarchy_1_1_hierarchy_1_1_i_hierarchy_element.html", null ],
      [ "HoloGen.Hierarchy.OptionTypes.IOption", "interface_holo_gen_1_1_hierarchy_1_1_option_types_1_1_i_option.html", null ]
    ] ],
    [ "HoloGen.Hierarchy.Hierarchy.IHasToolTip", "interface_holo_gen_1_1_hierarchy_1_1_hierarchy_1_1_i_has_tool_tip.html", [
      [ "HoloGen.Hierarchy.Hierarchy.IHierarchyElement", "interface_holo_gen_1_1_hierarchy_1_1_hierarchy_1_1_i_hierarchy_element.html", null ],
      [ "HoloGen.Hierarchy.OptionTypes.IOption", "interface_holo_gen_1_1_hierarchy_1_1_option_types_1_1_i_option.html", null ]
    ] ],
    [ "HoloGen.Hierarchy.Hierarchy.IHasWatermark", "interface_holo_gen_1_1_hierarchy_1_1_hierarchy_1_1_i_has_watermark.html", [
      [ "HoloGen.Hierarchy.OptionTypes.BooleanOption", "class_holo_gen_1_1_hierarchy_1_1_option_types_1_1_boolean_option.html", [
        [ "HoloGen.Hierarchy.OptionTypes.BooleanOptionWithChildren", "class_holo_gen_1_1_hierarchy_1_1_option_types_1_1_boolean_option_with_children.html", [
          [ "HoloGen.Options.Output.Run.ShowResults", "class_holo_gen_1_1_options_1_1_output_1_1_run_1_1_show_results.html", null ],
          [ "HoloGen.Options.Target.Target.ScaleTargetImage", "class_holo_gen_1_1_options_1_1_target_1_1_target_1_1_scale_target_image.html", null ]
        ] ],
        [ "HoloGen.Options.Algorithm.ErrorCalculation.ErrorNormalisation", "class_holo_gen_1_1_options_1_1_algorithm_1_1_error_calculation_1_1_error_normalisation.html", null ],
        [ "HoloGen.Options.Algorithm.Target.Target", "class_holo_gen_1_1_options_1_1_algorithm_1_1_target_1_1_target.html", null ],
        [ "HoloGen.Options.Target.Target.RandomiseTargetPhase", "class_holo_gen_1_1_options_1_1_target_1_1_target_1_1_randomise_target_phase.html", null ],
        [ "HoloGen.Options.Target.Target.TieNormalisationToZero", "class_holo_gen_1_1_options_1_1_target_1_1_target_1_1_tie_normalisation_to_zero.html", null ],
        [ "HoloGen.Settings.General.File.SaveScaleBarOnImageExport", "class_holo_gen_1_1_settings_1_1_general_1_1_file_1_1_save_scale_bar_on_image_export.html", null ],
        [ "HoloGen.Settings.General.File.ShowCloseUnsavedFileWarning", "class_holo_gen_1_1_settings_1_1_general_1_1_file_1_1_show_close_unsaved_file_warning.html", null ],
        [ "HoloGen.Settings.General.File.ShowLargeMatExportWarning", "class_holo_gen_1_1_settings_1_1_general_1_1_file_1_1_show_large_mat_export_warning.html", null ],
        [ "HoloGen.Settings.General.File.ShowLargeMatImportWarning", "class_holo_gen_1_1_settings_1_1_general_1_1_file_1_1_show_large_mat_import_warning.html", null ]
      ] ],
      [ "HoloGen.Hierarchy.OptionTypes.NumericOption< T >", "class_holo_gen_1_1_hierarchy_1_1_option_types_1_1_numeric_option.html", null ],
      [ "HoloGen.Hierarchy.OptionTypes.PathOption", "class_holo_gen_1_1_hierarchy_1_1_option_types_1_1_path_option.html", null ],
      [ "HoloGen.Hierarchy.OptionTypes.TextOption", "class_holo_gen_1_1_hierarchy_1_1_option_types_1_1_text_option.html", [
        [ "HoloGen.Hierarchy.OptionTypes.LargeTextOption", "class_holo_gen_1_1_hierarchy_1_1_option_types_1_1_large_text_option.html", null ]
      ] ]
    ] ],
    [ "IInterTabClient", null, [
      [ "HoloGen.UI.Common.InterTabClient", "class_holo_gen_1_1_u_i_1_1_common_1_1_inter_tab_client.html", null ]
    ] ],
    [ "HoloGen.Image.ImageCache", "class_holo_gen_1_1_image_1_1_image_cache.html", null ],
    [ "HoloGen.Image.ImageView", "class_holo_gen_1_1_image_1_1_image_view.html", null ],
    [ "HoloGen.Utils.InfoUtils", "class_holo_gen_1_1_utils_1_1_info_utils.html", null ],
    [ "INotifyPropertyChanged", null, [
      [ "HoloGen.Hierarchy.OptionTypes.IOption", "interface_holo_gen_1_1_hierarchy_1_1_option_types_1_1_i_option.html", null ],
      [ "HoloGen.Options.OptionsRoot", "class_holo_gen_1_1_options_1_1_options_root.html", null ],
      [ "HoloGen.Settings.ApplicationSettings", "class_holo_gen_1_1_settings_1_1_application_settings.html", null ],
      [ "HoloGen.UI.Hamburger.Views.HamburgerTabView", "class_holo_gen_1_1_u_i_1_1_hamburger_1_1_views_1_1_hamburger_tab_view.html", null ],
      [ "HoloGen.UI.Menus.IMenuWindow", "interface_holo_gen_1_1_u_i_1_1_menus_1_1_i_menu_window.html", [
        [ "HoloGen.UI.MainWindowViewModel", "class_holo_gen_1_1_u_i_1_1_main_window_view_model.html", null ]
      ] ],
      [ "HoloGen.UI.OptionEditors.DoubleEditor", "class_holo_gen_1_1_u_i_1_1_option_editors_1_1_double_editor.html", null ],
      [ "HoloGen.UI.OptionEditors.IntegerEditor", "class_holo_gen_1_1_u_i_1_1_option_editors_1_1_integer_editor.html", null ],
      [ "HoloGen.UI.OptionEditors.PathEditor", "class_holo_gen_1_1_u_i_1_1_option_editors_1_1_path_editor.html", null ],
      [ "HoloGen.UI.Utils.ITabViewModel", "interface_holo_gen_1_1_u_i_1_1_utils_1_1_i_tab_view_model.html", [
        [ "HoloGen.UI.ViewModels.AbstractTabViewModel< T >", "class_holo_gen_1_1_u_i_1_1_view_models_1_1_abstract_tab_view_model.html", null ]
      ] ],
      [ "HoloGen.UI.Utils.MVVM.BindableBase", "class_holo_gen_1_1_u_i_1_1_utils_1_1_m_v_v_m_1_1_bindable_base.html", [
        [ "HoloGen.UI.Graph.ViewModels.GraphViewModel", "class_holo_gen_1_1_u_i_1_1_graph_1_1_view_models_1_1_graph_view_model.html", null ],
        [ "HoloGen.UI.Hamburger.ViewModels.HamburgerItem< T >", "class_holo_gen_1_1_u_i_1_1_hamburger_1_1_view_models_1_1_hamburger_item.html", null ]
      ] ],
      [ "HoloGen.Utils.ObservableObject", "class_holo_gen_1_1_utils_1_1_observable_object.html", [
        [ "HoloGen.Hierarchy.Hierarchy.HierarchySaveable", "class_holo_gen_1_1_hierarchy_1_1_hierarchy_1_1_hierarchy_saveable.html", [
          [ "HoloGen.Hierarchy.Hierarchy.ReflectiveChildrenElement< T >", "class_holo_gen_1_1_hierarchy_1_1_hierarchy_1_1_reflective_children_element.html", [
            [ "HoloGen.Hierarchy.Hierarchy.HierarchyElement< T >", "class_holo_gen_1_1_hierarchy_1_1_hierarchy_1_1_hierarchy_element.html", null ],
            [ "HoloGen.Hierarchy.Hierarchy.PossibilityCollection< T >", "class_holo_gen_1_1_hierarchy_1_1_hierarchy_1_1_possibility_collection.html", null ]
          ] ],
          [ "HoloGen.Image.ComplexImage", "class_holo_gen_1_1_image_1_1_complex_image.html", null ]
        ] ],
        [ "HoloGen.Hierarchy.OptionTypes.Option< T >", "class_holo_gen_1_1_hierarchy_1_1_option_types_1_1_option.html", null ],
        [ "HoloGen.UI.Commands.ConnectedCommand", "class_holo_gen_1_1_u_i_1_1_commands_1_1_connected_command.html", null ],
        [ "HoloGen.UI.Hamburger.ViewModels.HamburgerTabViewModel", "class_holo_gen_1_1_u_i_1_1_hamburger_1_1_view_models_1_1_hamburger_tab_view_model.html", null ],
        [ "HoloGen.UI.Image.Viewer.ImageViewModel", "class_holo_gen_1_1_u_i_1_1_image_1_1_viewer_1_1_image_view_model.html", [
          [ "HoloGen.UI.X3.Viewer.X3PlotterViewModel", "class_holo_gen_1_1_u_i_1_1_x3_1_1_viewer_1_1_x3_plotter_view_model.html", [
            [ "HoloGen.UI.Mask.MaskViewModel", "class_holo_gen_1_1_u_i_1_1_mask_1_1_mask_view_model.html", [
              [ "HoloGen.UI.ViewModels.HologramViewModel", "class_holo_gen_1_1_u_i_1_1_view_models_1_1_hologram_view_model.html", null ]
            ] ]
          ] ]
        ] ],
        [ "HoloGen.UI.MainWindowViewModel", "class_holo_gen_1_1_u_i_1_1_main_window_view_model.html", null ],
        [ "HoloGen.UI.Menus.Theme.AccentColorMenuData", "class_holo_gen_1_1_u_i_1_1_menus_1_1_theme_1_1_accent_color_menu_data.html", [
          [ "HoloGen.UI.Menus.Theme.AppThemeMenuData", "class_holo_gen_1_1_u_i_1_1_menus_1_1_theme_1_1_app_theme_menu_data.html", null ]
        ] ],
        [ "HoloGen.UI.Menus.ViewModels.AbstractMenuViewModel", "class_holo_gen_1_1_u_i_1_1_menus_1_1_view_models_1_1_abstract_menu_view_model.html", [
          [ "HoloGen.UI.Menus.ViewModels.AccentMenuViewModel", "class_holo_gen_1_1_u_i_1_1_menus_1_1_view_models_1_1_accent_menu_view_model.html", null ],
          [ "HoloGen.UI.Menus.ViewModels.FileMenuViewModel", "class_holo_gen_1_1_u_i_1_1_menus_1_1_view_models_1_1_file_menu_view_model.html", null ],
          [ "HoloGen.UI.Menus.ViewModels.HelpMenuViewModel", "class_holo_gen_1_1_u_i_1_1_menus_1_1_view_models_1_1_help_menu_view_model.html", null ],
          [ "HoloGen.UI.Menus.ViewModels.MenuViewModel", "class_holo_gen_1_1_u_i_1_1_menus_1_1_view_models_1_1_menu_view_model.html", null ],
          [ "HoloGen.UI.Menus.ViewModels.SettingsMenuViewModel", "class_holo_gen_1_1_u_i_1_1_menus_1_1_view_models_1_1_settings_menu_view_model.html", null ],
          [ "HoloGen.UI.Menus.ViewModels.ThemeMenuViewModel", "class_holo_gen_1_1_u_i_1_1_menus_1_1_view_models_1_1_theme_menu_view_model.html", null ]
        ] ],
        [ "HoloGen.UI.Process.Monitor.ViewModels.ProcessViewModel", "class_holo_gen_1_1_u_i_1_1_process_1_1_monitor_1_1_view_models_1_1_process_view_model.html", null ],
        [ "HoloGen.UI.ViewModels.AbstractTabViewModel< T >", "class_holo_gen_1_1_u_i_1_1_view_models_1_1_abstract_tab_view_model.html", null ],
        [ "HoloGen.UI.ViewModels.BatchViewModel", "class_holo_gen_1_1_u_i_1_1_view_models_1_1_batch_view_model.html", null ],
        [ "HoloGen.UI.ViewModels.PdfViewModel", "class_holo_gen_1_1_u_i_1_1_view_models_1_1_pdf_view_model.html", null ],
        [ "HoloGen.UI.ViewModels.SetupViewModel", "class_holo_gen_1_1_u_i_1_1_view_models_1_1_setup_view_model.html", null ],
        [ "HoloGen.Utils.StatusMessage", "class_holo_gen_1_1_utils_1_1_status_message.html", null ]
      ] ]
    ] ],
    [ "HoloGen.Hierarchy.OptionTypes.ISelectOption", "interface_holo_gen_1_1_hierarchy_1_1_option_types_1_1_i_select_option.html", [
      [ "HoloGen.Hierarchy.OptionTypes.SelectOption< T >", "class_holo_gen_1_1_hierarchy_1_1_option_types_1_1_select_option.html", null ]
    ] ],
    [ "HoloGen.UI.Utils.ITabHandler", "interface_holo_gen_1_1_u_i_1_1_utils_1_1_i_tab_handler.html", [
      [ "HoloGen.UI.MainWindowViewModel", "class_holo_gen_1_1_u_i_1_1_main_window_view_model.html", null ]
    ] ],
    [ "IValueConverter", null, [
      [ "HoloGen.UI.Graph.ViewModels.ZoomingModeCoverter", "class_holo_gen_1_1_u_i_1_1_graph_1_1_view_models_1_1_zooming_mode_coverter.html", null ]
    ] ],
    [ "HoloGen.Serial.JsonDeserialiser< ApplicationSettings, ApplicationSettingsVersion >", "class_holo_gen_1_1_serial_1_1_json_deserialiser.html", [
      [ "HoloGen.Serial.JsonSettingsDeserialiser", "class_holo_gen_1_1_serial_1_1_json_settings_deserialiser.html", null ]
    ] ],
    [ "HoloGen.Serial.JsonDeserialiser< ComplexImage, ComplexImageVersion >", "class_holo_gen_1_1_serial_1_1_json_deserialiser.html", [
      [ "HoloGen.Serial.JsonImageDeserialiser", "class_holo_gen_1_1_serial_1_1_json_image_deserialiser.html", null ]
    ] ],
    [ "HoloGen.Serial.JsonDeserialiser< OptionsRoot, OptionsRootVersion >", "class_holo_gen_1_1_serial_1_1_json_deserialiser.html", [
      [ "HoloGen.Serial.JsonOptionsDeserialiser", "class_holo_gen_1_1_serial_1_1_json_options_deserialiser.html", null ]
    ] ],
    [ "HoloGen.IO.JsonFileLoader< ApplicationSettings, ApplicationSettingsVersion, JsonSettingsDeserialiser >", "class_holo_gen_1_1_i_o_1_1_json_file_loader.html", [
      [ "HoloGen.IO.JsonSettingsFileLoader", "class_holo_gen_1_1_i_o_1_1_json_settings_file_loader.html", null ]
    ] ],
    [ "HoloGen.IO.JsonFileLoader< ComplexImage, ComplexImageVersion, JsonImageDeserialiser >", "class_holo_gen_1_1_i_o_1_1_json_file_loader.html", [
      [ "HoloGen.IO.JsonImageFileLoader", "class_holo_gen_1_1_i_o_1_1_json_image_file_loader.html", null ]
    ] ],
    [ "HoloGen.IO.JsonFileLoader< OptionsRoot, OptionsRootVersion, JsonOptionsDeserialiser >", "class_holo_gen_1_1_i_o_1_1_json_file_loader.html", [
      [ "HoloGen.IO.JsonOptionsFileLoader", "class_holo_gen_1_1_i_o_1_1_json_options_file_loader.html", null ]
    ] ],
    [ "HoloGen.IO.JsonFileSaver< ApplicationSettings, ApplicationSettingsVersion, JsonSettingsSerialiser >", "class_holo_gen_1_1_i_o_1_1_json_file_saver.html", [
      [ "HoloGen.IO.JsonSettingsFileSaver", "class_holo_gen_1_1_i_o_1_1_json_settings_file_saver.html", null ]
    ] ],
    [ "HoloGen.IO.JsonFileSaver< ComplexImage, ComplexImageVersion, JsonImageSerialiser >", "class_holo_gen_1_1_i_o_1_1_json_file_saver.html", [
      [ "HoloGen.IO.JsonImageFileSaver", "class_holo_gen_1_1_i_o_1_1_json_image_file_saver.html", null ]
    ] ],
    [ "HoloGen.IO.JsonFileSaver< OptionsRoot, OptionsRootVersion, JsonOptionsSerialiser >", "class_holo_gen_1_1_i_o_1_1_json_file_saver.html", [
      [ "HoloGen.IO.JsonOptionsFileSaver", "class_holo_gen_1_1_i_o_1_1_json_options_file_saver.html", null ]
    ] ],
    [ "HoloGen.Serial.JsonSerialiser< ApplicationSettings, ApplicationSettingsVersion >", "class_holo_gen_1_1_serial_1_1_json_serialiser.html", [
      [ "HoloGen.Serial.JsonSettingsSerialiser", "class_holo_gen_1_1_serial_1_1_json_settings_serialiser.html", null ]
    ] ],
    [ "HoloGen.Serial.JsonSerialiser< ComplexImage, ComplexImageVersion >", "class_holo_gen_1_1_serial_1_1_json_serialiser.html", [
      [ "HoloGen.Serial.JsonImageSerialiser", "class_holo_gen_1_1_serial_1_1_json_image_serialiser.html", null ]
    ] ],
    [ "HoloGen.Serial.JsonSerialiser< OptionsRoot, OptionsRootVersion >", "class_holo_gen_1_1_serial_1_1_json_serialiser.html", [
      [ "HoloGen.Serial.JsonOptionsSerialiser", "class_holo_gen_1_1_serial_1_1_json_options_serialiser.html", null ]
    ] ],
    [ "HoloGen.Utils.LogFileHandle", "class_holo_gen_1_1_utils_1_1_log_file_handle.html", null ],
    [ "MarkupConverter", null, [
      [ "HoloGen.UI.Hamburger.NullToUnsetValueConverter", "class_holo_gen_1_1_u_i_1_1_hamburger_1_1_null_to_unset_value_converter.html", null ]
    ] ],
    [ "HoloGen.IO.Convertors.Export.MatExporter", "class_holo_gen_1_1_i_o_1_1_convertors_1_1_export_1_1_mat_exporter.html", null ],
    [ "HoloGen.IO.Convertors.Import.MatImporter", "class_holo_gen_1_1_i_o_1_1_convertors_1_1_import_1_1_mat_importer.html", null ],
    [ "MetroWindow", null, [
      [ "HoloGen.UI.MainWindow", "class_holo_gen_1_1_u_i_1_1_main_window.html", null ]
    ] ],
    [ "ModelVisual3D", null, [
      [ "HoloGen.UI.X3.Viewer.SurfacePlotVisual3D", "class_holo_gen_1_1_u_i_1_1_x3_1_1_viewer_1_1_surface_plot_visual3_d.html", null ]
    ] ],
    [ "HoloGen.Hierarchy.OptionTypes.NumericOption< double >", "class_holo_gen_1_1_hierarchy_1_1_option_types_1_1_numeric_option.html", [
      [ "HoloGen.Hierarchy.OptionTypes.DoubleOption", "class_holo_gen_1_1_hierarchy_1_1_option_types_1_1_double_option.html", [
        [ "HoloGen.Options.Algorithm.Algorithms.StartingTemperature", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_starting_temperature.html", null ],
        [ "HoloGen.Options.Algorithm.Algorithms.TemperatureCoefficient", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_temperature_coefficient.html", null ],
        [ "HoloGen.Options.Algorithm.Target.EfficiencyLimit", "class_holo_gen_1_1_options_1_1_algorithm_1_1_target_1_1_efficiency_limit.html", null ],
        [ "HoloGen.Options.Algorithm.Target.ErrorLimit", "class_holo_gen_1_1_options_1_1_algorithm_1_1_target_1_1_error_limit.html", null ],
        [ "HoloGen.Options.Algorithm.Termination.ErrorValue", "class_holo_gen_1_1_options_1_1_algorithm_1_1_termination_1_1_error_value.html", null ],
        [ "HoloGen.Options.Projector.Hologram.FocalLength", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_focal_length.html", null ],
        [ "HoloGen.Options.Projector.Hologram.MaximumAngle", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_maximum_angle.html", null ],
        [ "HoloGen.Options.Projector.Hologram.MaximumLevel", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_maximum_level.html", null ],
        [ "HoloGen.Options.Projector.Hologram.MinimumAngle", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_minimum_angle.html", null ],
        [ "HoloGen.Options.Projector.Hologram.MinimumLevel", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_minimum_level.html", null ],
        [ "HoloGen.Options.Projector.Hologram.PixelPitch", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_pixel_pitch.html", null ],
        [ "HoloGen.Options.Projector.Hologram.Wavelength", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_wavelength.html", null ]
      ] ]
    ] ],
    [ "HoloGen.Hierarchy.OptionTypes.NumericOption< int >", "class_holo_gen_1_1_hierarchy_1_1_option_types_1_1_numeric_option.html", [
      [ "HoloGen.Hierarchy.OptionTypes.IntegerOption", "class_holo_gen_1_1_hierarchy_1_1_option_types_1_1_integer_option.html", [
        [ "HoloGen.Options.Algorithm.Algorithms.SubFrames", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_sub_frames.html", null ],
        [ "HoloGen.Options.Algorithm.Termination.NumIterations", "class_holo_gen_1_1_options_1_1_algorithm_1_1_termination_1_1_num_iterations.html", null ],
        [ "HoloGen.Options.Output.Run.Blocks", "class_holo_gen_1_1_options_1_1_output_1_1_run_1_1_blocks.html", null ],
        [ "HoloGen.Options.Output.Run.ResultEvery", "class_holo_gen_1_1_options_1_1_output_1_1_run_1_1_result_every.html", null ],
        [ "HoloGen.Options.Output.Run.Threads", "class_holo_gen_1_1_options_1_1_output_1_1_run_1_1_threads.html", null ],
        [ "HoloGen.Options.Projector.Hologram.Oversampling", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_oversampling.html", null ],
        [ "HoloGen.Options.Projector.Hologram.SLMLevels", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_s_l_m_levels.html", null ],
        [ "HoloGen.Options.Projector.Hologram.SLMResolutionX", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_s_l_m_resolution_x.html", null ],
        [ "HoloGen.Options.Projector.Hologram.SLMResolutionY", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_s_l_m_resolution_y.html", null ],
        [ "HoloGen.Options.Target.Region.PixelCount", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_pixel_count.html", null ]
      ] ]
    ] ],
    [ "ObservableCollection", null, [
      [ "HoloGen.Utils.ColorSchemes", "class_holo_gen_1_1_utils_1_1_color_schemes.html", null ]
    ] ],
    [ "HoloGen.Hierarchy.OptionTypes.Option< bool >", "class_holo_gen_1_1_hierarchy_1_1_option_types_1_1_option.html", [
      [ "HoloGen.Hierarchy.OptionTypes.BooleanOption", "class_holo_gen_1_1_hierarchy_1_1_option_types_1_1_boolean_option.html", null ]
    ] ],
    [ "HoloGen.Hierarchy.OptionTypes.Option< FileInfo >", "class_holo_gen_1_1_hierarchy_1_1_option_types_1_1_option.html", [
      [ "HoloGen.Hierarchy.OptionTypes.PathOption", "class_holo_gen_1_1_hierarchy_1_1_option_types_1_1_path_option.html", null ]
    ] ],
    [ "HoloGen.Hierarchy.OptionTypes.Option< string >", "class_holo_gen_1_1_hierarchy_1_1_option_types_1_1_option.html", [
      [ "HoloGen.Hierarchy.OptionTypes.TextOption", "class_holo_gen_1_1_hierarchy_1_1_option_types_1_1_text_option.html", null ]
    ] ],
    [ "HoloGen.Hierarchy.Hierarchy.PossibilityCollection< AlgorithmPossibility >", "class_holo_gen_1_1_hierarchy_1_1_hierarchy_1_1_possibility_collection.html", [
      [ "HoloGen.Options.Algorithm.Algorithms.Type.AlgorithmPossibilities", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_algorithm_possibilities.html", null ]
    ] ],
    [ "HoloGen.Hierarchy.Hierarchy.PossibilityCollection< ColorSchemePossibility >", "class_holo_gen_1_1_hierarchy_1_1_hierarchy_1_1_possibility_collection.html", [
      [ "HoloGen.Image.Options.ColorSchemePossibilities", "class_holo_gen_1_1_image_1_1_options_1_1_color_scheme_possibilities.html", null ]
    ] ],
    [ "HoloGen.Hierarchy.Hierarchy.PossibilityCollection< CommonVariantPossibility >", "class_holo_gen_1_1_hierarchy_1_1_hierarchy_1_1_possibility_collection.html", [
      [ "HoloGen.Options.Algorithm.Algorithms.Type.Variants.Common.CommonVariantPossibilities", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_variants_1_1_common_1_1_common_variant_possibilities.html", null ]
    ] ],
    [ "HoloGen.Hierarchy.Hierarchy.PossibilityCollection< DimensionPossibility >", "class_holo_gen_1_1_hierarchy_1_1_hierarchy_1_1_possibility_collection.html", [
      [ "HoloGen.Image.Options.DimensionPossibilities", "class_holo_gen_1_1_image_1_1_options_1_1_dimension_possibilities.html", null ]
    ] ],
    [ "HoloGen.Hierarchy.Hierarchy.PossibilityCollection< DSVariantPossibility >", "class_holo_gen_1_1_hierarchy_1_1_hierarchy_1_1_possibility_collection.html", [
      [ "HoloGen.Options.Algorithm.Algorithms.Type.Variants.DS.DSVariantPossibilities", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_variants_1_1_d_s_1_1_d_s_variant_possibilities.html", null ]
    ] ],
    [ "HoloGen.Hierarchy.Hierarchy.PossibilityCollection< ErrorTypePossibility >", "class_holo_gen_1_1_hierarchy_1_1_hierarchy_1_1_possibility_collection.html", [
      [ "HoloGen.Options.Algorithm.ErrorCalculation.Type.ErrorTypePossibilities", "class_holo_gen_1_1_options_1_1_algorithm_1_1_error_calculation_1_1_type_1_1_error_type_possibilities.html", null ]
    ] ],
    [ "HoloGen.Hierarchy.Hierarchy.PossibilityCollection< GSVariantPossibility >", "class_holo_gen_1_1_hierarchy_1_1_hierarchy_1_1_possibility_collection.html", [
      [ "HoloGen.Options.Algorithm.Algorithms.Type.Variants.GS.GSVariantPossibilities", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_variants_1_1_g_s_1_1_g_s_variant_possibilities.html", null ]
    ] ],
    [ "HoloGen.Hierarchy.Hierarchy.PossibilityCollection< HardwarePossibility >", "class_holo_gen_1_1_hierarchy_1_1_hierarchy_1_1_possibility_collection.html", [
      [ "HoloGen.Options.Hardware.Hardware.Type.HardwarePossibilities", "class_holo_gen_1_1_options_1_1_hardware_1_1_hardware_1_1_type_1_1_hardware_possibilities.html", null ]
    ] ],
    [ "HoloGen.Hierarchy.Hierarchy.PossibilityCollection< HoloGen.Image.Options.ColorSchemePossibility >", "class_holo_gen_1_1_hierarchy_1_1_hierarchy_1_1_possibility_collection.html", null ],
    [ "HoloGen.Hierarchy.Hierarchy.PossibilityCollection< HoloGen.Image.Options.DimensionPossibility >", "class_holo_gen_1_1_hierarchy_1_1_hierarchy_1_1_possibility_collection.html", null ],
    [ "HoloGen.Hierarchy.Hierarchy.PossibilityCollection< HoloGen.Image.Options.ImageViewPossibility >", "class_holo_gen_1_1_hierarchy_1_1_hierarchy_1_1_possibility_collection.html", null ],
    [ "HoloGen.Hierarchy.Hierarchy.PossibilityCollection< HoloGen.Image.Options.ScaleLocationPossibility >", "class_holo_gen_1_1_hierarchy_1_1_hierarchy_1_1_possibility_collection.html", null ],
    [ "HoloGen.Hierarchy.Hierarchy.PossibilityCollection< HoloGen.Image.Options.TransformTypePossibility >", "class_holo_gen_1_1_hierarchy_1_1_hierarchy_1_1_possibility_collection.html", null ],
    [ "HoloGen.Hierarchy.Hierarchy.PossibilityCollection< HoloGen.Mask.Options.MouseType.MousePossibility >", "class_holo_gen_1_1_hierarchy_1_1_hierarchy_1_1_possibility_collection.html", null ],
    [ "HoloGen.Hierarchy.Hierarchy.PossibilityCollection< HoloGen.Mask.Options.ShapeType.ShapePossibility >", "class_holo_gen_1_1_hierarchy_1_1_hierarchy_1_1_possibility_collection.html", null ],
    [ "HoloGen.Hierarchy.Hierarchy.PossibilityCollection< HoloGen.Options.Algorithm.Algorithms.Type.AlgorithmPossibility >", "class_holo_gen_1_1_hierarchy_1_1_hierarchy_1_1_possibility_collection.html", null ],
    [ "HoloGen.Hierarchy.Hierarchy.PossibilityCollection< HoloGen.Options.Algorithm.ErrorCalculation.Type.ErrorTypePossibility >", "class_holo_gen_1_1_hierarchy_1_1_hierarchy_1_1_possibility_collection.html", null ],
    [ "HoloGen.Hierarchy.Hierarchy.PossibilityCollection< HoloGen.Options.Algorithm.Target.Type.TargetPossibility >", "class_holo_gen_1_1_hierarchy_1_1_hierarchy_1_1_possibility_collection.html", null ],
    [ "HoloGen.Hierarchy.Hierarchy.PossibilityCollection< HoloGen.Options.Algorithm.Termination.Type.TerminationPossibility >", "class_holo_gen_1_1_hierarchy_1_1_hierarchy_1_1_possibility_collection.html", null ],
    [ "HoloGen.Hierarchy.Hierarchy.PossibilityCollection< HoloGen.Options.Hardware.Hardware.Type.HardwarePossibility >", "class_holo_gen_1_1_hierarchy_1_1_hierarchy_1_1_possibility_collection.html", null ],
    [ "HoloGen.Hierarchy.Hierarchy.PossibilityCollection< HoloGen.Options.Projector.Hologram.SeedPossibility >", "class_holo_gen_1_1_hierarchy_1_1_hierarchy_1_1_possibility_collection.html", null ],
    [ "HoloGen.Hierarchy.Hierarchy.PossibilityCollection< HoloGen.Options.Projector.Hologram.SLMPossibility >", "class_holo_gen_1_1_hierarchy_1_1_hierarchy_1_1_possibility_collection.html", null ],
    [ "HoloGen.Hierarchy.Hierarchy.PossibilityCollection< HoloGen.Options.Projector.Illumination.Type.IlluminationPossibility >", "class_holo_gen_1_1_hierarchy_1_1_hierarchy_1_1_possibility_collection.html", null ],
    [ "HoloGen.Hierarchy.Hierarchy.PossibilityCollection< HoloGen.Options.Target.Region.Type.RegionPossibility >", "class_holo_gen_1_1_hierarchy_1_1_hierarchy_1_1_possibility_collection.html", null ],
    [ "HoloGen.Hierarchy.Hierarchy.PossibilityCollection< HoloGen.Options.Target.Region.Type.SymmetryPossibility >", "class_holo_gen_1_1_hierarchy_1_1_hierarchy_1_1_possibility_collection.html", null ],
    [ "HoloGen.Hierarchy.Hierarchy.PossibilityCollection< HoloGen.Options.Target.Region.Type.VariantPossibility >", "class_holo_gen_1_1_hierarchy_1_1_hierarchy_1_1_possibility_collection.html", null ],
    [ "HoloGen.Hierarchy.Hierarchy.PossibilityCollection< HoloGen.Process.Options.ProcessViewPossibility >", "class_holo_gen_1_1_hierarchy_1_1_hierarchy_1_1_possibility_collection.html", null ],
    [ "HoloGen.Hierarchy.Hierarchy.PossibilityCollection< IlluminationPossibility >", "class_holo_gen_1_1_hierarchy_1_1_hierarchy_1_1_possibility_collection.html", [
      [ "HoloGen.Options.Projector.Illumination.Type.IlluminationPossibilities", "class_holo_gen_1_1_options_1_1_projector_1_1_illumination_1_1_type_1_1_illumination_possibilities.html", null ]
    ] ],
    [ "HoloGen.Hierarchy.Hierarchy.PossibilityCollection< ImageViewPossibility >", "class_holo_gen_1_1_hierarchy_1_1_hierarchy_1_1_possibility_collection.html", [
      [ "HoloGen.Image.Options.ImageViewPossibilities", "class_holo_gen_1_1_image_1_1_options_1_1_image_view_possibilities.html", null ]
    ] ],
    [ "HoloGen.Hierarchy.Hierarchy.PossibilityCollection< MousePossibility >", "class_holo_gen_1_1_hierarchy_1_1_hierarchy_1_1_possibility_collection.html", [
      [ "HoloGen.Mask.Options.MouseType.MousePossibilities", "class_holo_gen_1_1_mask_1_1_options_1_1_mouse_type_1_1_mouse_possibilities.html", null ]
    ] ],
    [ "HoloGen.Hierarchy.Hierarchy.PossibilityCollection< OSPRVariantPossibility >", "class_holo_gen_1_1_hierarchy_1_1_hierarchy_1_1_possibility_collection.html", [
      [ "HoloGen.Options.Algorithm.Algorithms.Type.Variants.OSPR.OSPRVariantPossibilities", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_variants_1_1_o_s_p_r_1_1_o_s_p_r_variant_possibilities.html", null ]
    ] ],
    [ "HoloGen.Hierarchy.Hierarchy.PossibilityCollection< ProcessViewPossibility >", "class_holo_gen_1_1_hierarchy_1_1_hierarchy_1_1_possibility_collection.html", [
      [ "HoloGen.Process.Options.ProcessViewPossibilities", "class_holo_gen_1_1_process_1_1_options_1_1_process_view_possibilities.html", null ]
    ] ],
    [ "HoloGen.Hierarchy.Hierarchy.PossibilityCollection< RegionPossibility >", "class_holo_gen_1_1_hierarchy_1_1_hierarchy_1_1_possibility_collection.html", [
      [ "HoloGen.Options.Target.Region.Type.RegionPossibilities", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_type_1_1_region_possibilities.html", null ]
    ] ],
    [ "HoloGen.Hierarchy.Hierarchy.PossibilityCollection< SAVariantPossibility >", "class_holo_gen_1_1_hierarchy_1_1_hierarchy_1_1_possibility_collection.html", [
      [ "HoloGen.Options.Algorithm.Algorithms.Type.Variants.SA.SAVariantPossibilities", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_variants_1_1_s_a_1_1_s_a_variant_possibilities.html", null ]
    ] ],
    [ "HoloGen.Hierarchy.Hierarchy.PossibilityCollection< ScaleLocationPossibility >", "class_holo_gen_1_1_hierarchy_1_1_hierarchy_1_1_possibility_collection.html", [
      [ "HoloGen.Image.Options.ScaleLocationPossibilities", "class_holo_gen_1_1_image_1_1_options_1_1_scale_location_possibilities.html", null ]
    ] ],
    [ "HoloGen.Hierarchy.Hierarchy.PossibilityCollection< SeedPossibility >", "class_holo_gen_1_1_hierarchy_1_1_hierarchy_1_1_possibility_collection.html", [
      [ "HoloGen.Options.Projector.Hologram.SeedPossibilities", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_seed_possibilities.html", null ]
    ] ],
    [ "HoloGen.Hierarchy.Hierarchy.PossibilityCollection< ShapePossibility >", "class_holo_gen_1_1_hierarchy_1_1_hierarchy_1_1_possibility_collection.html", [
      [ "HoloGen.Mask.Options.ShapeType.ShapePossibilities", "class_holo_gen_1_1_mask_1_1_options_1_1_shape_type_1_1_shape_possibilities.html", null ]
    ] ],
    [ "HoloGen.Hierarchy.Hierarchy.PossibilityCollection< SLMPossibility >", "class_holo_gen_1_1_hierarchy_1_1_hierarchy_1_1_possibility_collection.html", [
      [ "HoloGen.Options.Projector.Hologram.SLMPossibilities", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_s_l_m_possibilities.html", null ]
    ] ],
    [ "HoloGen.Hierarchy.Hierarchy.PossibilityCollection< SymmetryPossibility >", "class_holo_gen_1_1_hierarchy_1_1_hierarchy_1_1_possibility_collection.html", [
      [ "HoloGen.Options.Target.Region.Type.SymmetryPossibilities", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_type_1_1_symmetry_possibilities.html", null ]
    ] ],
    [ "HoloGen.Hierarchy.Hierarchy.PossibilityCollection< TargetPossibility >", "class_holo_gen_1_1_hierarchy_1_1_hierarchy_1_1_possibility_collection.html", [
      [ "HoloGen.Options.Algorithm.Target.Type.TargetPossibilities", "class_holo_gen_1_1_options_1_1_algorithm_1_1_target_1_1_type_1_1_target_possibilities.html", null ]
    ] ],
    [ "HoloGen.Hierarchy.Hierarchy.PossibilityCollection< TerminationPossibility >", "class_holo_gen_1_1_hierarchy_1_1_hierarchy_1_1_possibility_collection.html", [
      [ "HoloGen.Options.Algorithm.Termination.Type.TerminationPossibilities", "class_holo_gen_1_1_options_1_1_algorithm_1_1_termination_1_1_type_1_1_termination_possibilities.html", null ]
    ] ],
    [ "HoloGen.Hierarchy.Hierarchy.PossibilityCollection< TransformTypePossibility >", "class_holo_gen_1_1_hierarchy_1_1_hierarchy_1_1_possibility_collection.html", [
      [ "HoloGen.Image.Options.TransformTypePossibilities", "class_holo_gen_1_1_image_1_1_options_1_1_transform_type_possibilities.html", null ]
    ] ],
    [ "HoloGen.Hierarchy.Hierarchy.PossibilityCollection< VariantPossibility >", "class_holo_gen_1_1_hierarchy_1_1_hierarchy_1_1_possibility_collection.html", [
      [ "HoloGen.Options.Target.Region.Type.VariantPossibilities", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_type_1_1_variant_possibilities.html", null ]
    ] ],
    [ "HoloGen.Program", "class_holo_gen_1_1_program.html", null ],
    [ "HoloGen.UI.Mask.Drawer.ResourceCenter", "class_holo_gen_1_1_u_i_1_1_mask_1_1_drawer_1_1_resource_center.html", null ],
    [ "HoloGen.Utils.Scale", "class_holo_gen_1_1_utils_1_1_scale.html", null ],
    [ "HoloGen.Image.ScaleAdder", "class_holo_gen_1_1_image_1_1_scale_adder.html", null ],
    [ "HoloGen.Hierarchy.OptionTypes.SelectOption< AlgorithmPossibility >", "class_holo_gen_1_1_hierarchy_1_1_option_types_1_1_select_option.html", [
      [ "HoloGen.Options.Algorithm.Algorithms.Type.AlgorithmOption", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_algorithm_option.html", null ]
    ] ],
    [ "HoloGen.Hierarchy.OptionTypes.SelectOption< ColorSchemePossibility >", "class_holo_gen_1_1_hierarchy_1_1_option_types_1_1_select_option.html", [
      [ "HoloGen.Image.Options.ColorSchemeOption", "class_holo_gen_1_1_image_1_1_options_1_1_color_scheme_option.html", null ]
    ] ],
    [ "HoloGen.Hierarchy.OptionTypes.SelectOption< CommonVariantPossibility >", "class_holo_gen_1_1_hierarchy_1_1_option_types_1_1_select_option.html", [
      [ "HoloGen.Options.Algorithm.Algorithms.Variants.Common.CommonVariantOption", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_variants_1_1_common_1_1_common_variant_option.html", null ]
    ] ],
    [ "HoloGen.Hierarchy.OptionTypes.SelectOption< DimensionPossibility >", "class_holo_gen_1_1_hierarchy_1_1_option_types_1_1_select_option.html", [
      [ "HoloGen.Image.Options.DimensionOption", "class_holo_gen_1_1_image_1_1_options_1_1_dimension_option.html", null ]
    ] ],
    [ "HoloGen.Hierarchy.OptionTypes.SelectOption< DSVariantPossibility >", "class_holo_gen_1_1_hierarchy_1_1_option_types_1_1_select_option.html", [
      [ "HoloGen.Options.Algorithm.Algorithms.Variants.DS.DSVariantOption", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_variants_1_1_d_s_1_1_d_s_variant_option.html", null ]
    ] ],
    [ "HoloGen.Hierarchy.OptionTypes.SelectOption< ErrorTypePossibility >", "class_holo_gen_1_1_hierarchy_1_1_option_types_1_1_select_option.html", [
      [ "HoloGen.Options.Algorithm.ErrorCalculation.Type.ErrorTypeOption", "class_holo_gen_1_1_options_1_1_algorithm_1_1_error_calculation_1_1_type_1_1_error_type_option.html", null ]
    ] ],
    [ "HoloGen.Hierarchy.OptionTypes.SelectOption< GSVariantPossibility >", "class_holo_gen_1_1_hierarchy_1_1_option_types_1_1_select_option.html", [
      [ "HoloGen.Options.Algorithm.Algorithms.Variants.GS.GSVariantOption", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_variants_1_1_g_s_1_1_g_s_variant_option.html", null ]
    ] ],
    [ "HoloGen.Hierarchy.OptionTypes.SelectOption< HardwarePossibility >", "class_holo_gen_1_1_hierarchy_1_1_option_types_1_1_select_option.html", [
      [ "HoloGen.Options.Hardware.Hardware.Type.HardwareOption", "class_holo_gen_1_1_options_1_1_hardware_1_1_hardware_1_1_type_1_1_hardware_option.html", null ]
    ] ],
    [ "HoloGen.Hierarchy.OptionTypes.SelectOption< IlluminationPossibility >", "class_holo_gen_1_1_hierarchy_1_1_option_types_1_1_select_option.html", [
      [ "HoloGen.Options.Projector.Illumination.Type.IlluminationOption", "class_holo_gen_1_1_options_1_1_projector_1_1_illumination_1_1_type_1_1_illumination_option.html", null ]
    ] ],
    [ "HoloGen.Hierarchy.OptionTypes.SelectOption< ImageViewPossibility >", "class_holo_gen_1_1_hierarchy_1_1_option_types_1_1_select_option.html", [
      [ "HoloGen.Image.Options.ImageViewOption", "class_holo_gen_1_1_image_1_1_options_1_1_image_view_option.html", null ]
    ] ],
    [ "HoloGen.Hierarchy.OptionTypes.SelectOption< MousePossibility >", "class_holo_gen_1_1_hierarchy_1_1_option_types_1_1_select_option.html", [
      [ "HoloGen.Mask.Options.MouseType.MouseOption", "class_holo_gen_1_1_mask_1_1_options_1_1_mouse_type_1_1_mouse_option.html", null ]
    ] ],
    [ "HoloGen.Hierarchy.OptionTypes.SelectOption< OSPRVariantPossibility >", "class_holo_gen_1_1_hierarchy_1_1_option_types_1_1_select_option.html", [
      [ "HoloGen.Options.Algorithm.Algorithms.Variants.OSPR.OSPRVariantOption", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_variants_1_1_o_s_p_r_1_1_o_s_p_r_variant_option.html", null ]
    ] ],
    [ "HoloGen.Hierarchy.OptionTypes.SelectOption< ProcessViewPossibility >", "class_holo_gen_1_1_hierarchy_1_1_option_types_1_1_select_option.html", [
      [ "HoloGen.Process.Options.ProcessViewOption", "class_holo_gen_1_1_process_1_1_options_1_1_process_view_option.html", null ]
    ] ],
    [ "HoloGen.Hierarchy.OptionTypes.SelectOption< RegionPossibility >", "class_holo_gen_1_1_hierarchy_1_1_option_types_1_1_select_option.html", [
      [ "HoloGen.Options.Target.Region.Type.RegionOption", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_type_1_1_region_option.html", null ]
    ] ],
    [ "HoloGen.Hierarchy.OptionTypes.SelectOption< SAVariantPossibility >", "class_holo_gen_1_1_hierarchy_1_1_option_types_1_1_select_option.html", [
      [ "HoloGen.Options.Algorithm.Algorithms.Variants.SA.SAVariantOption", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_variants_1_1_s_a_1_1_s_a_variant_option.html", null ]
    ] ],
    [ "HoloGen.Hierarchy.OptionTypes.SelectOption< ScaleLocationPossibility >", "class_holo_gen_1_1_hierarchy_1_1_option_types_1_1_select_option.html", [
      [ "HoloGen.Image.Options.ScaleLocationOption", "class_holo_gen_1_1_image_1_1_options_1_1_scale_location_option.html", null ]
    ] ],
    [ "HoloGen.Hierarchy.OptionTypes.SelectOption< SeedPossibility >", "class_holo_gen_1_1_hierarchy_1_1_option_types_1_1_select_option.html", [
      [ "HoloGen.Options.Projector.Hologram.SeedOption", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_seed_option.html", null ]
    ] ],
    [ "HoloGen.Hierarchy.OptionTypes.SelectOption< ShapePossibility >", "class_holo_gen_1_1_hierarchy_1_1_option_types_1_1_select_option.html", [
      [ "HoloGen.Mask.Options.ShapeType.ShapeOption", "class_holo_gen_1_1_mask_1_1_options_1_1_shape_type_1_1_shape_option.html", null ]
    ] ],
    [ "HoloGen.Hierarchy.OptionTypes.SelectOption< SLMPossibility >", "class_holo_gen_1_1_hierarchy_1_1_option_types_1_1_select_option.html", [
      [ "HoloGen.Options.Projector.Hologram.SLMTypeOption", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_s_l_m_type_option.html", null ]
    ] ],
    [ "HoloGen.Hierarchy.OptionTypes.SelectOption< SymmetryPossibility >", "class_holo_gen_1_1_hierarchy_1_1_option_types_1_1_select_option.html", [
      [ "HoloGen.Options.Target.Region.Type.SymmetryOption", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_type_1_1_symmetry_option.html", null ]
    ] ],
    [ "HoloGen.Hierarchy.OptionTypes.SelectOption< TargetPossibility >", "class_holo_gen_1_1_hierarchy_1_1_option_types_1_1_select_option.html", [
      [ "HoloGen.Options.Algorithm.Target.Type.TargetOption", "class_holo_gen_1_1_options_1_1_algorithm_1_1_target_1_1_type_1_1_target_option.html", null ]
    ] ],
    [ "HoloGen.Hierarchy.OptionTypes.SelectOption< TerminationPossibility >", "class_holo_gen_1_1_hierarchy_1_1_option_types_1_1_select_option.html", [
      [ "HoloGen.Options.Algorithm.Termination.Type.TerminationOption", "class_holo_gen_1_1_options_1_1_algorithm_1_1_termination_1_1_type_1_1_termination_option.html", null ]
    ] ],
    [ "HoloGen.Hierarchy.OptionTypes.SelectOption< TransformTypePossibility >", "class_holo_gen_1_1_hierarchy_1_1_option_types_1_1_select_option.html", [
      [ "HoloGen.Image.Options.TransformTypeOption", "class_holo_gen_1_1_image_1_1_options_1_1_transform_type_option.html", null ]
    ] ],
    [ "HoloGen.Hierarchy.OptionTypes.SelectOption< VariantPossibility >", "class_holo_gen_1_1_hierarchy_1_1_option_types_1_1_select_option.html", [
      [ "HoloGen.Options.Target.Region.Type.VariantOption", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_type_1_1_variant_option.html", null ]
    ] ],
    [ "HoloGen.Serial.Serialiser< S, T >", "class_holo_gen_1_1_serial_1_1_serialiser.html", [
      [ "HoloGen.Serial.JsonSerialiser< S, T >", "class_holo_gen_1_1_serial_1_1_json_serialiser.html", null ]
    ] ],
    [ "HoloGen.UI.Common.TabContent", "class_holo_gen_1_1_u_i_1_1_common_1_1_tab_content.html", null ],
    [ "HoloGen.UI.Utils.TabHandlerFramework", "class_holo_gen_1_1_u_i_1_1_utils_1_1_tab_handler_framework.html", null ],
    [ "HoloGen.Utils.Time", "class_holo_gen_1_1_utils_1_1_time.html", null ],
    [ "UserControl", null, [
      [ "HoloGen.UI.Graph.Views.GraphView", "class_holo_gen_1_1_u_i_1_1_graph_1_1_views_1_1_graph_view.html", null ],
      [ "HoloGen.UI.Hamburger.Views.HamburgerEditorView", "class_holo_gen_1_1_u_i_1_1_hamburger_1_1_views_1_1_hamburger_editor_view.html", null ],
      [ "HoloGen.UI.Hamburger.Views.HamburgerFolderView", "class_holo_gen_1_1_u_i_1_1_hamburger_1_1_views_1_1_hamburger_folder_view.html", null ],
      [ "HoloGen.UI.Hamburger.Views.HamburgerItemView", "class_holo_gen_1_1_u_i_1_1_hamburger_1_1_views_1_1_hamburger_item_view.html", null ],
      [ "HoloGen.UI.Hamburger.Views.HamburgerPageView", "class_holo_gen_1_1_u_i_1_1_hamburger_1_1_views_1_1_hamburger_page_view.html", null ],
      [ "HoloGen.UI.Hamburger.Views.HamburgerTabView", "class_holo_gen_1_1_u_i_1_1_hamburger_1_1_views_1_1_hamburger_tab_view.html", null ],
      [ "HoloGen.UI.Image.Viewer.ImageViewer", "class_holo_gen_1_1_u_i_1_1_image_1_1_viewer_1_1_image_viewer.html", null ],
      [ "HoloGen.UI.Mask.MaskViewer", "class_holo_gen_1_1_u_i_1_1_mask_1_1_mask_viewer.html", null ],
      [ "HoloGen.UI.Menus.Views.AccentMenuView", "class_holo_gen_1_1_u_i_1_1_menus_1_1_views_1_1_accent_menu_view.html", null ],
      [ "HoloGen.UI.Menus.Views.FileMenuView", "class_holo_gen_1_1_u_i_1_1_menus_1_1_views_1_1_file_menu_view.html", null ],
      [ "HoloGen.UI.Menus.Views.HelpMenuView", "class_holo_gen_1_1_u_i_1_1_menus_1_1_views_1_1_help_menu_view.html", null ],
      [ "HoloGen.UI.Menus.Views.MenuView", "class_holo_gen_1_1_u_i_1_1_menus_1_1_views_1_1_menu_view.html", null ],
      [ "HoloGen.UI.Menus.Views.SettingsMenuPageView", "class_holo_gen_1_1_u_i_1_1_menus_1_1_views_1_1_settings_menu_page_view.html", null ],
      [ "HoloGen.UI.Menus.Views.SettingsMenuView", "class_holo_gen_1_1_u_i_1_1_menus_1_1_views_1_1_settings_menu_view.html", null ],
      [ "HoloGen.UI.Menus.Views.ThemeMenuView", "class_holo_gen_1_1_u_i_1_1_menus_1_1_views_1_1_theme_menu_view.html", null ],
      [ "HoloGen.UI.OptionEditors.BooleanEditor", "class_holo_gen_1_1_u_i_1_1_option_editors_1_1_boolean_editor.html", null ],
      [ "HoloGen.UI.OptionEditors.DoubleEditor", "class_holo_gen_1_1_u_i_1_1_option_editors_1_1_double_editor.html", null ],
      [ "HoloGen.UI.OptionEditors.IntegerEditor", "class_holo_gen_1_1_u_i_1_1_option_editors_1_1_integer_editor.html", null ],
      [ "HoloGen.UI.OptionEditors.PathEditor", "class_holo_gen_1_1_u_i_1_1_option_editors_1_1_path_editor.html", null ],
      [ "HoloGen.UI.OptionEditors.SelectEditor", "class_holo_gen_1_1_u_i_1_1_option_editors_1_1_select_editor.html", null ],
      [ "HoloGen.UI.OptionEditors.TextEditor", "class_holo_gen_1_1_u_i_1_1_option_editors_1_1_text_editor.html", [
        [ "HoloGen.UI.OptionEditors.LargeTextEditor", "class_holo_gen_1_1_u_i_1_1_option_editors_1_1_large_text_editor.html", null ]
      ] ],
      [ "HoloGen.UI.Process.Monitor.Views.ProcessOptionsView", "class_holo_gen_1_1_u_i_1_1_process_1_1_monitor_1_1_views_1_1_process_options_view.html", null ],
      [ "HoloGen.UI.Process.Monitor.Views.ProcessView", "class_holo_gen_1_1_u_i_1_1_process_1_1_monitor_1_1_views_1_1_process_view.html", null ],
      [ "HoloGen.UI.Views.HologramOptionsView", "class_holo_gen_1_1_u_i_1_1_views_1_1_hologram_options_view.html", null ],
      [ "HoloGen.UI.Views.HologramTabView", "class_holo_gen_1_1_u_i_1_1_views_1_1_hologram_tab_view.html", null ],
      [ "HoloGen.UI.Views.HologramView", "class_holo_gen_1_1_u_i_1_1_views_1_1_hologram_view.html", null ],
      [ "HoloGen.UI.Views.OptionsCommandsPanel", "class_holo_gen_1_1_u_i_1_1_views_1_1_options_commands_panel.html", null ],
      [ "HoloGen.UI.Views.PdfTabView", "class_holo_gen_1_1_u_i_1_1_views_1_1_pdf_tab_view.html", null ],
      [ "HoloGen.UI.Views.PdfView", "class_holo_gen_1_1_u_i_1_1_views_1_1_pdf_view.html", null ],
      [ "HoloGen.UI.Views.ProcessTabView", "class_holo_gen_1_1_u_i_1_1_views_1_1_process_tab_view.html", null ],
      [ "HoloGen.UI.Views.ResetControl", "class_holo_gen_1_1_u_i_1_1_views_1_1_reset_control.html", null ],
      [ "HoloGen.UI.Views.RunControl", "class_holo_gen_1_1_u_i_1_1_views_1_1_run_control.html", null ],
      [ "HoloGen.UI.Views.SetupTabView", "class_holo_gen_1_1_u_i_1_1_views_1_1_setup_tab_view.html", null ],
      [ "HoloGen.UI.Views.SetupView", "class_holo_gen_1_1_u_i_1_1_views_1_1_setup_view.html", null ],
      [ "HoloGen.UI.Views.ShowMetadataControl", "class_holo_gen_1_1_u_i_1_1_views_1_1_show_metadata_control.html", null ],
      [ "HoloGen.UI.X3.Viewer.X3Plotter", "class_holo_gen_1_1_u_i_1_1_x3_1_1_viewer_1_1_x3_plotter.html", null ]
    ] ],
    [ "HoloGen.Options.Algorithm.Algorithms.Variants.VariantOption", "interface_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_variants_1_1_variant_option.html", [
      [ "HoloGen.Options.Algorithm.Algorithms.Variants.Common.CommonVariantOption", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_variants_1_1_common_1_1_common_variant_option.html", null ],
      [ "HoloGen.Options.Algorithm.Algorithms.Variants.DS.DSVariantOption", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_variants_1_1_d_s_1_1_d_s_variant_option.html", null ],
      [ "HoloGen.Options.Algorithm.Algorithms.Variants.GS.GSVariantOption", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_variants_1_1_g_s_1_1_g_s_variant_option.html", null ],
      [ "HoloGen.Options.Algorithm.Algorithms.Variants.OSPR.OSPRVariantOption", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_variants_1_1_o_s_p_r_1_1_o_s_p_r_variant_option.html", null ],
      [ "HoloGen.Options.Algorithm.Algorithms.Variants.SA.SAVariantOption", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_variants_1_1_s_a_1_1_s_a_variant_option.html", null ]
    ] ],
    [ "HoloGen.UI.Common.ViewContainer", "class_holo_gen_1_1_u_i_1_1_common_1_1_view_container.html", null ]
];