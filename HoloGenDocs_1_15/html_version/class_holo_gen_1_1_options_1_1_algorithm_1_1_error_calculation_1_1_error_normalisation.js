var class_holo_gen_1_1_options_1_1_algorithm_1_1_error_calculation_1_1_error_normalisation =
[
    [ "Default", "class_holo_gen_1_1_options_1_1_algorithm_1_1_error_calculation_1_1_error_normalisation.html#a00d985c85750441cbd9648df45874d00", null ],
    [ "Name", "class_holo_gen_1_1_options_1_1_algorithm_1_1_error_calculation_1_1_error_normalisation.html#a71e5958e86ff2911b630ddd3669802b1", null ],
    [ "OffText", "class_holo_gen_1_1_options_1_1_algorithm_1_1_error_calculation_1_1_error_normalisation.html#a94189d787239127b4527eada999e55aa", null ],
    [ "OnText", "class_holo_gen_1_1_options_1_1_algorithm_1_1_error_calculation_1_1_error_normalisation.html#a7036780cf9488a0c94bce3631ebbe220", null ],
    [ "ToolTip", "class_holo_gen_1_1_options_1_1_algorithm_1_1_error_calculation_1_1_error_normalisation.html#ab478cc995e7b6862dc33ee56841fa642", null ],
    [ "Watermark", "class_holo_gen_1_1_options_1_1_algorithm_1_1_error_calculation_1_1_error_normalisation.html#a019cd3a8a01ce81ae74171274f81ea2a", null ]
];