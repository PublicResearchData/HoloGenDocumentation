var dir_0c6fcecb90104f38224d0851c13d2f2f =
[
    [ "Type", "dir_2073a72544f72bcc9b115a40ca90d764.html", "dir_2073a72544f72bcc9b115a40ca90d764" ],
    [ "Variants", "dir_6953142ff73f924e5c42d22ddc728834.html", "dir_6953142ff73f924e5c42d22ddc728834" ],
    [ "AlgorithmsFolder.cs", "_algorithms_folder_8cs.html", [
      [ "AlgorithmsFolder", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_algorithms_folder.html", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_algorithms_folder" ]
    ] ],
    [ "StartingTemperature.cs", "_starting_temperature_8cs.html", [
      [ "StartingTemperature", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_starting_temperature.html", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_starting_temperature" ]
    ] ],
    [ "SubFrames.cs", "_sub_frames_8cs.html", [
      [ "SubFrames", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_sub_frames.html", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_sub_frames" ]
    ] ],
    [ "TemperatureCoefficient.cs", "_temperature_coefficient_8cs.html", [
      [ "TemperatureCoefficient", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_temperature_coefficient.html", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_temperature_coefficient" ]
    ] ]
];