var class_holo_gen_1_1_u_i_1_1_view_models_1_1_abstract_tab_view_model =
[
    [ "AbstractTabViewModel", "class_holo_gen_1_1_u_i_1_1_view_models_1_1_abstract_tab_view_model.html#ad179932e94b16525bb503af00f6675e6", null ],
    [ "ExportableImage", "class_holo_gen_1_1_u_i_1_1_view_models_1_1_abstract_tab_view_model.html#ab9b3dc31adb85679121b9d8364178539", null ],
    [ "ExportableImageWithoutScale", "class_holo_gen_1_1_u_i_1_1_view_models_1_1_abstract_tab_view_model.html#a6a05ce6d44c47a372934fef296da7a08", null ],
    [ "HasValidFile", "class_holo_gen_1_1_u_i_1_1_view_models_1_1_abstract_tab_view_model.html#a9fa4931c2a08bd5af0428a368439ac8f", null ],
    [ "Content", "class_holo_gen_1_1_u_i_1_1_view_models_1_1_abstract_tab_view_model.html#a3fb4dae5458faf1938d5d9cbe8b408a3", null ],
    [ "FileLocation", "class_holo_gen_1_1_u_i_1_1_view_models_1_1_abstract_tab_view_model.html#a9c1a2421bcc87eb0833126ad5c4fb4a5", null ],
    [ "IsFileBased", "class_holo_gen_1_1_u_i_1_1_view_models_1_1_abstract_tab_view_model.html#a5fe59d2ca60caadf445571718a9aabac", null ],
    [ "IsImageExportable", "class_holo_gen_1_1_u_i_1_1_view_models_1_1_abstract_tab_view_model.html#a97e7b4922cf093ed2ab91186d6184d76", null ],
    [ "Name", "class_holo_gen_1_1_u_i_1_1_view_models_1_1_abstract_tab_view_model.html#ab03d2c64d13c1d733e2d420f68c38cf2", null ],
    [ "SaveableData", "class_holo_gen_1_1_u_i_1_1_view_models_1_1_abstract_tab_view_model.html#af35e32a99f55bf08405270e40ac4e969", null ]
];