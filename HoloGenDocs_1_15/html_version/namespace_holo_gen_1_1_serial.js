var namespace_holo_gen_1_1_serial =
[
    [ "Deserialiser", "class_holo_gen_1_1_serial_1_1_deserialiser.html", "class_holo_gen_1_1_serial_1_1_deserialiser" ],
    [ "JsonDeserialiser", "class_holo_gen_1_1_serial_1_1_json_deserialiser.html", "class_holo_gen_1_1_serial_1_1_json_deserialiser" ],
    [ "JsonImageDeserialiser", "class_holo_gen_1_1_serial_1_1_json_image_deserialiser.html", null ],
    [ "JsonImageSerialiser", "class_holo_gen_1_1_serial_1_1_json_image_serialiser.html", null ],
    [ "JsonOptionsDeserialiser", "class_holo_gen_1_1_serial_1_1_json_options_deserialiser.html", null ],
    [ "JsonOptionsSerialiser", "class_holo_gen_1_1_serial_1_1_json_options_serialiser.html", null ],
    [ "JsonSerialiser", "class_holo_gen_1_1_serial_1_1_json_serialiser.html", "class_holo_gen_1_1_serial_1_1_json_serialiser" ],
    [ "JsonSettingsDeserialiser", "class_holo_gen_1_1_serial_1_1_json_settings_deserialiser.html", null ],
    [ "JsonSettingsSerialiser", "class_holo_gen_1_1_serial_1_1_json_settings_serialiser.html", null ],
    [ "Serialiser", "class_holo_gen_1_1_serial_1_1_serialiser.html", "class_holo_gen_1_1_serial_1_1_serialiser" ]
];