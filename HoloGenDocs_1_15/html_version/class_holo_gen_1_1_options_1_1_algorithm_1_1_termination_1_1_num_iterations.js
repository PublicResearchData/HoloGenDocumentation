var class_holo_gen_1_1_options_1_1_algorithm_1_1_termination_1_1_num_iterations =
[
    [ "Default", "class_holo_gen_1_1_options_1_1_algorithm_1_1_termination_1_1_num_iterations.html#ac49cbfcea9ad7180cfa73aee5d96b350", null ],
    [ "MaxValue", "class_holo_gen_1_1_options_1_1_algorithm_1_1_termination_1_1_num_iterations.html#a093621de1da60aaf65a84727eb7e7cdc", null ],
    [ "MinValue", "class_holo_gen_1_1_options_1_1_algorithm_1_1_termination_1_1_num_iterations.html#ab19b95fe1aeec5a7b166a34598d516fd", null ],
    [ "Name", "class_holo_gen_1_1_options_1_1_algorithm_1_1_termination_1_1_num_iterations.html#a71c895cf443e57232d0ab0958a04f000", null ],
    [ "ToolTip", "class_holo_gen_1_1_options_1_1_algorithm_1_1_termination_1_1_num_iterations.html#a35021d2d550cd0582ca5bcbd8f5da78e", null ],
    [ "Watermark", "class_holo_gen_1_1_options_1_1_algorithm_1_1_termination_1_1_num_iterations.html#a06507ea125945f725a986eb67c870782", null ]
];