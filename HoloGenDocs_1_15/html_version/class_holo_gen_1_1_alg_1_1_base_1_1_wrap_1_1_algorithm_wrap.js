var class_holo_gen_1_1_alg_1_1_base_1_1_wrap_1_1_algorithm_wrap =
[
    [ "AlgorithmWrap", "class_holo_gen_1_1_alg_1_1_base_1_1_wrap_1_1_algorithm_wrap.html#a97f3e963c05fa7614c24e760f6660fc2", null ],
    [ "AddIlluminationImage", "class_holo_gen_1_1_alg_1_1_base_1_1_wrap_1_1_algorithm_wrap.html#a483249ddce32c64d71e04ede2e69727a", null ],
    [ "AddInitalImage", "class_holo_gen_1_1_alg_1_1_base_1_1_wrap_1_1_algorithm_wrap.html#aaac3ea20137c3bad88c62906b3252a67", null ],
    [ "AddTargetImage", "class_holo_gen_1_1_alg_1_1_base_1_1_wrap_1_1_algorithm_wrap.html#a2ee8c9b34781e1071deec1b6a9125678", null ],
    [ "CheckResolutions", "class_holo_gen_1_1_alg_1_1_base_1_1_wrap_1_1_algorithm_wrap.html#a13691a231051455c189bf00d5d334c22", null ],
    [ "GetResult", "class_holo_gen_1_1_alg_1_1_base_1_1_wrap_1_1_algorithm_wrap.html#ae5aac6c780f62846c24c1f13af998d1c", null ],
    [ "RunCleanup", "class_holo_gen_1_1_alg_1_1_base_1_1_wrap_1_1_algorithm_wrap.html#ab56a45ae392067b13d30454ccb34619e", null ],
    [ "RunIterations", "class_holo_gen_1_1_alg_1_1_base_1_1_wrap_1_1_algorithm_wrap.html#ace90e121b6cf80020fd0954b5103a2cb", null ],
    [ "RunStartup", "class_holo_gen_1_1_alg_1_1_base_1_1_wrap_1_1_algorithm_wrap.html#aab3cea7b59b4f07c570dc78ab94106e4", null ],
    [ "SetBaseParameters", "class_holo_gen_1_1_alg_1_1_base_1_1_wrap_1_1_algorithm_wrap.html#ae6449a37c35daefdaa940d1607ac0599", null ],
    [ "SetLogger", "class_holo_gen_1_1_alg_1_1_base_1_1_wrap_1_1_algorithm_wrap.html#a8154ffe208b48cd8b63e7c5190593687", null ],
    [ "managedAlgBase", "class_holo_gen_1_1_alg_1_1_base_1_1_wrap_1_1_algorithm_wrap.html#aa4a9e76a3a9221325babaf4131a108ed", null ]
];