var class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_pixel_pitch =
[
    [ "Default", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_pixel_pitch.html#a5595547a4768d649e415f4e3a74a257e", null ],
    [ "Enabled", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_pixel_pitch.html#a7595d5ea9dbf54bc3691f23a424f312a", null ],
    [ "MaxValue", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_pixel_pitch.html#a3d52ce2f1f8eb78e9c6216756d86c32b", null ],
    [ "MinValue", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_pixel_pitch.html#af006f8952c0390b450f0853ab52b4b34", null ],
    [ "Name", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_pixel_pitch.html#ae7b1c154aea3fc1f0b94009b4eaae806", null ],
    [ "ToolTip", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_pixel_pitch.html#a55973ef41fbe783808ed746425b37dab", null ],
    [ "Watermark", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_pixel_pitch.html#a02d961235814cbbf706ee9565edd6fd2", null ]
];