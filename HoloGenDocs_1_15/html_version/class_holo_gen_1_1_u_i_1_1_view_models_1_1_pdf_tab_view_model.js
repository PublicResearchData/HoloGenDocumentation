var class_holo_gen_1_1_u_i_1_1_view_models_1_1_pdf_tab_view_model =
[
    [ "PdfTabViewModel", "class_holo_gen_1_1_u_i_1_1_view_models_1_1_pdf_tab_view_model.html#ac445e80e1c3dc249c13b2f38491b1c4e", null ],
    [ "IsFileBased", "class_holo_gen_1_1_u_i_1_1_view_models_1_1_pdf_tab_view_model.html#a53c2c32f44804a696be666ac019bd1c5", null ],
    [ "IsImageExportable", "class_holo_gen_1_1_u_i_1_1_view_models_1_1_pdf_tab_view_model.html#ae5031de1a6b292e2b0610b8ab68e4190", null ],
    [ "SaveableData", "class_holo_gen_1_1_u_i_1_1_view_models_1_1_pdf_tab_view_model.html#af8b7a5a1a0c72c381ed6bd90f6c794f8", null ],
    [ "PdfViewModel", "class_holo_gen_1_1_u_i_1_1_view_models_1_1_pdf_tab_view_model.html#abb104088646a242c0ae21cfb8257ed61", null ]
];