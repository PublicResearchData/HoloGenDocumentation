var class_holo_gen_1_1_u_i_1_1_view_models_1_1_process_tab_view_model =
[
    [ "ProcessTabViewModel", "class_holo_gen_1_1_u_i_1_1_view_models_1_1_process_tab_view_model.html#ad5b57eee646253e8bd56a1107f575e17", null ],
    [ "IsFileBased", "class_holo_gen_1_1_u_i_1_1_view_models_1_1_process_tab_view_model.html#a0953dbfe78f004770b2031a6e2b33fc7", null ],
    [ "IsImageExportable", "class_holo_gen_1_1_u_i_1_1_view_models_1_1_process_tab_view_model.html#a4edfba2412daf8370578013054554324", null ],
    [ "ProcessViewModel", "class_holo_gen_1_1_u_i_1_1_view_models_1_1_process_tab_view_model.html#a5698246c0c10989d6af3694345d02b91", null ],
    [ "SaveableData", "class_holo_gen_1_1_u_i_1_1_view_models_1_1_process_tab_view_model.html#a2ba82c40340ea2d15ffa20357449c401", null ],
    [ "Name", "class_holo_gen_1_1_u_i_1_1_view_models_1_1_process_tab_view_model.html#a9185a4ee91ff9e4b18b039f4a7cfe919", null ]
];