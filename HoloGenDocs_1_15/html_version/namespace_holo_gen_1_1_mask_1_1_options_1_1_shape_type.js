var namespace_holo_gen_1_1_mask_1_1_options_1_1_shape_type =
[
    [ "ShapeEllipse", "class_holo_gen_1_1_mask_1_1_options_1_1_shape_type_1_1_shape_ellipse.html", "class_holo_gen_1_1_mask_1_1_options_1_1_shape_type_1_1_shape_ellipse" ],
    [ "ShapeOption", "class_holo_gen_1_1_mask_1_1_options_1_1_shape_type_1_1_shape_option.html", "class_holo_gen_1_1_mask_1_1_options_1_1_shape_type_1_1_shape_option" ],
    [ "ShapePoint", "class_holo_gen_1_1_mask_1_1_options_1_1_shape_type_1_1_shape_point.html", "class_holo_gen_1_1_mask_1_1_options_1_1_shape_type_1_1_shape_point" ],
    [ "ShapePolygon", "class_holo_gen_1_1_mask_1_1_options_1_1_shape_type_1_1_shape_polygon.html", "class_holo_gen_1_1_mask_1_1_options_1_1_shape_type_1_1_shape_polygon" ],
    [ "ShapePossibilities", "class_holo_gen_1_1_mask_1_1_options_1_1_shape_type_1_1_shape_possibilities.html", "class_holo_gen_1_1_mask_1_1_options_1_1_shape_type_1_1_shape_possibilities" ],
    [ "ShapePossibility", "class_holo_gen_1_1_mask_1_1_options_1_1_shape_type_1_1_shape_possibility.html", null ],
    [ "ShapeRectangle", "class_holo_gen_1_1_mask_1_1_options_1_1_shape_type_1_1_shape_rectangle.html", "class_holo_gen_1_1_mask_1_1_options_1_1_shape_type_1_1_shape_rectangle" ]
];