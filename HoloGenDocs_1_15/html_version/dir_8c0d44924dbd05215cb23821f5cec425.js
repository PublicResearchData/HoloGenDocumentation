var dir_8c0d44924dbd05215cb23821f5cec425 =
[
    [ "AbstractMenuViewModel.cs", "_abstract_menu_view_model_8cs.html", [
      [ "AbstractMenuViewModel", "class_holo_gen_1_1_u_i_1_1_menus_1_1_view_models_1_1_abstract_menu_view_model.html", "class_holo_gen_1_1_u_i_1_1_menus_1_1_view_models_1_1_abstract_menu_view_model" ]
    ] ],
    [ "AccentMenuViewModel.cs", "_accent_menu_view_model_8cs.html", [
      [ "AccentMenuViewModel", "class_holo_gen_1_1_u_i_1_1_menus_1_1_view_models_1_1_accent_menu_view_model.html", "class_holo_gen_1_1_u_i_1_1_menus_1_1_view_models_1_1_accent_menu_view_model" ]
    ] ],
    [ "FileMenuViewModel.cs", "_file_menu_view_model_8cs.html", [
      [ "FileMenuViewModel", "class_holo_gen_1_1_u_i_1_1_menus_1_1_view_models_1_1_file_menu_view_model.html", "class_holo_gen_1_1_u_i_1_1_menus_1_1_view_models_1_1_file_menu_view_model" ]
    ] ],
    [ "HelpMenuView.cs", "_help_menu_view_8cs.html", [
      [ "HelpMenuViewModel", "class_holo_gen_1_1_u_i_1_1_menus_1_1_view_models_1_1_help_menu_view_model.html", "class_holo_gen_1_1_u_i_1_1_menus_1_1_view_models_1_1_help_menu_view_model" ]
    ] ],
    [ "MenuViewModel.cs", "_menu_view_model_8cs.html", [
      [ "MenuViewModel", "class_holo_gen_1_1_u_i_1_1_menus_1_1_view_models_1_1_menu_view_model.html", "class_holo_gen_1_1_u_i_1_1_menus_1_1_view_models_1_1_menu_view_model" ]
    ] ],
    [ "SettingsMenuViewModel.cs", "_settings_menu_view_model_8cs.html", [
      [ "SettingsMenuViewModel", "class_holo_gen_1_1_u_i_1_1_menus_1_1_view_models_1_1_settings_menu_view_model.html", "class_holo_gen_1_1_u_i_1_1_menus_1_1_view_models_1_1_settings_menu_view_model" ]
    ] ],
    [ "ThemeMenuViewModel.cs", "_theme_menu_view_model_8cs.html", [
      [ "ThemeMenuViewModel", "class_holo_gen_1_1_u_i_1_1_menus_1_1_view_models_1_1_theme_menu_view_model.html", "class_holo_gen_1_1_u_i_1_1_menus_1_1_view_models_1_1_theme_menu_view_model" ]
    ] ]
];