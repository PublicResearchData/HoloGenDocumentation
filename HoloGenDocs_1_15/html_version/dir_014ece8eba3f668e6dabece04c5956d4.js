var dir_014ece8eba3f668e6dabece04c5956d4 =
[
    [ "TerminationError.cs", "_termination_error_8cs.html", [
      [ "TerminationError", "class_holo_gen_1_1_options_1_1_algorithm_1_1_termination_1_1_type_1_1_termination_error.html", "class_holo_gen_1_1_options_1_1_algorithm_1_1_termination_1_1_type_1_1_termination_error" ]
    ] ],
    [ "TerminationFirst.cs", "_termination_first_8cs.html", [
      [ "TerminationFirst", "class_holo_gen_1_1_options_1_1_algorithm_1_1_termination_1_1_type_1_1_termination_first.html", "class_holo_gen_1_1_options_1_1_algorithm_1_1_termination_1_1_type_1_1_termination_first" ]
    ] ],
    [ "TerminationIterations.cs", "_termination_iterations_8cs.html", [
      [ "TerminationIterations", "class_holo_gen_1_1_options_1_1_algorithm_1_1_termination_1_1_type_1_1_termination_iterations.html", "class_holo_gen_1_1_options_1_1_algorithm_1_1_termination_1_1_type_1_1_termination_iterations" ]
    ] ],
    [ "TerminationOption.cs", "_termination_option_8cs.html", [
      [ "TerminationOption", "class_holo_gen_1_1_options_1_1_algorithm_1_1_termination_1_1_type_1_1_termination_option.html", "class_holo_gen_1_1_options_1_1_algorithm_1_1_termination_1_1_type_1_1_termination_option" ]
    ] ],
    [ "TerminationPossibilities.cs", "_termination_possibilities_8cs.html", [
      [ "TerminationPossibilities", "class_holo_gen_1_1_options_1_1_algorithm_1_1_termination_1_1_type_1_1_termination_possibilities.html", "class_holo_gen_1_1_options_1_1_algorithm_1_1_termination_1_1_type_1_1_termination_possibilities" ]
    ] ],
    [ "TerminationPossibility.cs", "_termination_possibility_8cs.html", [
      [ "TerminationPossibility", "class_holo_gen_1_1_options_1_1_algorithm_1_1_termination_1_1_type_1_1_termination_possibility.html", null ]
    ] ]
];