var namespace_holo_gen_1_1_options =
[
    [ "Algorithm", "namespace_holo_gen_1_1_options_1_1_algorithm.html", "namespace_holo_gen_1_1_options_1_1_algorithm" ],
    [ "Hardware", "namespace_holo_gen_1_1_options_1_1_hardware.html", "namespace_holo_gen_1_1_options_1_1_hardware" ],
    [ "Output", "namespace_holo_gen_1_1_options_1_1_output.html", "namespace_holo_gen_1_1_options_1_1_output" ],
    [ "Projector", "namespace_holo_gen_1_1_options_1_1_projector.html", "namespace_holo_gen_1_1_options_1_1_projector" ],
    [ "Target", "namespace_holo_gen_1_1_options_1_1_target.html", "namespace_holo_gen_1_1_options_1_1_target" ],
    [ "ImagePathOption", "class_holo_gen_1_1_options_1_1_image_path_option.html", "class_holo_gen_1_1_options_1_1_image_path_option" ],
    [ "OptionsRoot", "class_holo_gen_1_1_options_1_1_options_root.html", "class_holo_gen_1_1_options_1_1_options_root" ],
    [ "OptionsRootVersion", "class_holo_gen_1_1_options_1_1_options_root_version.html", "class_holo_gen_1_1_options_1_1_options_root_version" ],
    [ "OutputPage", "class_holo_gen_1_1_options_1_1_output_page.html", "class_holo_gen_1_1_options_1_1_output_page" ]
];