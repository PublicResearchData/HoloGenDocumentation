var namespace_holo_gen_1_1_u_i =
[
    [ "Commands", "namespace_holo_gen_1_1_u_i_1_1_commands.html", "namespace_holo_gen_1_1_u_i_1_1_commands" ],
    [ "Common", "namespace_holo_gen_1_1_u_i_1_1_common.html", "namespace_holo_gen_1_1_u_i_1_1_common" ],
    [ "Graph", "namespace_holo_gen_1_1_u_i_1_1_graph.html", "namespace_holo_gen_1_1_u_i_1_1_graph" ],
    [ "Hamburger", "namespace_holo_gen_1_1_u_i_1_1_hamburger.html", "namespace_holo_gen_1_1_u_i_1_1_hamburger" ],
    [ "Image", "namespace_holo_gen_1_1_u_i_1_1_image.html", "namespace_holo_gen_1_1_u_i_1_1_image" ],
    [ "Mask", "namespace_holo_gen_1_1_u_i_1_1_mask.html", "namespace_holo_gen_1_1_u_i_1_1_mask" ],
    [ "Menus", "namespace_holo_gen_1_1_u_i_1_1_menus.html", "namespace_holo_gen_1_1_u_i_1_1_menus" ],
    [ "OptionEditors", "namespace_holo_gen_1_1_u_i_1_1_option_editors.html", "namespace_holo_gen_1_1_u_i_1_1_option_editors" ],
    [ "Process", "namespace_holo_gen_1_1_u_i_1_1_process.html", "namespace_holo_gen_1_1_u_i_1_1_process" ],
    [ "Utils", "namespace_holo_gen_1_1_u_i_1_1_utils.html", "namespace_holo_gen_1_1_u_i_1_1_utils" ],
    [ "ViewModels", "namespace_holo_gen_1_1_u_i_1_1_view_models.html", "namespace_holo_gen_1_1_u_i_1_1_view_models" ],
    [ "Views", "namespace_holo_gen_1_1_u_i_1_1_views.html", "namespace_holo_gen_1_1_u_i_1_1_views" ],
    [ "X3", "namespace_holo_gen_1_1_u_i_1_1_x3.html", "namespace_holo_gen_1_1_u_i_1_1_x3" ],
    [ "App", "class_holo_gen_1_1_u_i_1_1_app.html", "class_holo_gen_1_1_u_i_1_1_app" ],
    [ "MainWindow", "class_holo_gen_1_1_u_i_1_1_main_window.html", "class_holo_gen_1_1_u_i_1_1_main_window" ],
    [ "MainWindowViewModel", "class_holo_gen_1_1_u_i_1_1_main_window_view_model.html", "class_holo_gen_1_1_u_i_1_1_main_window_view_model" ]
];