var namespace_holo_gen_1_1_u_i_1_1_menus_1_1_view_models =
[
    [ "AbstractMenuViewModel", "class_holo_gen_1_1_u_i_1_1_menus_1_1_view_models_1_1_abstract_menu_view_model.html", "class_holo_gen_1_1_u_i_1_1_menus_1_1_view_models_1_1_abstract_menu_view_model" ],
    [ "AccentMenuViewModel", "class_holo_gen_1_1_u_i_1_1_menus_1_1_view_models_1_1_accent_menu_view_model.html", "class_holo_gen_1_1_u_i_1_1_menus_1_1_view_models_1_1_accent_menu_view_model" ],
    [ "FileMenuViewModel", "class_holo_gen_1_1_u_i_1_1_menus_1_1_view_models_1_1_file_menu_view_model.html", "class_holo_gen_1_1_u_i_1_1_menus_1_1_view_models_1_1_file_menu_view_model" ],
    [ "HelpMenuViewModel", "class_holo_gen_1_1_u_i_1_1_menus_1_1_view_models_1_1_help_menu_view_model.html", "class_holo_gen_1_1_u_i_1_1_menus_1_1_view_models_1_1_help_menu_view_model" ],
    [ "MenuViewModel", "class_holo_gen_1_1_u_i_1_1_menus_1_1_view_models_1_1_menu_view_model.html", "class_holo_gen_1_1_u_i_1_1_menus_1_1_view_models_1_1_menu_view_model" ],
    [ "SettingsMenuViewModel", "class_holo_gen_1_1_u_i_1_1_menus_1_1_view_models_1_1_settings_menu_view_model.html", "class_holo_gen_1_1_u_i_1_1_menus_1_1_view_models_1_1_settings_menu_view_model" ],
    [ "ThemeMenuViewModel", "class_holo_gen_1_1_u_i_1_1_menus_1_1_view_models_1_1_theme_menu_view_model.html", "class_holo_gen_1_1_u_i_1_1_menus_1_1_view_models_1_1_theme_menu_view_model" ]
];