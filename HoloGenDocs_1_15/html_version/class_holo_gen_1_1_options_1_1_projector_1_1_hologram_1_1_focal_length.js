var class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_focal_length =
[
    [ "Default", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_focal_length.html#a73e2c37994c376ad6fed2cbc3996fd88", null ],
    [ "Enabled", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_focal_length.html#a51216e9ca6b52c6a00cb8d2ff8ea24f2", null ],
    [ "MaxValue", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_focal_length.html#a30f03b2d1a5870719f5730b9c15fafdf", null ],
    [ "MinValue", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_focal_length.html#a580469305b4d8a0d08b5ab0b11e74a9b", null ],
    [ "Name", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_focal_length.html#a8d713fecd02e3304bb065902ce2dbf1f", null ],
    [ "ToolTip", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_focal_length.html#ae28a17533c07561621be46755ac05bbb", null ],
    [ "Watermark", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_focal_length.html#a96ad3ab44d6f12dda59b2180fefc3fe2", null ]
];