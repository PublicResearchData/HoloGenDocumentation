var class_holo_gen_1_1_options_1_1_algorithm_1_1_termination_1_1_error_value =
[
    [ "Default", "class_holo_gen_1_1_options_1_1_algorithm_1_1_termination_1_1_error_value.html#aa11add8d3e9516d94c496e4a204facc3", null ],
    [ "MaxValue", "class_holo_gen_1_1_options_1_1_algorithm_1_1_termination_1_1_error_value.html#a46492ebb2d6c2a99e0dbed1ff25c11b2", null ],
    [ "MinValue", "class_holo_gen_1_1_options_1_1_algorithm_1_1_termination_1_1_error_value.html#a5a311080c0756b25b891199c720eb69c", null ],
    [ "Name", "class_holo_gen_1_1_options_1_1_algorithm_1_1_termination_1_1_error_value.html#a830a28a568d3711f8576d934a6f4acbc", null ],
    [ "ToolTip", "class_holo_gen_1_1_options_1_1_algorithm_1_1_termination_1_1_error_value.html#a11e79dd2cfc6135a97a5753a46b53c67", null ],
    [ "Watermark", "class_holo_gen_1_1_options_1_1_algorithm_1_1_termination_1_1_error_value.html#aa46786b87fc7fab7cdc95889f211d42f", null ]
];