var class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_variants_1_1_common_1_1_common_variant_option =
[
    [ "Name", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_variants_1_1_common_1_1_common_variant_option.html#a5089ae29edd98d09eef47f4f2adf26c6", null ],
    [ "Possibilities", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_variants_1_1_common_1_1_common_variant_option.html#a78583b54fc183762acd61e0c2ca99ed9", null ],
    [ "Possibilities2", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_variants_1_1_common_1_1_common_variant_option.html#ab39b737c8d56b6ef4b882e5b44b71dc0", null ],
    [ "ToolTip", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_variants_1_1_common_1_1_common_variant_option.html#ab630181a0aa9ad963efbc966f67084e8", null ],
    [ "VariantPossibility", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_variants_1_1_common_1_1_common_variant_option.html#a85351c97dc25d133f87d78b00aec9f55", null ]
];