var dir_3cfebdf9126b7fde092cd7cf978b725c =
[
    [ "ProcessViewEfficiency.cs", "_process_view_efficiency_8cs.html", [
      [ "ProcessViewEfficiency", "class_holo_gen_1_1_process_1_1_options_1_1_process_view_efficiency.html", "class_holo_gen_1_1_process_1_1_options_1_1_process_view_efficiency" ]
    ] ],
    [ "ProcessViewError.cs", "_process_view_error_8cs.html", [
      [ "ProcessViewError", "class_holo_gen_1_1_process_1_1_options_1_1_process_view_error.html", "class_holo_gen_1_1_process_1_1_options_1_1_process_view_error" ]
    ] ],
    [ "ProcessViewOption.cs", "_process_view_option_8cs.html", [
      [ "ProcessViewOption", "class_holo_gen_1_1_process_1_1_options_1_1_process_view_option.html", "class_holo_gen_1_1_process_1_1_options_1_1_process_view_option" ]
    ] ],
    [ "ProcessViewPossibilities.cs", "_process_view_possibilities_8cs.html", [
      [ "ProcessViewPossibilities", "class_holo_gen_1_1_process_1_1_options_1_1_process_view_possibilities.html", "class_holo_gen_1_1_process_1_1_options_1_1_process_view_possibilities" ]
    ] ],
    [ "ProcessViewPossibility.cs", "_process_view_possibility_8cs.html", [
      [ "ProcessViewPossibility", "class_holo_gen_1_1_process_1_1_options_1_1_process_view_possibility.html", "class_holo_gen_1_1_process_1_1_options_1_1_process_view_possibility" ]
    ] ]
];