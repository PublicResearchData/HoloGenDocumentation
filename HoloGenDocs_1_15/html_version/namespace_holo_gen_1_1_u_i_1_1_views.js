var namespace_holo_gen_1_1_u_i_1_1_views =
[
    [ "HologramOptionsView", "class_holo_gen_1_1_u_i_1_1_views_1_1_hologram_options_view.html", "class_holo_gen_1_1_u_i_1_1_views_1_1_hologram_options_view" ],
    [ "HologramTabView", "class_holo_gen_1_1_u_i_1_1_views_1_1_hologram_tab_view.html", "class_holo_gen_1_1_u_i_1_1_views_1_1_hologram_tab_view" ],
    [ "HologramView", "class_holo_gen_1_1_u_i_1_1_views_1_1_hologram_view.html", "class_holo_gen_1_1_u_i_1_1_views_1_1_hologram_view" ],
    [ "OptionsCommandsPanel", "class_holo_gen_1_1_u_i_1_1_views_1_1_options_commands_panel.html", "class_holo_gen_1_1_u_i_1_1_views_1_1_options_commands_panel" ],
    [ "PdfTabView", "class_holo_gen_1_1_u_i_1_1_views_1_1_pdf_tab_view.html", "class_holo_gen_1_1_u_i_1_1_views_1_1_pdf_tab_view" ],
    [ "PdfView", "class_holo_gen_1_1_u_i_1_1_views_1_1_pdf_view.html", "class_holo_gen_1_1_u_i_1_1_views_1_1_pdf_view" ],
    [ "ProcessTabView", "class_holo_gen_1_1_u_i_1_1_views_1_1_process_tab_view.html", "class_holo_gen_1_1_u_i_1_1_views_1_1_process_tab_view" ],
    [ "ResetControl", "class_holo_gen_1_1_u_i_1_1_views_1_1_reset_control.html", "class_holo_gen_1_1_u_i_1_1_views_1_1_reset_control" ],
    [ "RunControl", "class_holo_gen_1_1_u_i_1_1_views_1_1_run_control.html", "class_holo_gen_1_1_u_i_1_1_views_1_1_run_control" ],
    [ "SetupTabView", "class_holo_gen_1_1_u_i_1_1_views_1_1_setup_tab_view.html", "class_holo_gen_1_1_u_i_1_1_views_1_1_setup_tab_view" ],
    [ "SetupView", "class_holo_gen_1_1_u_i_1_1_views_1_1_setup_view.html", "class_holo_gen_1_1_u_i_1_1_views_1_1_setup_view" ],
    [ "ShowMetadataControl", "class_holo_gen_1_1_u_i_1_1_views_1_1_show_metadata_control.html", "class_holo_gen_1_1_u_i_1_1_views_1_1_show_metadata_control" ]
];