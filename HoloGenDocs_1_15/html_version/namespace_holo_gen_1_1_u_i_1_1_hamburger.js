var namespace_holo_gen_1_1_u_i_1_1_hamburger =
[
    [ "ViewModels", "namespace_holo_gen_1_1_u_i_1_1_hamburger_1_1_view_models.html", "namespace_holo_gen_1_1_u_i_1_1_hamburger_1_1_view_models" ],
    [ "Views", "namespace_holo_gen_1_1_u_i_1_1_hamburger_1_1_views.html", "namespace_holo_gen_1_1_u_i_1_1_hamburger_1_1_views" ],
    [ "NullToUnsetValueConverter", "class_holo_gen_1_1_u_i_1_1_hamburger_1_1_null_to_unset_value_converter.html", "class_holo_gen_1_1_u_i_1_1_hamburger_1_1_null_to_unset_value_converter" ],
    [ "PageDataTemplateSelector", "class_holo_gen_1_1_u_i_1_1_hamburger_1_1_page_data_template_selector.html", "class_holo_gen_1_1_u_i_1_1_hamburger_1_1_page_data_template_selector" ]
];