var class_holo_gen_1_1_u_i_1_1_view_models_1_1_hologram_view_model =
[
    [ "HologramViewModel", "class_holo_gen_1_1_u_i_1_1_view_models_1_1_hologram_view_model.html#abb502cceeabb900cc381af1fa3037d42", null ],
    [ "MetadataEnabled", "class_holo_gen_1_1_u_i_1_1_view_models_1_1_hologram_view_model.html#a75fe6344c78657540e1273c4d1b32e85", null ],
    [ "MetadataCommand", "class_holo_gen_1_1_u_i_1_1_view_models_1_1_hologram_view_model.html#a03dbe9894e110c2c929d850a1a4fe84b", null ],
    [ "Options", "class_holo_gen_1_1_u_i_1_1_view_models_1_1_hologram_view_model.html#a246ed6fa72b11bf3e6868a6d08ccdeea", null ],
    [ "PlotterVisibility", "class_holo_gen_1_1_u_i_1_1_view_models_1_1_hologram_view_model.html#a8cb1edbc0f872666219c8582272ef9fc", null ],
    [ "ViewerVisibility", "class_holo_gen_1_1_u_i_1_1_view_models_1_1_hologram_view_model.html#a4a3d44a5847be19b120cf4278f3020fe", null ]
];