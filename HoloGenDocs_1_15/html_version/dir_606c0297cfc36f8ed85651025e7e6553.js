var dir_606c0297cfc36f8ed85651025e7e6553 =
[
    [ "BackProjectSeed.cs", "_back_project_seed_8cs.html", [
      [ "BackProjectSeed", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_back_project_seed.html", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_back_project_seed" ]
    ] ],
    [ "BinaryAmpSLM.cs", "_binary_amp_s_l_m_8cs.html", [
      [ "BinaryAmpSLM", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_binary_amp_s_l_m.html", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_binary_amp_s_l_m" ]
    ] ],
    [ "BinaryPhaseSLM.cs", "_binary_phase_s_l_m_8cs.html", [
      [ "BinaryPhaseSLM", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_binary_phase_s_l_m.html", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_binary_phase_s_l_m" ]
    ] ],
    [ "FocalLength.cs", "_focal_length_8cs.html", [
      [ "FocalLength", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_focal_length.html", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_focal_length" ]
    ] ],
    [ "FromFileSeed.cs", "_from_file_seed_8cs.html", [
      [ "FromFileSeed", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_from_file_seed.html", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_from_file_seed" ]
    ] ],
    [ "HologramFolder.cs", "_hologram_folder_8cs.html", [
      [ "HologramFolder", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_hologram_folder.html", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_hologram_folder" ]
    ] ],
    [ "MaximumAngle.cs", "_maximum_angle_8cs.html", [
      [ "MaximumAngle", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_maximum_angle.html", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_maximum_angle" ]
    ] ],
    [ "MaximumLevel.cs", "_maximum_level_8cs.html", [
      [ "MaximumLevel", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_maximum_level.html", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_maximum_level" ]
    ] ],
    [ "MinimumAngle.cs", "_minimum_angle_8cs.html", [
      [ "MinimumAngle", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_minimum_angle.html", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_minimum_angle" ]
    ] ],
    [ "MinimumLevel.cs", "_minimum_level_8cs.html", [
      [ "MinimumLevel", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_minimum_level.html", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_minimum_level" ]
    ] ],
    [ "MultiAmpSLM.CS", "_multi_amp_s_l_m_8_c_s.html", [
      [ "MultiAmpSLM", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_multi_amp_s_l_m.html", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_multi_amp_s_l_m" ]
    ] ],
    [ "MultiPhaseSLM.cs", "_multi_phase_s_l_m_8cs.html", [
      [ "MultiPhaseSLM", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_multi_phase_s_l_m.html", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_multi_phase_s_l_m" ]
    ] ],
    [ "Oversampling.cs", "_oversampling_8cs.html", [
      [ "Oversampling", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_oversampling.html", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_oversampling" ]
    ] ],
    [ "PixelPitch.cs", "_pixel_pitch_8cs.html", [
      [ "PixelPitch", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_pixel_pitch.html", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_pixel_pitch" ]
    ] ],
    [ "RandomSeed.cs", "_random_seed_8cs.html", [
      [ "RandomSeed", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_random_seed.html", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_random_seed" ]
    ] ],
    [ "SeedFile.cs", "_seed_file_8cs.html", [
      [ "SeedFile", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_seed_file.html", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_seed_file" ]
    ] ],
    [ "SeedOption.cs", "_seed_option_8cs.html", [
      [ "SeedOption", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_seed_option.html", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_seed_option" ]
    ] ],
    [ "SeedPossibilities.cs", "_seed_possibilities_8cs.html", [
      [ "SeedPossibilities", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_seed_possibilities.html", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_seed_possibilities" ]
    ] ],
    [ "SeedPossibility.cs", "_seed_possibility_8cs.html", [
      [ "SeedPossibility", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_seed_possibility.html", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_seed_possibility" ]
    ] ],
    [ "SLMLevels.cs", "_s_l_m_levels_8cs.html", [
      [ "SLMLevels", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_s_l_m_levels.html", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_s_l_m_levels" ]
    ] ],
    [ "SLMPossibilities.cs", "_s_l_m_possibilities_8cs.html", [
      [ "SLMPossibilities", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_s_l_m_possibilities.html", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_s_l_m_possibilities" ]
    ] ],
    [ "SLMPossibility.cs", "_s_l_m_possibility_8cs.html", [
      [ "SLMPossibility", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_s_l_m_possibility.html", null ]
    ] ],
    [ "SLMResolutionX.cs", "_s_l_m_resolution_x_8cs.html", [
      [ "SLMResolutionX", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_s_l_m_resolution_x.html", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_s_l_m_resolution_x" ]
    ] ],
    [ "SLMResolutionY.cs", "_s_l_m_resolution_y_8cs.html", [
      [ "SLMResolutionY", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_s_l_m_resolution_y.html", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_s_l_m_resolution_y" ]
    ] ],
    [ "SLMTypeOption.cs", "_s_l_m_type_option_8cs.html", [
      [ "SLMTypeOption", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_s_l_m_type_option.html", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_s_l_m_type_option" ]
    ] ],
    [ "Wavelength.cs", "_wavelength_8cs.html", [
      [ "Wavelength", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_wavelength.html", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_wavelength" ]
    ] ]
];