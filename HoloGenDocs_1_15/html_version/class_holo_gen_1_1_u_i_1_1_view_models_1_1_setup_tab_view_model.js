var class_holo_gen_1_1_u_i_1_1_view_models_1_1_setup_tab_view_model =
[
    [ "SetupTabViewModel", "class_holo_gen_1_1_u_i_1_1_view_models_1_1_setup_tab_view_model.html#a5194022dbc16547f2523ab3187e9bd91", null ],
    [ "IsFileBased", "class_holo_gen_1_1_u_i_1_1_view_models_1_1_setup_tab_view_model.html#a1fa398baefe1d4bf1c0fc59f32296d67", null ],
    [ "IsImageExportable", "class_holo_gen_1_1_u_i_1_1_view_models_1_1_setup_tab_view_model.html#aefdd634fad8cede72238598f6ad105ed", null ],
    [ "SaveableData", "class_holo_gen_1_1_u_i_1_1_view_models_1_1_setup_tab_view_model.html#a038522e83423ecc020f8b8dc0d0efd1e", null ],
    [ "SetupViewModel", "class_holo_gen_1_1_u_i_1_1_view_models_1_1_setup_tab_view_model.html#ad8b5958cc77f4501b1206fe117f32e01", null ]
];