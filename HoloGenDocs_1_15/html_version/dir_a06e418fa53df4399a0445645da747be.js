var dir_a06e418fa53df4399a0445645da747be =
[
    [ "Algorithms", "dir_0c6fcecb90104f38224d0851c13d2f2f.html", "dir_0c6fcecb90104f38224d0851c13d2f2f" ],
    [ "ErrorCalculation", "dir_f4c0475804b55cb338affd3a6bc74cc2.html", "dir_f4c0475804b55cb338affd3a6bc74cc2" ],
    [ "Target", "dir_808e9b9f555f839c0f69b424bda00a66.html", "dir_808e9b9f555f839c0f69b424bda00a66" ],
    [ "Termination", "dir_49e583c1cd82d035c4ffaea82bc0d8d8.html", "dir_49e583c1cd82d035c4ffaea82bc0d8d8" ],
    [ "AlgorithmsPage.cs", "_algorithms_page_8cs.html", [
      [ "AlgorithmsPage", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_page.html", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_page" ]
    ] ]
];