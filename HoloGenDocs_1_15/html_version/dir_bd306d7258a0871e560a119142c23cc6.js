var dir_bd306d7258a0871e560a119142c23cc6 =
[
    [ "Common", "dir_5c5dc640cfd170dd67920e19a68777da.html", "dir_5c5dc640cfd170dd67920e19a68777da" ],
    [ "ViewModels", "dir_39da25f943e7d35ccb4ea4f77996029c.html", "dir_39da25f943e7d35ccb4ea4f77996029c" ],
    [ "Views", "dir_09c78b3a5c37e10755f221b5ca3bc69f.html", "dir_09c78b3a5c37e10755f221b5ca3bc69f" ],
    [ "App.xaml.cs", "_app_8xaml_8cs.html", [
      [ "App", "class_holo_gen_1_1_u_i_1_1_app.html", "class_holo_gen_1_1_u_i_1_1_app" ]
    ] ],
    [ "MainWindow.xaml.cs", "_main_window_8xaml_8cs.html", [
      [ "MainWindow", "class_holo_gen_1_1_u_i_1_1_main_window.html", "class_holo_gen_1_1_u_i_1_1_main_window" ]
    ] ],
    [ "MainWindowViewModel.cs", "_main_window_view_model_8cs.html", [
      [ "MainWindowViewModel", "class_holo_gen_1_1_u_i_1_1_main_window_view_model.html", "class_holo_gen_1_1_u_i_1_1_main_window_view_model" ]
    ] ],
    [ "NamespaceDescriptions.cs", "_namespace_descriptions_8cs.html", null ]
];