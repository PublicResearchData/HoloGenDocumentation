var dir_6953142ff73f924e5c42d22ddc728834 =
[
    [ "Common", "dir_4b00cec90a89ea10d9a26c7e7cd7ecd5.html", "dir_4b00cec90a89ea10d9a26c7e7cd7ecd5" ],
    [ "DS", "dir_3ffb452e0de405542eaa871fe1f3f3e5.html", "dir_3ffb452e0de405542eaa871fe1f3f3e5" ],
    [ "GS", "dir_0b6d2792f80d578a9622edfb6b1c2ecf.html", "dir_0b6d2792f80d578a9622edfb6b1c2ecf" ],
    [ "OSPR", "dir_e55b2e1c74f1e01233a6ef2d28639beb.html", "dir_e55b2e1c74f1e01233a6ef2d28639beb" ],
    [ "SA", "dir_ba4f460854edd16462a28f5f262dc276.html", "dir_ba4f460854edd16462a28f5f262dc276" ],
    [ "VariantOption.cs", "_algorithm_2_algorithms_2_variants_2_variant_option_8cs.html", [
      [ "VariantOption", "interface_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_variants_1_1_variant_option.html", "interface_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_variants_1_1_variant_option" ]
    ] ],
    [ "VariantPossibility.cs", "_algorithm_2_algorithms_2_variants_2_variant_possibility_8cs.html", [
      [ "VariantPossibility", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_variants_1_1_variant_possibility.html", null ]
    ] ]
];