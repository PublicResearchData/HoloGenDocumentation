var class_holo_gen_1_1_options_1_1_output_1_1_run_1_1_show_results =
[
    [ "Default", "class_holo_gen_1_1_options_1_1_output_1_1_run_1_1_show_results.html#a85694f1628c91714c8743a8bf3ce90d9", null ],
    [ "Name", "class_holo_gen_1_1_options_1_1_output_1_1_run_1_1_show_results.html#a1712e076d36d8375e5feb28935efbff7", null ],
    [ "OffText", "class_holo_gen_1_1_options_1_1_output_1_1_run_1_1_show_results.html#ad50e30189870cc30bf26852fd5ddbbfa", null ],
    [ "OnText", "class_holo_gen_1_1_options_1_1_output_1_1_run_1_1_show_results.html#a79ebdb043c43096b9bcf79fa0048f555", null ],
    [ "ToolTip", "class_holo_gen_1_1_options_1_1_output_1_1_run_1_1_show_results.html#a597c9f3da5af4aee8395be5622c62e20", null ],
    [ "Watermark", "class_holo_gen_1_1_options_1_1_output_1_1_run_1_1_show_results.html#a4800fdabe973368412f75f3eec7f2374", null ],
    [ "ResultEvery", "class_holo_gen_1_1_options_1_1_output_1_1_run_1_1_show_results.html#aaab90453e6d21f8df49e72881b315fbb", null ]
];