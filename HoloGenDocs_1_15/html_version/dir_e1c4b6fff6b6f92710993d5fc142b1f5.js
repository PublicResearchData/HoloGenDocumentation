var dir_e1c4b6fff6b6f92710993d5fc142b1f5 =
[
    [ "Deserialiser.cs", "_deserialiser_8cs.html", [
      [ "Deserialiser", "class_holo_gen_1_1_serial_1_1_deserialiser.html", "class_holo_gen_1_1_serial_1_1_deserialiser" ]
    ] ],
    [ "DeserialiseResult.cs", "_deserialise_result_8cs.html", "_deserialise_result_8cs" ],
    [ "JsonDeserialiser.cs", "_json_deserialiser_8cs.html", [
      [ "JsonDeserialiser", "class_holo_gen_1_1_serial_1_1_json_deserialiser.html", "class_holo_gen_1_1_serial_1_1_json_deserialiser" ]
    ] ],
    [ "JsonImageDeserialiser.cs", "_json_image_deserialiser_8cs.html", [
      [ "JsonImageDeserialiser", "class_holo_gen_1_1_serial_1_1_json_image_deserialiser.html", null ]
    ] ],
    [ "JsonImageSerialiser.cs", "_json_image_serialiser_8cs.html", [
      [ "JsonImageSerialiser", "class_holo_gen_1_1_serial_1_1_json_image_serialiser.html", null ]
    ] ],
    [ "JsonOptionsDeserialiser.cs", "_json_options_deserialiser_8cs.html", [
      [ "JsonOptionsDeserialiser", "class_holo_gen_1_1_serial_1_1_json_options_deserialiser.html", null ]
    ] ],
    [ "JsonOptionsSerialiser.cs", "_json_options_serialiser_8cs.html", [
      [ "JsonOptionsSerialiser", "class_holo_gen_1_1_serial_1_1_json_options_serialiser.html", null ]
    ] ],
    [ "JsonSerialiser.cs", "_json_serialiser_8cs.html", [
      [ "JsonSerialiser", "class_holo_gen_1_1_serial_1_1_json_serialiser.html", "class_holo_gen_1_1_serial_1_1_json_serialiser" ]
    ] ],
    [ "JsonSettingsDeserialiser.cs", "_json_settings_deserialiser_8cs.html", [
      [ "JsonSettingsDeserialiser", "class_holo_gen_1_1_serial_1_1_json_settings_deserialiser.html", null ]
    ] ],
    [ "JsonSettingsSerialiser.cs.cs", "_json_settings_serialiser_8cs_8cs.html", [
      [ "JsonSettingsSerialiser", "class_holo_gen_1_1_serial_1_1_json_settings_serialiser.html", null ]
    ] ],
    [ "Serialiser.cs", "_serialiser_8cs.html", [
      [ "Serialiser", "class_holo_gen_1_1_serial_1_1_serialiser.html", "class_holo_gen_1_1_serial_1_1_serialiser" ]
    ] ],
    [ "SerialiseResult.cs", "_serialise_result_8cs.html", "_serialise_result_8cs" ]
];