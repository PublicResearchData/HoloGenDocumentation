var dir_e55b2e1c74f1e01233a6ef2d28639beb =
[
    [ "AdaptiveOSPRVariant.cs", "_adaptive_o_s_p_r_variant_8cs.html", [
      [ "AdaptiveOSPRVariant", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_variants_1_1_o_s_p_r_1_1_adaptive_o_s_p_r_variant.html", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_variants_1_1_o_s_p_r_1_1_adaptive_o_s_p_r_variant" ]
    ] ],
    [ "OSPRVariantOption.cs", "_o_s_p_r_variant_option_8cs.html", [
      [ "OSPRVariantOption", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_variants_1_1_o_s_p_r_1_1_o_s_p_r_variant_option.html", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_variants_1_1_o_s_p_r_1_1_o_s_p_r_variant_option" ]
    ] ],
    [ "OSPRVariantPossibilities.cs", "_o_s_p_r_variant_possibilities_8cs.html", [
      [ "OSPRVariantPossibilities", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_variants_1_1_o_s_p_r_1_1_o_s_p_r_variant_possibilities.html", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_variants_1_1_o_s_p_r_1_1_o_s_p_r_variant_possibilities" ]
    ] ],
    [ "OSPRVariantPossibility.cs", "_o_s_p_r_variant_possibility_8cs.html", [
      [ "OSPRVariantPossibility", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_variants_1_1_o_s_p_r_1_1_o_s_p_r_variant_possibility.html", null ]
    ] ],
    [ "StandardOSPRVariant.cs", "_standard_o_s_p_r_variant_8cs.html", [
      [ "StandardOSPRVariant", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_variants_1_1_o_s_p_r_1_1_standard_o_s_p_r_variant.html", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_variants_1_1_o_s_p_r_1_1_standard_o_s_p_r_variant" ]
    ] ]
];