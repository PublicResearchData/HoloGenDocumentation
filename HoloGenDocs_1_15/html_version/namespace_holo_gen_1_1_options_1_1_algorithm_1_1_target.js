var namespace_holo_gen_1_1_options_1_1_algorithm_1_1_target =
[
    [ "Type", "namespace_holo_gen_1_1_options_1_1_algorithm_1_1_target_1_1_type.html", "namespace_holo_gen_1_1_options_1_1_algorithm_1_1_target_1_1_type" ],
    [ "EfficiencyLimit", "class_holo_gen_1_1_options_1_1_algorithm_1_1_target_1_1_efficiency_limit.html", "class_holo_gen_1_1_options_1_1_algorithm_1_1_target_1_1_efficiency_limit" ],
    [ "ErrorLimit", "class_holo_gen_1_1_options_1_1_algorithm_1_1_target_1_1_error_limit.html", "class_holo_gen_1_1_options_1_1_algorithm_1_1_target_1_1_error_limit" ],
    [ "Target", "class_holo_gen_1_1_options_1_1_algorithm_1_1_target_1_1_target.html", "class_holo_gen_1_1_options_1_1_algorithm_1_1_target_1_1_target" ],
    [ "TargetFolder", "class_holo_gen_1_1_options_1_1_algorithm_1_1_target_1_1_target_folder.html", "class_holo_gen_1_1_options_1_1_algorithm_1_1_target_1_1_target_folder" ]
];