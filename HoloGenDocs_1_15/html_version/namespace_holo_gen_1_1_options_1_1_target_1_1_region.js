var namespace_holo_gen_1_1_options_1_1_target_1_1_region =
[
    [ "Type", "namespace_holo_gen_1_1_options_1_1_target_1_1_region_1_1_type.html", "namespace_holo_gen_1_1_options_1_1_target_1_1_region_1_1_type" ],
    [ "PixelCount", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_pixel_count.html", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_pixel_count" ],
    [ "RegionFile", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_region_file.html", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_region_file" ],
    [ "RegionFolder", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_region_folder.html", "class_holo_gen_1_1_options_1_1_target_1_1_region_1_1_region_folder" ]
];