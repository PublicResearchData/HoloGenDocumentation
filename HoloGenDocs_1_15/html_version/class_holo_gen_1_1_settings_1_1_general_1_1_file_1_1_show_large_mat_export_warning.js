var class_holo_gen_1_1_settings_1_1_general_1_1_file_1_1_show_large_mat_export_warning =
[
    [ "Default", "class_holo_gen_1_1_settings_1_1_general_1_1_file_1_1_show_large_mat_export_warning.html#a3f2c75542bc94f8bf5529210bb09f5c4", null ],
    [ "Name", "class_holo_gen_1_1_settings_1_1_general_1_1_file_1_1_show_large_mat_export_warning.html#a616d9af4c2231da9d110ca4b5cb73852", null ],
    [ "OffText", "class_holo_gen_1_1_settings_1_1_general_1_1_file_1_1_show_large_mat_export_warning.html#aad7d267c61f766a26437f47e33586808", null ],
    [ "OnText", "class_holo_gen_1_1_settings_1_1_general_1_1_file_1_1_show_large_mat_export_warning.html#a58efa222ae8d091ed4cbf8623c04317f", null ],
    [ "ToolTip", "class_holo_gen_1_1_settings_1_1_general_1_1_file_1_1_show_large_mat_export_warning.html#aeb427444c9c2f86f92046d62af83fb6a", null ],
    [ "Watermark", "class_holo_gen_1_1_settings_1_1_general_1_1_file_1_1_show_large_mat_export_warning.html#a21e3680e237d523dfb0e9777d3512313", null ]
];