var dir_3abfef960843114a853cefcb08c1544e =
[
    [ "Commands", "dir_30865e7276d6060533cde6527ec61473.html", "dir_30865e7276d6060533cde6527ec61473" ],
    [ "MVVM", "dir_9548274b3dee4dec24feffeb3daed33b.html", "dir_9548274b3dee4dec24feffeb3daed33b" ],
    [ "Navigation", "dir_fd0c9fd9fe740600d39418eec72bd277.html", "dir_fd0c9fd9fe740600d39418eec72bd277" ],
    [ "FileLocations.cs", "_file_locations_8cs.html", [
      [ "FileLocations", "class_holo_gen_1_1_u_i_1_1_utils_1_1_file_locations.html", null ]
    ] ],
    [ "ITabHandler.cs", "_i_tab_handler_8cs.html", [
      [ "ITabHandler", "interface_holo_gen_1_1_u_i_1_1_utils_1_1_i_tab_handler.html", "interface_holo_gen_1_1_u_i_1_1_utils_1_1_i_tab_handler" ]
    ] ],
    [ "ITabViewModel.cs", "_i_tab_view_model_8cs.html", [
      [ "ITabViewModel", "interface_holo_gen_1_1_u_i_1_1_utils_1_1_i_tab_view_model.html", "interface_holo_gen_1_1_u_i_1_1_utils_1_1_i_tab_view_model" ]
    ] ],
    [ "TabHandlerFramework.cs", "_tab_handler_framework_8cs.html", [
      [ "TabHandlerFramework", "class_holo_gen_1_1_u_i_1_1_utils_1_1_tab_handler_framework.html", null ]
    ] ],
    [ "WaitCursor.cs", "_wait_cursor_8cs.html", [
      [ "WaitCursor", "class_holo_gen_1_1_u_i_1_1_utils_1_1_wait_cursor.html", "class_holo_gen_1_1_u_i_1_1_utils_1_1_wait_cursor" ]
    ] ]
];