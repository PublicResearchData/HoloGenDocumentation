var interface_holo_gen_1_1_u_i_1_1_utils_1_1_i_tab_handler =
[
    [ "AddTab", "interface_holo_gen_1_1_u_i_1_1_utils_1_1_i_tab_handler.html#a23ed2fc59ee5bea8f34eb29ad83c2c44", null ],
    [ "AddTab", "interface_holo_gen_1_1_u_i_1_1_utils_1_1_i_tab_handler.html#a02c56a73f8bd6da5d3bc87ba5713931d", null ],
    [ "AddTab", "interface_holo_gen_1_1_u_i_1_1_utils_1_1_i_tab_handler.html#a0066771a9a977043a57fd6d54c7bf6f9", null ],
    [ "NewBatchProcessingFile", "interface_holo_gen_1_1_u_i_1_1_utils_1_1_i_tab_handler.html#a10ddac19c73cf34537ec2a943815ed3a", null ],
    [ "NewHologramSetupFile", "interface_holo_gen_1_1_u_i_1_1_utils_1_1_i_tab_handler.html#a3206bc6263fb8dc176bad654a7967bb6", null ],
    [ "NewPdfTab", "interface_holo_gen_1_1_u_i_1_1_utils_1_1_i_tab_handler.html#a206ff2bca5f53bf305af5fb8ec6ef590", null ],
    [ "RemoveTab", "interface_holo_gen_1_1_u_i_1_1_utils_1_1_i_tab_handler.html#ab39f1152a98b9dcf954548ba1211c169", null ],
    [ "Selected", "interface_holo_gen_1_1_u_i_1_1_utils_1_1_i_tab_handler.html#ab161827cea39c566d28d7dd4bdf6f93a", null ]
];