var namespace_holo_gen_1_1_settings_1_1_general_1_1_file =
[
    [ "FileSettingsFolder", "class_holo_gen_1_1_settings_1_1_general_1_1_file_1_1_file_settings_folder.html", "class_holo_gen_1_1_settings_1_1_general_1_1_file_1_1_file_settings_folder" ],
    [ "SaveScaleBarOnImageExport", "class_holo_gen_1_1_settings_1_1_general_1_1_file_1_1_save_scale_bar_on_image_export.html", "class_holo_gen_1_1_settings_1_1_general_1_1_file_1_1_save_scale_bar_on_image_export" ],
    [ "ShowCloseUnsavedFileWarning", "class_holo_gen_1_1_settings_1_1_general_1_1_file_1_1_show_close_unsaved_file_warning.html", "class_holo_gen_1_1_settings_1_1_general_1_1_file_1_1_show_close_unsaved_file_warning" ],
    [ "ShowLargeMatExportWarning", "class_holo_gen_1_1_settings_1_1_general_1_1_file_1_1_show_large_mat_export_warning.html", "class_holo_gen_1_1_settings_1_1_general_1_1_file_1_1_show_large_mat_export_warning" ],
    [ "ShowLargeMatImportWarning", "class_holo_gen_1_1_settings_1_1_general_1_1_file_1_1_show_large_mat_import_warning.html", "class_holo_gen_1_1_settings_1_1_general_1_1_file_1_1_show_large_mat_import_warning" ],
    [ "WarningSettingsFolder", "class_holo_gen_1_1_settings_1_1_general_1_1_file_1_1_warning_settings_folder.html", "class_holo_gen_1_1_settings_1_1_general_1_1_file_1_1_warning_settings_folder" ]
];