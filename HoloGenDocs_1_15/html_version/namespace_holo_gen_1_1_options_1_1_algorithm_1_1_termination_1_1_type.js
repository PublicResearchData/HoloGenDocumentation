var namespace_holo_gen_1_1_options_1_1_algorithm_1_1_termination_1_1_type =
[
    [ "TerminationError", "class_holo_gen_1_1_options_1_1_algorithm_1_1_termination_1_1_type_1_1_termination_error.html", "class_holo_gen_1_1_options_1_1_algorithm_1_1_termination_1_1_type_1_1_termination_error" ],
    [ "TerminationFirst", "class_holo_gen_1_1_options_1_1_algorithm_1_1_termination_1_1_type_1_1_termination_first.html", "class_holo_gen_1_1_options_1_1_algorithm_1_1_termination_1_1_type_1_1_termination_first" ],
    [ "TerminationFolder", "class_holo_gen_1_1_options_1_1_algorithm_1_1_termination_1_1_type_1_1_termination_folder.html", "class_holo_gen_1_1_options_1_1_algorithm_1_1_termination_1_1_type_1_1_termination_folder" ],
    [ "TerminationIterations", "class_holo_gen_1_1_options_1_1_algorithm_1_1_termination_1_1_type_1_1_termination_iterations.html", "class_holo_gen_1_1_options_1_1_algorithm_1_1_termination_1_1_type_1_1_termination_iterations" ],
    [ "TerminationOption", "class_holo_gen_1_1_options_1_1_algorithm_1_1_termination_1_1_type_1_1_termination_option.html", "class_holo_gen_1_1_options_1_1_algorithm_1_1_termination_1_1_type_1_1_termination_option" ],
    [ "TerminationPossibilities", "class_holo_gen_1_1_options_1_1_algorithm_1_1_termination_1_1_type_1_1_termination_possibilities.html", "class_holo_gen_1_1_options_1_1_algorithm_1_1_termination_1_1_type_1_1_termination_possibilities" ],
    [ "TerminationPossibility", "class_holo_gen_1_1_options_1_1_algorithm_1_1_termination_1_1_type_1_1_termination_possibility.html", null ]
];