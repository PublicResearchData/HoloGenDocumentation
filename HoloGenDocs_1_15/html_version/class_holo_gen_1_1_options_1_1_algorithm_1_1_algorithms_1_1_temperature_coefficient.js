var class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_temperature_coefficient =
[
    [ "Default", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_temperature_coefficient.html#acf38afc580940f0edb7d6663ce6be60d", null ],
    [ "MaxValue", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_temperature_coefficient.html#ac5b13a2848a04ca93cd3cb9c146be10f", null ],
    [ "MinValue", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_temperature_coefficient.html#af2788cec5e9c9047e250a8cdf8517086", null ],
    [ "Name", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_temperature_coefficient.html#adeaf74ad5c35b1e3538c7372c8d4bc1b", null ],
    [ "ToolTip", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_temperature_coefficient.html#a402151c77e7ae2fade5c3b9a9abbc492", null ],
    [ "Watermark", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_temperature_coefficient.html#afb1825ba3204cb9d9b6dd7a1058535be", null ]
];