var namespace_holo_gen_1_1_options_1_1_hardware_1_1_hardware_1_1_type =
[
    [ "HardwareCPU", "class_holo_gen_1_1_options_1_1_hardware_1_1_hardware_1_1_type_1_1_hardware_c_p_u.html", "class_holo_gen_1_1_options_1_1_hardware_1_1_hardware_1_1_type_1_1_hardware_c_p_u" ],
    [ "HardwareGPU", "class_holo_gen_1_1_options_1_1_hardware_1_1_hardware_1_1_type_1_1_hardware_g_p_u.html", "class_holo_gen_1_1_options_1_1_hardware_1_1_hardware_1_1_type_1_1_hardware_g_p_u" ],
    [ "HardwareOption", "class_holo_gen_1_1_options_1_1_hardware_1_1_hardware_1_1_type_1_1_hardware_option.html", "class_holo_gen_1_1_options_1_1_hardware_1_1_hardware_1_1_type_1_1_hardware_option" ],
    [ "HardwarePossibilities", "class_holo_gen_1_1_options_1_1_hardware_1_1_hardware_1_1_type_1_1_hardware_possibilities.html", "class_holo_gen_1_1_options_1_1_hardware_1_1_hardware_1_1_type_1_1_hardware_possibilities" ],
    [ "HardwarePossibility", "class_holo_gen_1_1_options_1_1_hardware_1_1_hardware_1_1_type_1_1_hardware_possibility.html", null ]
];