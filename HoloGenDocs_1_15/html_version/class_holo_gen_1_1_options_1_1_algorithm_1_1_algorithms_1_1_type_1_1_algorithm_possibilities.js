var class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_algorithm_possibilities =
[
    [ "CommonAlgorithm", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_algorithm_possibilities.html#ac87d9691dbd9aa60782ebb7d0a002777", null ],
    [ "DSAlgorithm", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_algorithm_possibilities.html#aa8c60773c7383b8c69e22cc58dd5aff7", null ],
    [ "GSAlgorithm", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_algorithm_possibilities.html#a7bcdfd879ef2e19e8cd13aefb3de469e", null ],
    [ "OSPRAlgorithm", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_algorithm_possibilities.html#a99105e00100b9c93cb1c000a602be697", null ],
    [ "SAAlgorithm", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_type_1_1_algorithm_possibilities.html#a8627b53689e0131c7bdc09c76f5dbe88", null ]
];