var namespace_holo_gen_1_1_i_o =
[
    [ "Convertors", "namespace_holo_gen_1_1_i_o_1_1_convertors.html", "namespace_holo_gen_1_1_i_o_1_1_convertors" ],
    [ "FileLoader", "class_holo_gen_1_1_i_o_1_1_file_loader.html", "class_holo_gen_1_1_i_o_1_1_file_loader" ],
    [ "FileSaver", "class_holo_gen_1_1_i_o_1_1_file_saver.html", "class_holo_gen_1_1_i_o_1_1_file_saver" ],
    [ "JsonFileLoader", "class_holo_gen_1_1_i_o_1_1_json_file_loader.html", "class_holo_gen_1_1_i_o_1_1_json_file_loader" ],
    [ "JsonFileSaver", "class_holo_gen_1_1_i_o_1_1_json_file_saver.html", "class_holo_gen_1_1_i_o_1_1_json_file_saver" ],
    [ "JsonImageFileLoader", "class_holo_gen_1_1_i_o_1_1_json_image_file_loader.html", null ],
    [ "JsonImageFileSaver", "class_holo_gen_1_1_i_o_1_1_json_image_file_saver.html", null ],
    [ "JsonOptionsFileLoader", "class_holo_gen_1_1_i_o_1_1_json_options_file_loader.html", null ],
    [ "JsonOptionsFileSaver", "class_holo_gen_1_1_i_o_1_1_json_options_file_saver.html", null ],
    [ "JsonSettingsFileLoader", "class_holo_gen_1_1_i_o_1_1_json_settings_file_loader.html", null ],
    [ "JsonSettingsFileSaver", "class_holo_gen_1_1_i_o_1_1_json_settings_file_saver.html", null ]
];