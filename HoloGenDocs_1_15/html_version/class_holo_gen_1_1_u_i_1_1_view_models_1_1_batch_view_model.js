var class_holo_gen_1_1_u_i_1_1_view_models_1_1_batch_view_model =
[
    [ "BatchViewModel", "class_holo_gen_1_1_u_i_1_1_view_models_1_1_batch_view_model.html#a8559ca48f6cf68524c7b985224ce1758", null ],
    [ "_NumErrors", "class_holo_gen_1_1_u_i_1_1_view_models_1_1_batch_view_model.html#acf9f5276c651e95ad150d99ddeef71cb", null ],
    [ "_RunName", "class_holo_gen_1_1_u_i_1_1_view_models_1_1_batch_view_model.html#ac2e16f624d24ae2a3177fccc4e56030b", null ],
    [ "_RunToolTip", "class_holo_gen_1_1_u_i_1_1_view_models_1_1_batch_view_model.html#aff8c3679e2e65bd642fb68604581de29", null ],
    [ "ViewModel", "class_holo_gen_1_1_u_i_1_1_view_models_1_1_batch_view_model.html#a7a7b0af422f767e95f476f9010b60210", null ],
    [ "NumErrors", "class_holo_gen_1_1_u_i_1_1_view_models_1_1_batch_view_model.html#ac5e62122457c45f89a4d3920b11c7cfa", null ],
    [ "Options", "class_holo_gen_1_1_u_i_1_1_view_models_1_1_batch_view_model.html#a5098c4614dca1ca912a36b69342525d0", null ],
    [ "ResetCommand", "class_holo_gen_1_1_u_i_1_1_view_models_1_1_batch_view_model.html#ac6298e10c8fd3ac8fa45147554e21313", null ],
    [ "RunCommand", "class_holo_gen_1_1_u_i_1_1_view_models_1_1_batch_view_model.html#ac730fcc076ba70400838e1d0a4313682", null ],
    [ "RunName", "class_holo_gen_1_1_u_i_1_1_view_models_1_1_batch_view_model.html#ad92fa532927c8ba39367d9fe3a0117fa", null ],
    [ "RunToolTip", "class_holo_gen_1_1_u_i_1_1_view_models_1_1_batch_view_model.html#ac0aa0492d6ae3796d5c9477bd3e9a9dd", null ]
];