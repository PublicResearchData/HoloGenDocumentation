var namespace_holo_gen_1_1_image =
[
    [ "Options", "namespace_holo_gen_1_1_image_1_1_options.html", "namespace_holo_gen_1_1_image_1_1_options" ],
    [ "ComplexImage", "class_holo_gen_1_1_image_1_1_complex_image.html", "class_holo_gen_1_1_image_1_1_complex_image" ],
    [ "ComplexImageVersion", "class_holo_gen_1_1_image_1_1_complex_image_version.html", "class_holo_gen_1_1_image_1_1_complex_image_version" ],
    [ "ImageCache", "class_holo_gen_1_1_image_1_1_image_cache.html", "class_holo_gen_1_1_image_1_1_image_cache" ],
    [ "ImageView", "class_holo_gen_1_1_image_1_1_image_view.html", "class_holo_gen_1_1_image_1_1_image_view" ],
    [ "ScaleAdder", "class_holo_gen_1_1_image_1_1_scale_adder.html", null ]
];