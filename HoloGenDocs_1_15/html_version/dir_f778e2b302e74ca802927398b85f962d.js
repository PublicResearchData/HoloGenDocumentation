var dir_f778e2b302e74ca802927398b85f962d =
[
    [ "HardwareCPU.cs", "_hardware_c_p_u_8cs.html", [
      [ "HardwareCPU", "class_holo_gen_1_1_options_1_1_hardware_1_1_hardware_1_1_type_1_1_hardware_c_p_u.html", "class_holo_gen_1_1_options_1_1_hardware_1_1_hardware_1_1_type_1_1_hardware_c_p_u" ]
    ] ],
    [ "HardwareGPU.cs", "_hardware_g_p_u_8cs.html", [
      [ "HardwareGPU", "class_holo_gen_1_1_options_1_1_hardware_1_1_hardware_1_1_type_1_1_hardware_g_p_u.html", "class_holo_gen_1_1_options_1_1_hardware_1_1_hardware_1_1_type_1_1_hardware_g_p_u" ]
    ] ],
    [ "HardwareOption.cs", "_hardware_option_8cs.html", [
      [ "HardwareOption", "class_holo_gen_1_1_options_1_1_hardware_1_1_hardware_1_1_type_1_1_hardware_option.html", "class_holo_gen_1_1_options_1_1_hardware_1_1_hardware_1_1_type_1_1_hardware_option" ]
    ] ],
    [ "HardwarePossibilities.cs", "_hardware_possibilities_8cs.html", [
      [ "HardwarePossibilities", "class_holo_gen_1_1_options_1_1_hardware_1_1_hardware_1_1_type_1_1_hardware_possibilities.html", "class_holo_gen_1_1_options_1_1_hardware_1_1_hardware_1_1_type_1_1_hardware_possibilities" ]
    ] ],
    [ "HardwarePossibility.cs", "_hardware_possibility_8cs.html", [
      [ "HardwarePossibility", "class_holo_gen_1_1_options_1_1_hardware_1_1_hardware_1_1_type_1_1_hardware_possibility.html", null ]
    ] ]
];