var dir_d3fdba0ce8e39b5e243d91f33b3a58a3 =
[
    [ "AbstractQuantiser.cu", "_abstract_quantiser_8cu.html", null ],
    [ "AbstractQuantiser.cuh", "_abstract_quantiser_8cuh.html", null ],
    [ "AlgorithmCuda.cu", "_algorithm_cuda_8cu.html", null ],
    [ "AlgorithmCuda.cuh", "_algorithm_cuda_8cuh.html", null ],
    [ "FFTDirectionCuda.h", "_f_f_t_direction_cuda_8h.html", "_f_f_t_direction_cuda_8h" ],
    [ "FFTHandler.cu", "_f_f_t_handler_8cu.html", null ],
    [ "FFTHandler.cuh", "_f_f_t_handler_8cuh.html", null ],
    [ "FFTScaleType.h", "_f_f_t_scale_type_8h.html", "_f_f_t_scale_type_8h" ],
    [ "Globals.h", "_globals_8h.html", [
      [ "Globals", "class_holo_gen_1_1_alg_1_1_base_1_1_cuda_1_1_globals.html", null ]
    ] ],
    [ "IlluminationTypeCuda.h", "_illumination_type_cuda_8h.html", "_illumination_type_cuda_8h" ],
    [ "Image.cuh", "_image_8cuh.html", null ],
    [ "Image.h", "_image_8h.html", null ],
    [ "InitialSeedTypeCuda.h", "_initial_seed_type_cuda_8h.html", "_initial_seed_type_cuda_8h" ],
    [ "LoggingCallback.h", "_logging_callback_8h.html", "_logging_callback_8h" ],
    [ "LoggingDelegate.h", "_logging_delegate_8h.html", "_logging_delegate_8h" ],
    [ "Normaliser.cu", "_normaliser_8cu.html", null ],
    [ "Normaliser.cuh", "_normaliser_8cuh.html", null ],
    [ "QuantiserNearest.cu", "_quantiser_nearest_8cu.html", null ],
    [ "QuantiserNearest.cuh", "_quantiser_nearest_8cuh.html", null ],
    [ "Randomiser.cu", "_randomiser_8cu.html", null ],
    [ "Randomiser.cuh", "_randomiser_8cuh.html", null ],
    [ "SLMModulationSchemeCuda.h", "_s_l_m_modulation_scheme_cuda_8h.html", "_s_l_m_modulation_scheme_cuda_8h" ]
];