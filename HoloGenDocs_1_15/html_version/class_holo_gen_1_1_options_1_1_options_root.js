var class_holo_gen_1_1_options_1_1_options_root =
[
    [ "OptionsRoot", "class_holo_gen_1_1_options_1_1_options_root.html#a83047023a6a5f189048ac74d016297bf", null ],
    [ "Name", "class_holo_gen_1_1_options_1_1_options_root.html#a20c459b593dba2e75e0fdba56e3d8ddb", null ],
    [ "ToolTip", "class_holo_gen_1_1_options_1_1_options_root.html#a2723be6b91ed52143eab07c40e217f1a", null ],
    [ "AlgorithmsPage", "class_holo_gen_1_1_options_1_1_options_root.html#a8bbbce77b707e8d096c7608b71c59585", null ],
    [ "HardwarePage", "class_holo_gen_1_1_options_1_1_options_root.html#aab66b219bc8659e27bc597fcdb183f61", null ],
    [ "OutputPage", "class_holo_gen_1_1_options_1_1_options_root.html#a795832c52b6be9070ed1a14f70bbacc0", null ],
    [ "ProjectorPage", "class_holo_gen_1_1_options_1_1_options_root.html#a9dfbd97475715152ee03ded2468d1048", null ],
    [ "TargetPage", "class_holo_gen_1_1_options_1_1_options_root.html#aa9b6814e7842153cd51e396d0062f85a", null ],
    [ "Valid", "class_holo_gen_1_1_options_1_1_options_root.html#a3f12fc5f8c6dde9f2dfa481aa77bdf49", null ]
];