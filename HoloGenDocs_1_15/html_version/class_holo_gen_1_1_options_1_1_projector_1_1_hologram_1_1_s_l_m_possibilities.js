var class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_s_l_m_possibilities =
[
    [ "BinaryAmpSLM", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_s_l_m_possibilities.html#aef22f5a40fdc4300a664f837a2624928", null ],
    [ "BinaryPhaseSLM", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_s_l_m_possibilities.html#a66bf3746bdb0b1dd825b513bf8246fe2", null ],
    [ "MultiAmpSLM", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_s_l_m_possibilities.html#aa2632509ed2c693c0db90f21ed8b60ab", null ],
    [ "MultiPhaseSLM", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_s_l_m_possibilities.html#af2545b9c7acdd18668fd94025be96ef1", null ]
];