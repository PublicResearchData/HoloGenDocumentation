var class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_variants_1_1_s_a_1_1_s_a_variant_option =
[
    [ "Name", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_variants_1_1_s_a_1_1_s_a_variant_option.html#accc7f6bb452d864a5fbb56208ae93a72", null ],
    [ "Possibilities", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_variants_1_1_s_a_1_1_s_a_variant_option.html#a02cd55df0bbf2885437500a543162dfc", null ],
    [ "Possibilities2", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_variants_1_1_s_a_1_1_s_a_variant_option.html#a304399a72d2933d4c06cb26e9a383050", null ],
    [ "ToolTip", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_variants_1_1_s_a_1_1_s_a_variant_option.html#aaab46582077defc00dcdb4b0d6d86f6e", null ],
    [ "VariantPossibility", "class_holo_gen_1_1_options_1_1_algorithm_1_1_algorithms_1_1_variants_1_1_s_a_1_1_s_a_variant_option.html#a76a1a660601aa63113b8d7525c88800a", null ]
];