var class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_maximum_angle =
[
    [ "Default", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_maximum_angle.html#acccd373be96052029cca44fb50cd1fd7", null ],
    [ "MaxValue", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_maximum_angle.html#a590bf06b4641fc62da91d29fa5f72d6d", null ],
    [ "MinValue", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_maximum_angle.html#a3eda83b4bcfbb0dadc24320227f8ea13", null ],
    [ "Name", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_maximum_angle.html#abcfdcd1b5e137fa311bf617d2cdb92eb", null ],
    [ "ToolTip", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_maximum_angle.html#a260b78c02387251df5f9683fa9cd138a", null ],
    [ "Watermark", "class_holo_gen_1_1_options_1_1_projector_1_1_hologram_1_1_maximum_angle.html#a638f5825110196df159741d4159dc644", null ]
];