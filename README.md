README
======

The documentation for HoloGen

Original version available https://doi.org/10.17863/CAM.26100

## Contributors

### Peter J. Christopher
* Email: pjc209@cam.ac.uk
* Email: peterjchristopher@gmail.com
* LinkedIn: https://www.linkedin.com/in/peterjchristopher/
* Website: www.peterjchristopher.me.uk
* Blog: http://peterjchristopher.blogspot.com/
* Address: Centre of Molecular Materials for Photonics and Electronics (CMMPE), Centre for Advanced Photonics and Electronics, University of Cambridge, 9 JJ Thomson Avenue, Cambridge, UK, CB3 0FF
